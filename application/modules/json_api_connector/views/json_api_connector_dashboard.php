<?php include("application/modules/json_api_connector/views/all_js.php"); ?>
<style type="text/css">
  .space{height: 10px;}
  .css-checkbox{display: none;}
  .css-label{padding:8.5px 10px; background: #eee;border-radius: 7px;-moz-border-radius: 7px;-webkit-border-radius: 7px;text-align: center;}
  .css-label:hover{background: #ddd;cursor: pointer;}
  .single-label{min-width: 97.5%;}
  .double-label{min-width: 48.5%;}
  .triple-label{min-width: 31%;}
  .checkbox-inline+.checkbox-inline, .radio-inline+.radio-inline {margin-left: 0px !important;}
  .list-group-item small { word-break: break-all !important; }

  .margin_div { margin-bottom: 10px; }

</style>


<div class="container-fluid"><br>
  <div class="box box-widget widget-user-2" >
    <div class="widget-user-header" style="border-radius: 0;">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
                <div class="widget-user-image">
                    <i class="fa fa-recycle fa-4x pull-left dynamic_font_color"></i>
                </div>
                <h3 class="widget-user-username dynamic_font_color"><?php echo $this->lang->line("Json API Connector"); ?>
                    <a href="#" data-placement="bottom" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("Json API Connector ");?>" data-content="<?php echo $this->lang->line('JSON API connector sends JSON data to any of your provided external URL. Once any specific event mentioned below is triggered, system will send json data to your provided webhook URL & it will wait for 10 second to get response. If system does not get response in 10 seconds, it will send another post request to your webhook. You can then process your data as you want. This feature can be used to connect our system with any third party app like Zapier or your own custom app.') ?>"><i style="font-size: 16px;" class='fa fa-info-circle'></i> </a>
                </h3>
                <h5 class="widget-user-desc"><?php echo $this->lang->line("Connection Lists"); ?></h5>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                <a class="btn btn-outline-primary pull-right add_connector" id="add_feed" style="margin-top:15px;" data-toggle="modal" href='#add_feed_modal'><i class="fa fa-plus-circle"></i> <?php echo $this->lang->line('New JSON API Connection');?></a>
            </div>
        </div>
    </div>

    <div class="box-footer" style="border-radius: 0;padding:20px;">
    <?php 
        if(empty($connector_data)) echo "<h4 class='text-center'>".$this->lang->line('No Data found.')."</h4>";
        else
        {
            echo "<div class='table-responsive'> 
            <table class='table table-bordered table-condensed' id='settings_data_table'>";
                echo "<thead>";
                    echo "<tr>";
                        echo "<th class='text-center'>".$this->lang->line("SN")."</th>";
                        echo "<th class='text-center'>".$this->lang->line("Name")."</th>";
                        echo "<th class='text-center'>".$this->lang->line("Webhook URL")."</th>";
                        echo "<th class='text-center'>".$this->lang->line("Actions")."</th>";               
                        echo "<th class='text-center'>".$this->lang->line("Page Name")."</th>";
                        echo "<th class='text-center'>".$this->lang->line("Added Time")."</th>";               
                        echo "<th class='text-center'>".$this->lang->line("Last Triggered Time")."</th>";  
                    echo "</tr>";             
                echo "</thead>";

                echo "<tbody>";
                  $i=0;
                  foreach ($connector_data as $key => $value) 
                  {
                    $i++;
                    if($value['added_date']!="0000-00-00 00:00:00") $added_date=date('j M H:i',strtotime($value['added_date']));
                    else $added_date =  "<i class='fa fa-remove'></i>";
                    
                    if($value['last_trigger_time']!="0000-00-00 00:00:00") $last_trigger_time=date('j M H:i',strtotime($value['last_trigger_time']));
                    else $last_trigger_time =  "<i class='fa fa-remove'></i>";

                    echo "<tr>";
                        echo "<td class='text-center' nowrap>".$i."</td>";
                        echo "<td class='text-center' nowrap>".$value['name']."</td>";
                        echo "<td class='text-center' nowrap>".$value['webhook_url']."</td>";
                        echo "<td class='text-center' nowrap>";
                            echo "<a title='".$this->lang->line("view")."' class='btn btn-sm btn-outline-dark view_connector' table_id='".$value['id']."'><i class='fa fa-eye'></i></a>&nbsp;";
                            echo "<a title='".$this->lang->line("edit")."' class='btn btn-sm btn-outline-primary edit_connector' table_id='".$value['id']."'><i class='fa fa-edit'></i></a>&nbsp;";
                            echo "<a title='".$this->lang->line("Delete")."' class='btn btn-sm btn-outline-danger delete_connector' table_id='".$value['id']."'><i class='fa fa-trash'></i></a>&nbsp;";
                        echo "</td>";
                        echo "<td class='text-center' nowrap>".$value['page_name']."</td>";
                        echo "<td class='text-center' nowrap>".$added_date."</td>";
                        echo "<td class='text-center' nowrap>".$last_trigger_time."</td>";
                    echo "</tr>";
                  }
                echo "</tbody>";
            echo "</table></div>";
        }
      ?>
    </div>
  </div>

</div>


<div class="modal fade" id="add_new_connector_modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id='add_connector_modal_close' data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-center"><i class="fa fa-plus-circle"></i> <?php echo $this->lang->line("Add New Connection");?></h4>
            </div>
            <div class="modal-body">
                <style>
                    .download_box{border:1px solid #ccc;margin: 0 auto;text-align: center;margin-top:3%;padding-bottom: 20px;background-color: #fffddd;color:#000;}
                </style>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div id="AddconnectorBody" style="padding: 20px;">
                            <form id="json_api_connector_form" action="" method="POST">
                                <div class="row margin_div">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="name"><i class="fa fa-flag"></i> <?php echo $this->lang->line("Connection Name"); ?><span style="color: red;"> *</span></label>
                                            <input type="text" class="form-control" id="name" placeholder="<?php echo $this->lang->line("Enter your connector name"); ?>" name="connector_name">
                                        </div>
                                    </div>
                                </div>

                                <div class="row margin_div">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="webhook_url"><i class="fa fa-globe"></i> <?php echo $this->lang->line("Webhook URL"); ?><span style="color: red;"> *</span></label>
                                            <input type="text" class="form-control" id="webhook_url" placeholder="<?php echo $this->lang->line("Enter your webhook URL"); ?>" name="webhook_url">
                                        </div>
                                    </div>
                                </div>

                                <div class="row margin_div"> 
                                    <div class="col-xs-12"> 
                                        <div class="form-group">
                                            <label><i class="fa fa-sticky-note"></i> <?php echo $this->lang->line("Please select a page");?><span style="color: red;"> *</span></label>
                                            <select name="page_table_id" id="page_table_id" class="form-control">
                                            <?php
                                                echo "<option value=''>{$this->lang->line('Please select a page')}</option>";
                                                foreach($page_info as $key => $val)
                                                {
                                                    $page_id   = $val['id'];
                                                    $page_name = $val['page_name'];
                                                    echo "<option value='{$page_id}'>{$page_name}</option>";
                                                }

                                            ?>
                                            </select>
                                        </div>       
                                    </div>  
                                </div>


                                <div class="row margin_div">
                                    <div class="col-xs-12 col-md-12">
                                        <label><i class="fa fa-sitemap"></i> <?php echo $this->lang->line("What Field Change Trigger Webhook"); ?><span style="color: red;"> *</span></label>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-2">
                                                <div class="form-group">
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox" value="trigger_email" id="trigger_email" name="field[]"><?php echo $this->lang->line("email"); ?>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-4">
                                                <div class="form-group">
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox" value="trigger_phone_number" id="trigger_phone_number" name="field[]"><?php echo $this->lang->line("Phone number"); ?>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-3">
                                                <div class="form-group">
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox" value="trigger_location" id="trigger_location" name="field[]"><?php echo $this->lang->line("location"); ?>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-3">
                                                <div class="form-group">
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox" value="trigger_postbackid" id="trigger_postbackid" name="field[]"><?php echo $this->lang->line("Postback ID"); ?>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row margin_div" id="postback_div" style="display: none;">
                                </div>
                                
                                <div class="row margin_div">
                                    <div class="col-xs-12">
                                        <label><i class="fa fa-send"></i> <?php echo $this->lang->line("Which Data You Want To Send"); ?><span style="color: red;"> *</span></label>
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" value="psid" id="psid" name="variable_post[]"><?php echo $this->lang->line("PSID"); ?>
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" value="subscribed_at" id="subscribed_at" name="variable_post[]"><?php echo $this->lang->line("Subscribed At"); ?>
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" value="postbackid" id="phone_number" name="variable_post[]"><?php echo $this->lang->line("Postback ID"); ?>
                                                </label>
                                            </div>
                                            <div class="col-xs-3">
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" value="first_name" id="first_name" name="variable_post[]"><?php echo $this->lang->line("First Name"); ?>
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" value="last_name" id="last_name" name="variable_post[]"><?php echo $this->lang->line("Last Name"); ?>
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" value="email" id="email" name="variable_post[]"><?php echo $this->lang->line("Email"); ?>
                                                </label>
                                                

                                            </div>
                                            <div class="col-xs-4">
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" value="page_id" id="page_id" name="variable_post[]"><?php echo $this->lang->line("Page ID"); ?>
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" value="page_name" id="page_name" name="variable_post[]"><?php echo $this->lang->line("Page Name"); ?>
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" value="phone_number" id="phone_number" name="variable_post[]"><?php echo $this->lang->line("Phone number"); ?>
                                                </label>
                                                
                                            </div>
                                            <div class="col-xs-2">
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" value="user_location" id="user_location" name="variable_post[]"><?php echo $this->lang->line("Location"); ?>
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" value="labels" id="labels" name="variable_post[]"><?php echo $this->lang->line("Labels"); ?>
                                                </label>
                                            </div>
                                                
                                        </div>
                                    </div>
                                </div><hr>

                                <div class="row margin_div">
                                    <div class="text-center" id="response_status"></div>
                                </div><br>

                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <button id="save_added_connector_infos" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> <?php echo $this->lang->line('save'); ?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="view_connector_info_modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-center"><i class="fa fa-eye"></i> <?php echo $this->lang->line("Json API Connector Info"); ?></h4>
            </div>
            <div class="row">
                <div class="col-xs-12"><div class="modal-body" id="info_modal"></div></div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="update_connector_modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id='add_connector_modal_close' data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-center"><i class="fa fa-edit"></i> <?php echo $this->lang->line("Update Connector");?></h4>
            </div>
            <div class="modal-body">
                <style>
                    .download_box{border:1px solid #ccc;margin: 0 auto;text-align: center;margin-top:3%;padding-bottom: 20px;background-color: #fffddd;color:#000;}
                </style>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div id="updateConnectorForm" style="padding: 20px;">

                        </div>
                    </div>
                </div>
           
            </div>
        </div>
    </div>
</div>
