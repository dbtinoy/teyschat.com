<script type="text/javascript">
    $j("document").ready(function()
    {
        var base_url = "<?php echo site_url(); ?>";

        $('[data-toggle="popover"]').popover(); 
        $('[data-toggle="popover"]').on('click', function(e) {e.preventDefault(); return true;});
        $("#settings_data_table").DataTable();

        $(document.body).on('click','.add_connector',function(event)
        {
            event.preventDefault();

            $("#add_new_connector_modal").modal();

        });          

        // open add connector form modal
        $(document.body).on('click','#add_connector_modal_close',function(event){
            event.preventDefault();
            location.reload();
        });

        $(document.body).on('click', '#save_added_connector_infos', function(event) 
        {
            event.preventDefault();

            var name              = $("#name").val();
            var webhook_url       = $("#webhook_url").val();
            var page_table_id     = $("#page_table_id").val();
            var triggered_webhook = $('input[name="field[]"]:checked').length;
            var sending_data      = $('input[name="variable_post[]"]:checked').length;

            var checkorNot = false;
            if($("#trigger_postbackid").prop('checked')==true)
            {
                var postbackids = $('#postback').val();
                checkorNot = true;
            }



            if(name == '')
            {   
                var error = '<?php echo $this->lang->line("You have not given any connector name. Please give a name."); ?>';
                alertify.alert('<?php echo $this->lang->line("Alert")?>',error,function(){});

            } else if(webhook_url == '')
            {
                var error = '<?php echo $this->lang->line("You have not given any Webhook URL. Please give an URL."); ?>';
                alertify.alert('<?php echo $this->lang->line("Alert")?>',error,function(){});

            } else if(page_table_id == '')
            {
                var error = '<?php echo $this->lang->line("Please select a page."); ?>';
                alertify.alert('<?php echo $this->lang->line("Alert")?>',error,function(){});

            } else if(triggered_webhook == 0)
            {
                var error = '<?php echo $this->lang->line("Please select at least one field from <b>Trigger Webhook</b> Section."); ?>';
                alertify.alert('<?php echo $this->lang->line("Alert")?>',error,function(){});

            } else if(sending_data == 0)
            {
                var error = '<?php echo $this->lang->line("Please select at least one field from <b>Which Data You Want To Send</b> Section."); ?>';
                alertify.alert('<?php echo $this->lang->line("Alert")?>',error,function(){});
            } else if(checkorNot == true && postbackids == null)
            {
                var error = '<?php echo $this->lang->line("Please select at least one Postback ID."); ?>';
                alertify.alert('<?php echo $this->lang->line("Alert")?>',error,function(){});
            }
            else
            {
                var loading = '<img src="'+base_url+'assets/pre-loader/Fading squares2.gif" class="center-block">';
                $('#response_status').html(loading);

                var alldatas = new FormData($("#json_api_connector_form")[0]);

                $.ajax({
                    url: base_url+'json_api_connector/ajax_connector_info_saving',
                    type: 'POST',
                    dataType: 'JSON',
                    data: alldatas,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success:function(response)
                    {
                        if(response.result =='1')
                        {
                            $('#response_status').html(response.msg);
                        } else
                        {
                            $('#response_status').html(response.msg);
                        }
                    }
                });
            }        
        });

        $(document.body).on('click','.edit_connector',function(event)
        {
            event.preventDefault();

            var table_id = $(this).attr('table_id');

            var loading = '<img src="'+base_url+'assets/pre-loader/Fading squares2.gif" class="center-block">';
                $('#response_status').html(loading);



            $.ajax({
                url: base_url+'json_api_connector/ajax_get_update_connector_info',
                type: 'POST',
                data: {table_id:table_id},
                success:function(response)
                {
                    if(response)
                    {
                        $("#update_connector_modal").modal();
                        $("#updateConnectorForm").html(response);
                        if($("#trigger_postbackid_updated").prop('checked')==true)
                        {
                            $("#updated_postback_div").css('display','block');

                        } else 
                        {
                            
                            $("#updated_postback_div").css('display','none');
                        }
                    }
                }
            });


        });


        $(document.body).on('click', '#save_updated_connector_infos', function(event) 
        {
            event.preventDefault();

            var name              = $("#connector_name").val();
            var webhook_url       = $("#updated_webhook_url").val();
            var page_table_id     = $("#updated_page_table_id").val();
            var triggered_webhook = $('input[name="updated_field[]"]:checked').length;
            var sending_data      = $('input[name="updated_variable_post[]"]:checked').length;

            var checkedornot = false;
            if($("#trigger_postbackid_updated").prop('checked')==true)
            {   
                var postbackids = $('#postback').val();
                checkedornot = true;
            }


            if(name == '')
            {   
                var error = '<?php echo $this->lang->line("You have not given any connector name. Please give a name."); ?>';
                alertify.alert('<?php echo $this->lang->line("Alert")?>',error,function(){});

            } else if(webhook_url == '')
            {
                var error = '<?php echo $this->lang->line("You have not given any Webhook URL. Please give an URL."); ?>';
                alertify.alert('<?php echo $this->lang->line("Alert")?>',error,function(){});

            } else if(page_table_id == '')
            {
                var error = '<?php echo $this->lang->line("Please select a page."); ?>';
                alertify.alert('<?php echo $this->lang->line("Alert")?>',error,function(){});

            } else if(triggered_webhook == 0)
            {
                var error = '<?php echo $this->lang->line("Please select at least one field from <b>Trigger Webhook</b> Section."); ?>';
                alertify.alert('<?php echo $this->lang->line("Alert")?>',error,function(){});

            } else if(sending_data == 0)
            {
                var error = '<?php echo $this->lang->line("Please select at least one field from <b>Which Data You Want To Send</b> Section."); ?>';
                alertify.alert('<?php echo $this->lang->line("Alert")?>',error,function(){});

            } else if(checkedornot == true && postbackids == null)
            {
                var error = '<?php echo $this->lang->line("Please select at least one postback ID."); ?>';
                alertify.alert('<?php echo $this->lang->line("Alert")?>',error,function(){});
            }
            else
            {
                var loading = '<img src="'+base_url+'assets/pre-loader/Fading squares2.gif" class="center-block">';
                $('#updated_response_status').html(loading);

                var alldatas = new FormData($("#json_api_connector_update_form")[0]);

                $.ajax({
                    url: base_url+'json_api_connector/ajax_connector_info_updating',
                    type: 'POST',
                    dataType: 'JSON',
                    data: alldatas,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success:function(response)
                    {
                        if(response.result =='1')
                        {
                            $('#updated_response_status').html(response.msg);
                        } else
                        {
                            $('#updated_response_status').html(response.msg);
                        }
                    }
                });
            }        
        }); 


        // delete item
        $(document.body).on('click','.delete_connector',function(event)
        {
            event.preventDefault();

            var hide = $(this).parent().parent();

            var table_id = $(this).attr('table_id');
            var doDelete = "<?php echo $this->lang->line('Do you want to detete this record?');?>";

            alertify.confirm('<?php echo $this->lang->line("are you sure");?>',doDelete, 
              function(){ 
                $.ajax({
                    type:'POST' ,
                    url:"<?php echo site_url();?>json_api_connector/ajax_delete_connector_info",
                    data:{table_id:table_id},
                    success:function(response)
                    {
                        if(response =='1') 
                        {
                            $(hide).addClass('hidden');
                            alertify.success('<?php echo $this->lang->line("your data data has been successfully deleted from database.") ?>');
                        }
                        else
                        {                           
                            $(hide).removeClass('hidden');
                            alertify.error('<?php echo $this->lang->line("something went wrong, please try again.") ?>');
                        }
                    }

                });
              },
              function(){});
        });


        $(document.body).on('click','.view_connector',function(event)
        {
            event.preventDefault();
            $("#view_connector_info_modal").modal();

            var table_id = $(this).attr('table_id');
            $("#view_connector_info_modal").modal();

            $.ajax({
                type:'POST' ,
                url:"<?php echo site_url();?>json_api_connector/ajax_view_connector_info",
                data:{table_id:table_id},
                success:function(response)
                {
                    if(response) 
                    {
                        $("#info_modal").html(response);
                    }
                    else
                    {                           

                    }
                }

            });

        });

        $(document.body).on('change','#trigger_postbackid',function(event){
            event.preventDefault();

            if($("#page_table_id").val()=='')
            {
                $(this).prop('checked', false); 
                var error = '<?php echo $this->lang->line("Please select at least one page"); ?>';
                alertify.alert('<?php echo $this->lang->line("Alert")?>',error,function(){});

            } else
                {
                if($(this).prop('checked')==true)
                {
                    $("#postback_div").css('display','block');

                } else 
                {   
                    $("#postback_div").css('display','none');
                }
            }

        });


        $j("#postback").multipleSelect({
            filter: true,
            multiple: true
        });


        $(document.body).on('change','#page_table_id',function(event)
        {
            event.preventDefault();

            var page_id  = $(this).val();

            $.ajax({
                url: base_url+'json_api_connector/find_page_postback',
                type: 'POST',
                data: {page_id: page_id},
                success:function(response)
                {   
                    $("#postback_div").html(response);
                }
            })

        });


        $(document.body).on('change','#updated_page_table_id',function(event)
        {
            event.preventDefault();

            var page_id  = $(this).val();
            var table_id = $('#table_id').val();

            $.ajax({
                url: base_url+'json_api_connector/find_page_update_postback',
                type: 'POST',
                data: {page_id: page_id,table_id:table_id},
                success:function(response)
                {   
                    $("#updated_postback_div").html(response);
                }
            })

        });


        $(document.body).on('change','#trigger_postbackid_updated',function(event)
        {
            event.preventDefault();

             if($(this).prop('checked')==true)
            {
                $("#updated_postback_div").css('display','block');

            } else 
            {
                $("#updated_postback_div").css('display','none');
            }

        });

            
    });

</script>