<?php
/*
Addon Name: Json API Connector
Unique Name: Json_api_connector
Module ID: 258
Project ID: 23
Addon URI: http://getfbinboxer.com
Author: Xerone IT
Author URI: http://xeroneit.net
Version: 1.1
Description: JSON API connector sends JSON data to any of your provided external URL like Zapier
*/
require_once("application/controllers/Home.php"); // loading home controller
class Json_api_connector extends Home
{
    public $addon_data=array();
    public function __construct()
    {
        parent::__construct();
        // getting addon information in array and storing to public variable
        // addon_name,unique_name,module_id,addon_uri,author,author_uri,version,description,controller_name,installed
        //------------------------------------------------------------------------------------------
        $addon_path=APPPATH."modules/".strtolower($this->router->fetch_class())."/controllers/".ucfirst($this->router->fetch_class()).".php"; // path of addon controller
        $addondata=$this->get_addon_data($addon_path); 
        $this->member_validity();
        $this->addon_data=$addondata;
        $this->user_id=$this->session->userdata('user_id'); // user_id of logged in user, we may need it
        $function_name=$this->uri->segment(2);
        // all addon must be login protected
        //------------------------------------------------------------------------------------------
        if ($this->session->userdata('logged_in')!= 1) redirect('home/login', 'location');          
        // if you want the addon to be accessed by admin and member who has permission to this addon
        //-------------------------------------------------------------------------------------------
        if(isset($addondata['module_id']) && is_numeric($addondata['module_id']) && $addondata['module_id']>0)
        {
            if($this->session->userdata('user_type') != 'Admin' && !in_array($addondata['module_id'],$this->module_access))
            {
                redirect('home/login_page', 'location');
                exit();
            }
        }
    }


  public function index()
  {
    $this->json_api_connector_dashbaord();
  }

  public function json_api_connector_dashbaord()
  {

    $page_info = array();
    $join      = array('messenger_bot_user_info'=>'messenger_bot_page_info.messenger_bot_user_info_id=messenger_bot_user_info.id,left');
    $page_info = $this->basic->get_data('messenger_bot_page_info',array('where'=>array('messenger_bot_page_info.user_id'=>$this->user_id,'bot_enabled'=>'1')),array('messenger_bot_page_info.id','page_name','name','page_id'),$join);

     // echo "<pre>"; print_r($page_info);

    $connector_data = $this->basic->get_data("messenger_bot_thirdparty_webhook",array("where"=>array("user_id"=>$this->user_id)),'','','','');

    $page_postback_info = array();
    $join1      = array('messenger_bot_page_info'=>'messenger_bot_postback.page_id=messenger_bot_page_info.id,left',);
    $page_postback_info = $this->basic->get_data('messenger_bot_postback',array('where'=>array('messenger_bot_page_info.user_id'=>$this->user_id,'bot_enabled'=>'1')),array('messenger_bot_postback.*','page_name'),$join1);

    $data['connector_data'] = $connector_data;
    $data['page_title']     = $this->lang->line("Json API Connector");
    $data['page_info']      = $page_info;
    $data['body']           = "json_api_connector_dashboard";

    $this->_viewcontroller($data); 
  }

  public function ajax_connector_info_saving()
  {

    $ext = array();

    $post=$_POST;

    foreach ($post as $key => $value) 
    {
        $$key=$value;
    }

    // get the page_name,page_id by page_table_id
    $page_name = $this->basic->get_data('messenger_bot_page_info',array('where'=>array('user_id'=>$this->user_id,'id'=>$page_table_id,'bot_enabled'=>'1')),array('page_name','page_id'));

    $alldata = array();

    $alldata['name']          = trim($connector_name);
    $alldata['user_id']       = $this->user_id;
    $alldata['webhook_url']   = trim($webhook_url);
    $alldata['variable_post'] = implode(',',$variable_post);
    $alldata['page_id']       = $page_name[0]['page_id'];
    $alldata['page_name']     = $page_name[0]['page_name'];
    $alldata['added_date']    = date("Y-m-d H:i:s");

    if(in_array("trigger_postbackid",$field))
    {
      $find_trigger_postback = array_search('trigger_postbackid',$field);
      unset($field[$find_trigger_postback]);
    
      if(!empty($postback))
      {
        foreach ($postback as $single_postback) 
        {
          array_push($field,$single_postback);
        }
      }

    }


    $table = 'messenger_bot_thirdparty_webhook';

    if($this->basic->insert_data($table,$alldata))
    {
      $inserted_id = $this->db->insert_id();
      $triggered_table = array();

      foreach ($field as $value) 
      {
        $triggered_table['webhook_id']     = $inserted_id;
        $triggered_table['trigger_option'] = $value;
        $this->basic->insert_data('messenger_bot_thirdparty_webhook_trigger',$triggered_table);
      }

      $ext['result'] = 1;
      $ext['msg'] = "<div class='alert alert-success'><i class='fa fa-check-circle'></i> ".$this->lang->line("Your given information has been added successfully.")."</div><br>";

    } else 
    {
      $ext['result'] = 0;
      $ext['msg']    = "<div class='alert alert-danger'><i class='fa fa-times'></i> ".$this->lang->line("something went wrong,please try again.")."</div>";
    }

    echo json_encode($ext);

  }


  public function find_page_postback()
  {
    if(!$_POST) exit();

    $data = array();

    $pageID = $this->input->post("page_id");

    $postback_infos = $this->basic->get_data("messenger_bot_postback",array('where'=>array('page_id'=>$pageID)));

    $html = '<div class="col-xs-12">
                <label><i class="fa fa-retweet"></i> '.$this->lang->line("Choose Postback ID").'</label>
                <div class="form-group">
                <select multiple="multiple" name="postback[]" id="postback" class="form-control">
                <script>
                  $j("#postback").multipleSelect({
                      filter: true,
                      multiple: true
                  }); 
                </script>';
                if(!empty($postback_infos))
                { 
                  foreach($postback_infos as $postback)
                  {
                      $html .="<option value='trigger_postback_{$postback['postback_id']}'>{$postback['postback_id']}</option>";
                  }
                } else
                {
                  $html .='<span class="orange">No Postback ID Record Found for this page.</span>';
                }

    $html .='</select></div>
              </div>';
    echo $html;
    
  }

  public function ajax_view_connector_info()
  {
    $table_id       = $this->input->post('table_id',true);
    $table          = 'messenger_bot_thirdparty_webhook';
    $where['where'] = array('id'=>$table_id,'user_id'=>$this->user_id);
    $result         = $this->basic->get_data($table,$where);
    $webhook_id     = $result[0]['id'];
    $name           = $result[0]['name'];
    $webhook_url    = $result[0]['webhook_url'];
    $variable_post  = $result[0]['variable_post'];
    $page_name      = $result[0]['page_name'];
    $last_trigger   = $result[0]['last_trigger_time'];
    $trigger_option = $this->basic->get_data('messenger_bot_thirdparty_webhook_trigger',array('where'=>array('webhook_id'=>$webhook_id)),array('trigger_option'));

    $trigger   = implode(',', array_map(function($el){ return $el['trigger_option']; }, $trigger_option));
    $triggered = str_replace(array('trigger_','_'),' ',$trigger);

    $last_activity = $this->basic->get_data('messenger_bot_thirdparty_webhook_activity',array('where'=>array('webhook_id'=>$webhook_id)),$select='',$join='',$limit='',$start=NULL,$order_by='id desc');

    if($result != '')
    {
      $str = '<div class="row">
                <div class="col-sm-6 col-xs-12">
                  <ul class="list-group">
                    <li class="list-group-item">
                      <div class="row">
                        <div class="col-xs-3"><strong>'.$this->lang->line("Name").':</strong></div>
                        <div class="col-xs-8"><small>'.$name.'</small></div>
                      </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row">
                          <div class="col-xs-3"><strong>'.$this->lang->line("Webhook URL").':</strong></div>
                          <div class="col-xs-8"><small>'.$webhook_url.'</small></div>
                        </div>
                    </li>
                    <li class="list-group-item">
                      <div class="row">
                        <div class="col-xs-3"><strong>'.$this->lang->line("Post Variables").':</strong></div>
                        <div class="col-xs-8"><small>'.str_replace('_',' ',$variable_post).'</small></div>
                      </div>
                    </li>
                  </ul>
                </div>
                <div class="col-sm-6 col-xs-12">
                  <ul class="list-group">
                    <li class="list-group-item">
                      <div class="row">
                        <div class="col-xs-4"><strong>'.$this->lang->line("Page Name").' :</strong></div>
                        <div class="col-xs-8"><small>'.$page_name.'</small></div>
                      </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row">
                          <div class="col-xs-4"><strong>'.$this->lang->line("Triggered Options").' :</strong></div>
                          <div class="col-xs-8"><small>'.$triggered.'</small></div>
                        </div>
                    </li>
                    <li class="list-group-item">
                      <div class="row">
                        <div class="col-xs-4"><strong>'.$this->lang->line("Last Triggered Time").' :</strong></div>
                        <div class="col-xs-8"><small>'.$last_trigger.'</small></div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div><hr>';

      $str.= '<div class="box box-widget widget-user-2" style="box-shadow:none !important;">
              <div class="widget-user-header" style="border-radius: 0;">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                        <h4 class="widget-user-username dynamic_font_color" style="margin-left:0px;"><i class="fa fa-flag-checkered"></i> '.$this->lang->line("Last 10 Activities").'</h4></div></div></div>';

      if(empty($last_activity)) $str.="<h4 class='text-center'>".$this->lang->line('No Data found.')."</h4>";
      else 
      {
        $str.='<script>
                      $(".table-responsive").mCustomScrollbar({
                        autoHideScrollbar:true,
                        theme:"dark-3",
                        axis:"x"
                      });
                  </script>

                  <div class="box-footer" style="border-radius: 0;padding:20px;border-top:none;">
                    <div class="table-responsive" style="overflow:hidden">
                      <table class="table table-bordered table-condensed" id="settings_data_table">
                      <thead>
                        <tr>
                          <th class="text-center">'.$this->lang->line("SN").'</th>
                          <th class="text-center">'.$this->lang->line("Http Code").'</th>
                          <th class="text-center">'.$this->lang->line("Curl Error").'</th>
                          <th class="text-center">'.$this->lang->line("Post Data").'</th>
                          <th class="text-center">'.$this->lang->line("Post Time").'</th>
                        </tr>
                      </thead>
                      <tbody>';
        $i = 0;
        foreach ($last_activity as $value) 
        {
            $i++;
            $str.= '<tr>
                      <td class="text-center" nowrap>'.$i.'</td>
                      <td class="text-center" nowrap>'.$value['http_code'].'</td>
                      <td class="text-center" nowrap>'.$value['curl_error'].'</td>
                      <td class="text-center" nowrap>'.$value['post_data'].'</td>
                      <td class="text-center" nowrap>'.$value['post_time'].'</td>
                    </tr>';
        }

        $str.='</table></div></div></div></div>';
      }
      
      echo $str;
    } 
  }

  public function ajax_get_update_connector_info()
  {
      $table_id       = $this->input->post("table_id");
      $table          = 'messenger_bot_thirdparty_webhook';
      $where['where'] = array('id'=>$table_id);
      $info           = $this->basic->get_data($table,$where);
      $connector_name = $info[0]['name'];
      $webhook_url    = $info[0]['webhook_url'];
      $webhook_id     = $info[0]['id'];
      $pageid         = $info[0]['page_id'];

      // for postback infos
      $triggered_postback_val = array();
      $pageInfoPostack = $this->basic->get_data('messenger_bot_page_info',array('where'=>array('page_id'=>$pageid,'bot_enabled'=>'1')));
      $get_page_postbacks = $this->basic->get_data('messenger_bot_postback',array('where'=>array('page_id'=>$pageInfoPostack[0]['id'])));
      $triggered_info = $this->basic->get_data('messenger_bot_thirdparty_webhook_trigger',array('where'=>array('webhook_id'=>$webhook_id)),array('trigger_option'));

      foreach ($triggered_info as $single_triggered_postback) 
      {
        foreach ($single_triggered_postback as $value) 
        {
          array_push($triggered_postback_val,$value);
        }
      }


      $triggered_val = array();

      foreach ($triggered_info as $triggered) 
      {
        foreach ($triggered as $value) 
        {
          array_push($triggered_val,$value);
        }
      }


      // tiggered value section
      if(in_array("trigger_email",$triggered_val)) $checked_email = "checked";
      else $checked_email = "";
      if(in_array("trigger_phone_number",$triggered_val)) $checked_phone = "checked";
      else $checked_phone = "";
      if(in_array("trigger_location",$triggered_val)) $checked_location= "checked";
      else $checked_location= "";

      foreach($triggered_val as $string) 
      {
          if(strpos($string, 'trigger_postback') !== FALSE)
          {
             $checked_postback= "checked";
             break;
          }
          else
          {
            $checked_postback= "";
          }
      }


      // for checking the sending data
      $updated_variable_post = explode(",",$info[0]['variable_post']);

      if(in_array("psid",$updated_variable_post)) $psid = "checked";
      else $psid = "";
      if(in_array("first_name",$updated_variable_post)) $first_name = "checked";
      else $first_name = "";
      if(in_array("last_name",$updated_variable_post)) $last_name = "checked";
      else $last_name = "";
      if(in_array("subscribed_at",$updated_variable_post)) $subscribed_at = "checked";
      else $subscribed_at= "";
      if(in_array("email",$updated_variable_post)) $email = "checked";
      else $email = "";
      if(in_array("labels",$updated_variable_post)) $labels = "checked";
      else $labels = "";
      if(in_array("page_id",$updated_variable_post)) $page_id = "checked";
      else $page_id = "";
      if(in_array("page_name",$updated_variable_post)) $page_name = "checked";
      else $page_name = "";
      if(in_array("phone_number",$updated_variable_post)) $phone_number = "checked";
      else $phone_number = "";
      if(in_array("user_location",$updated_variable_post)) $user_location = "checked";
      else $user_location = "";
      if(in_array("trigger_postbackid",$updated_variable_post)) $postbackid = "checked";
      else $postbackid = "";

      // this for page list and selected page section
      $join     = array('messenger_bot_user_info'=>'messenger_bot_page_info.messenger_bot_user_info_id=messenger_bot_user_info.id,left');
      $page_info = $this->basic->get_data('messenger_bot_page_info',array('where'=>array('messenger_bot_page_info.user_id'=>$this->user_id,'bot_enabled'=>'1')),array('messenger_bot_page_info.id','page_name','name','page_id'),$join);

      $update_form = '<form id="json_api_connector_update_form" action="" method="POST">
                          <input type="hidden" name="table_id" id="table_id" value="'.$table_id.'">';



      // name section
      $update_form .= '<div class="row margin_div">
                          <div class="col-xs-12">
                              <div class="form-group">
                                  <label for="name"><i class="fa fa-flag"></i> '.$this->lang->line("Connection Name").'<span style="color:red"> *</span></label>
                                  <input type="text" class="form-control" id="connector_name" name="connector_name" value="'.$connector_name.'">
                              </div>
                          </div>
                      </div>';

      // webhook url section
      $update_form .= '<div class="row margin_div">
                          <div class="col-xs-12">
                              <div class="form-group">
                                  <label for="webhook_url"><i class="fa fa-globe"></i> '.$this->lang->line("Webhook URL").'<span style="color:red"> *</span></label>
                                  <input type="text" class="form-control" id="updated_webhook_url" name="updated_webhook_url" value="'.$webhook_url.'">
                              </div>
                          </div>
                      </div>';

      // page selection
      $update_form .= '<div class="row margin_div"> 
                          <div class="col-xs-12"> 
                              <div class="form-group">
                                <label><i class="fa fa-sticky-note"></i> '.$this->lang->line("Please select a page").'<span style="color:red"> *</span></label>
                                  <select name="updated_page_table_id" id="updated_page_table_id" class="form-control">';
                                    foreach ($page_info as $page_list) 
                                    {
                                        if ($page_list['page_id'] == $info[0]['page_id'])
                                            $update_form .='<option value="'.$page_list['id'].'" selected>'.$info[0]['page_name'].'</option>';
                                        else
                                            $update_form .='<option value="'.$page_list['id'].'">'.$page_list['page_name'].'</option>'; 
                                    }

      $update_form .= '</select></div></div></div>';

      // triggered options
      $update_form .= '<div class="row margin_div">
                          <div class="col-xs-12">
                            <label><i class="fa fa-sitemap"></i> '.$this->lang->line("What Field Change Trigger Webhook").'<span style="color:red"> *</span></label>
                            <div class="row">
                              <div class="col-xs-12 col-md-2">
                                <label class="checkbox-inline">
                                <input type="checkbox" value="trigger_email" id="trigger_email" name="updated_field[]" '.$checked_email.'>'.$this->lang->line("email").'</label>
                              </div>
                              <div class="col-xs-12 col-md-4">
                                <label class="checkbox-inline">
                                <input type="checkbox" value="trigger_phone_number" id="trigger_phone_number" name="updated_field[]" '.$checked_phone.'>'. $this->lang->line("phone number").'</label>
                              </div>
                              <div class="col-xs-12 col-md-3">
                                <label class="checkbox-inline">
                                <input type="checkbox" value="trigger_location" id="trigger_location" name="updated_field[]" '.$checked_location.'>'.$this->lang->line("location").'</label>
                              </div>
                              <div class="col-xs-12 col-md-3">
                                <label class="checkbox-inline">
                                  <input type="checkbox" value="trigger_postbackid" id="trigger_postbackid_updated" name="updated_field[]" '.$checked_postback.'>'.$this->lang->line("Postback ID").'</label>
                              </div>
                            </div>
                          </div>
                      </div>';

      // postback ID div
      $update_form .= '<div class="row margin_div" id="updated_postback_div">
                          <div class="col-xs-12">
                            <label><i class="fa fa-retweet"></i> '.$this->lang->line("Page Postback ID").'</label>
                            <div class="form-group">
                            <select multiple="multiple" class="form-control" name="postback[]" id="postback">
                            <script>
                              $j("#postback").multipleSelect({
                                  filter: true,
                                  multiple: true
                              }); 
                            </script>';
                      if(!empty($get_page_postbacks))
                      { 
                        foreach ($get_page_postbacks as $postback) 
                        { 
                          if(in_array("trigger_postback_".$postback['postback_id'],$triggered_postback_val)) $checked="selected";
                          else $checked = "";

                          $update_form .="<option value='trigger_postback_{$postback['postback_id']}' {$checked}>{$postback['postback_id']}</option>";
                        }

                      } else
                      {
                        $update_form .='<span class="red">No Postback ID Record Found for this page.</span>';
                      }

       $update_form .= '</select></div>
                </div></div>';

      // sending data
      $update_form .= '<div class="row">
                          <div class="col-xs-12">
                            <label><i class="fa fa-send"></i> '.$this->lang->line("Which Data You Want To Send").'<span style="color:red"> *</span></label>';
      $update_form .= '<div class="row">
                          <div class=" col-xs-12 col-md-3">
                            <label class="checkbox-inline">
                                <input type="checkbox" value="psid" id="psid" name="updated_variable_post[]" '.$psid.'> '
                                .$this->lang->line("PSid").'
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="subscribed_at" id="subscribed_at" name="updated_variable_post[]" '.$subscribed_at.'> '
                                .$this->lang->line("Subscribed At").'
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="postbackid" id="phone_number" name="updated_variable_post[]" '.$postbackid.'> '
                                .$this->lang->line("Postback ID").'
                            </label>

                          </div>';
      $update_form .= '<div class=" col-xs-12 col-md-3">
                          <label class="checkbox-inline">
                              <input type="checkbox" value="first_name" id="first_name" name="updated_variable_post[]" '.$first_name.'> '
                              .$this->lang->line("First Name").'
                          </label>
                          <label class="checkbox-inline">
                              <input type="checkbox" value="last_name" id="last_name" name="updated_variable_post[]" '.$last_name.'> '
                              .$this->lang->line("Last Name").'
                          </label>
                          <label class="checkbox-inline">
                              <input type="checkbox" value="email" id="email" name="updated_variable_post[]" '.$email.'> '
                              .$this->lang->line("Email").'
                          </label>
                      </div>';
      $update_form  .= '<div class=" col-xs-12 col-md-4">
                            <label class="checkbox-inline">
                                <input type="checkbox" value="page_id" id="page_id" name="updated_variable_post[]" '.$page_id.'> '
                                .$this->lang->line("Page ID").'
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="page_name" id="page_name" name="updated_variable_post[]" '.$page_name.'> '
                                .$this->lang->line("Page Name").'
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="phone_number" id="phone_number" name="updated_variable_post[]" '.$phone_number.'> '
                                .$this->lang->line("Phone number").'
                            </label>
                        </div>';
      $update_form .= '<div class=" col-xs-12 col-md-2">
                          <label class="checkbox-inline">
                              <input type="checkbox" value="user_location" id="user_location" name="updated_variable_post[]" '.$user_location.'>'
                              .$this->lang->line("Location").'
                          </label>
                          <label class="checkbox-inline">
                              <input type="checkbox" value="labels" id="labels" name="updated_variable_post[]" '.$labels.'> '
                              .$this->lang->line("Labels").'
                          </label>
                          
                        </div>                 
                      </div>
                    </div>
                </div><hr>';

      $update_form .= '<div class="row"><div class="text-center" id="updated_response_status"></div></div><br>';

      $update_form .= '<div class="row"><div class="col-xs-12 text-center"><button id="save_updated_connector_infos" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> '.$this->lang->line("update").'</button></div></div></form>';

      echo $update_form;
  }

  public function find_page_update_postback()
  {
      $triggered_postback_val = array();

      $pageID = $this->input->post("page_id",true);
      $tableID = $this->input->post('table_id',true);

      $triggered_postback_info = $this->basic->get_data("messenger_bot_thirdparty_webhook_trigger",array('where'=>array('webhook_id'=>$tableID)),array('trigger_option'));

      $postback_infos = $this->basic->get_data("messenger_bot_postback",array('where'=>array('page_id'=>$pageID)));

      foreach ($triggered_postback_info as $single_triggered_postback) 
      {
        foreach ($single_triggered_postback as $value) 
        {
          array_push($triggered_postback_val,$value);
        }
      }

      $html = '<div class="col-xs-12">
                  <label><i class="fa fa-retweet"></i> '.$this->lang->line("Choose Postback ID").'</label>
                  <div class="form-group">
                  <select multiple="multiple" name="postback[]" id="postback" class="form-control">
                  <script>
                    $j("#postback").multipleSelect({
                        filter: true,
                        multiple: true
                    }); 
                  </script>
                  ';
      if(!empty($postback_infos))
      { 
        foreach ($postback_infos as $postback) 
        {
          if(in_array("trigger_postback_".$postback['postback_id'],$triggered_postback_val)) $checked="selected";
          else $checked = "";

          $html .="<option value='trigger_postback_{$postback['postback_id']}' {$checked}>{$postback['postback_id']}</option>";
        }

      } else
      {
        $html .='<span class="red">No Postback ID Record Found for this page.</span>';
      }

      $html .='</select></div>
                </div>';
                
      echo $html;
  }


  public function ajax_connector_info_updating()
  {
    $post_data = $_POST;

    foreach ($post_data as $key => $value) { $$key = $value; }

    $update_data = array();
    $table_id                     = $table_id;
    $update_data['name']          = $connector_name;
    $update_data['user_id']       = $this->user_id;

    $page_name                    = $this->basic->get_data('messenger_bot_page_info',array('where'=>array('user_id'=>$this->user_id,'id'=>$updated_page_table_id,'bot_enabled'=>'1')),array('page_name','page_id'));

    $update_data['page_name']     = $page_name[0]['page_name'];
    $update_data['page_id']       = $page_name[0]['page_id'];
    $update_data['webhook_url']   = $updated_webhook_url;
    $update_data['variable_post'] = implode(',',$updated_variable_post);

    if(in_array("trigger_postbackid",$updated_field))
    {
      $find_trigger_postback = array_search('trigger_postbackid',$updated_field);
      unset($updated_field[$find_trigger_postback]);
    
      if(!empty($postback))
      {
        foreach ($postback as $single_postback) 
        {
          array_push($updated_field,$single_postback);
        }
      }
    }

 
    $updated_table_name           = 'messenger_bot_thirdparty_webhook';
    $update_data_where            = array('id'=>$table_id);

    $success = array();

    if($this->basic->update_data($updated_table_name,$update_data_where,$update_data))
    {
      $update_trigger_data = array();

      $get_triggered_data = $this->basic->delete_data('messenger_bot_thirdparty_webhook_trigger',array('webhook_id'=>$table_id));

      foreach ($updated_field as $single_field) 
      {
        $trigger_table       = 'messenger_bot_thirdparty_webhook_trigger';
        $update_where        = array('webhook_id'=>$table_id);
        $update_trigger_data['webhook_id']     = $table_id;
        $update_trigger_data['trigger_option'] = $single_field;
        $this->basic->insert_data('messenger_bot_thirdparty_webhook_trigger',$update_trigger_data);
      }

      $success['result'] = 1;
      $success['msg']    = "<div class='alert alert-success'><i class='fa fa-check-circle'></i> ".$this->lang->line("Your given information has been updated successfully.")."</div><br>";

    } else
    {
      $success['result'] = 0;
      $success['msg']    = "<div class='alert alert-danger'><i class='fa fa-times'></i> ".$this->lang->line("something went wrong,please try again.")."</div><br>";
    }

    echo json_encode($success);    
  }


  public function ajax_delete_connector_info()
  {
    $table_id = $this->input->post("table_id",true);

    if($this->basic->delete_data('messenger_bot_thirdparty_webhook',array("id"=>$table_id)) && $this->basic->delete_data('messenger_bot_thirdparty_webhook_trigger',array("webhook_id"=>$table_id)))
    {
        echo "1";

    } else
    {
        echo "0";
    }
  }


  public function activate()
    {
        if(!$_POST) exit();
   
        $is_free_addon=true; 
        $addon_controller_name=ucfirst($this->router->fetch_class()); // here addon_controller_name name is Comment [origianl file is Comment.php, put except .php]
        $purchase_code=$this->input->post('purchase_code');
        if(!$is_free_addon)
        {
            $this->addon_credential_check($purchase_code,strtolower($addon_controller_name)); // retuns json status,message if error
        }  
        //this addon system support 2-level sidebar entry, to make sidebar entry you must provide 2D array like below
        $sidebar=array(); 
      // mysql raw query needed to run, it's an array, put each query in a seperate index, create table query must should IF NOT EXISTS
      $sql=array
      (
          0=>"CREATE TABLE IF NOT EXISTS `messenger_bot_thirdparty_webhook` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `name` varchar(100) CHARACTER SET utf8 NOT NULL,
              `user_id` int(11) NOT NULL,
              `page_id` varchar(50) NOT NULL,
              `page_name` varchar(250) CHARACTER SET utf8 NOT NULL,
              `webhook_url` text NOT NULL,
              `variable_post` text NOT NULL,
              `added_date` datetime NOT NULL,
              `last_trigger_time` datetime NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;",
          1=>"CREATE TABLE IF NOT EXISTS `messenger_bot_thirdparty_webhook_activity` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `http_code` varchar(10) NOT NULL,
              `webhook_id` int(11) NOT NULL,
              `curl_error` varchar(250) NOT NULL,
              `post_time` datetime NOT NULL,
              `post_data` text CHARACTER SET utf8 NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;",
          2=>"CREATE TABLE IF NOT EXISTS `messenger_bot_thirdparty_webhook_trigger` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `webhook_id` int(11) NOT NULL,
              `trigger_option` varchar(50) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;",
          3=>"UPDATE `modules` SET `extra_text` = '' , `limit_enabled`=>'0' WHERE `modules`.`id` = 258"        
      ); 
      if($this->db->table_exists('facebook_rx_fb_user_info')) // this is FB Inboxer
      {
          $sql[4]="INSERT INTO `menu_child_1` (`name`, `url`, `serial`, `icon`, `module_access`, `parent_id`, `have_child`, `only_admin`, `only_member`, `is_external`) VALUES ('json API Connection', 'json_api_connector/index', '7', 'fa fa-recycle', '258', '0', '0', '0', '0', '0');";
          $sql[5]="UPDATE menu_child_1 SET parent_id= (select id from menu WHERE module_access='200' AND have_child='1') WHERE url='json_api_connector/index';";
      }
      else // this is BOT Inboxer standalone
      {
          $sql[4]="INSERT INTO `menu` (`name`, `icon`, `url`, `serial`, `module_access`, `have_child`, `only_admin`, `only_member`, `add_ons_id`, `is_external`) VALUES ('JSON API Connection', 'fa fa-recycle', 'json_api_connector/index', '9', '258', '0', '0', '0', '', '0');";
          $sql[5]="UPDATE menu SET add_ons_id= (select id from add_ons WHERE unique_name='json_api_connector') WHERE url='json_api_connector/index';";
      }
      //send blank array if you does not need sidebar entry,send a blank array if your addon does not need any sql to run
      $this->register_addon($addon_controller_name,$sidebar,$sql,$purchase_code,"Messenger Bot - JSON API Connector"); 
  }

  public function deactivate()
  {  
      if($this->is_demo == '1')
      {
          if($this->session->userdata('user_type') == "Admin")
          {
              echo "<div class='alert alert-danger text-center'><i class='fa fa-ban'></i> This function is disabled from admin account in this demo!!</div>";
              exit();
          }
      }

      $addon_controller_name=ucfirst($this->router->fetch_class()); // here addon_controller_name name is Comment [origianl file is Comment.php, put except .php]

      if($this->db->table_exists('facebook_rx_fb_user_info'))
      {
          $this->db->query("DELETE FROM `menu_child_1` WHERE `url` = 'json_api_connector/index'");
      }
      else
      {
          $this->db->query("DELETE FROM `menu` WHERE `url` = 'json_api_connector/index'");
      }

      // only deletes add_ons,modules and menu, menu_child1 table entires and put install.txt back, it does not delete any files or custom sql
      $this->unregister_addon($addon_controller_name);         
  }

  public function delete()
  { 
      if($this->is_demo == '1')
      {
          if($this->session->userdata('user_type') == "Admin")
          {
              echo "<div class='alert alert-danger text-center'><i class='fa fa-ban'></i> This function is disabled from admin account in this demo!!</div>";
              exit();
          }
      }

      $addon_controller_name=ucfirst($this->router->fetch_class()); // here addon_controller_name name is Comment [origianl file is Comment.php, put except .php]
       // mysql raw query needed to run, it's an array, put each query in a seperate index, drop table/column query should have IF EXISTS
      $sql=array
      (
        0=>"DROP TABLE IF EXISTS `messenger_bot_thirdparty_webhook`;",
        1=>"DROP TABLE IF EXISTS `messenger_bot_thirdparty_webhook_activity`;",
        2=>"DROP TABLE IF EXISTS `messenger_bot_thirdparty_webhook_trigger`;"
      );  

      if($this->db->table_exists('facebook_rx_fb_user_info'))
      {
          $this->db->query("DELETE FROM `menu_child_1` WHERE `url` = 'json_api_connector/index'");
      }
      else
      {
          $this->db->query("DELETE FROM `menu` WHERE `url` = 'json_api_connector/index'");
      }
      
      // deletes add_ons,modules and menu, menu_child1 table ,custom sql as well as module folder, no need to send sql or send blank array if you does not need any sql to run on delete
      $this->delete_addon($addon_controller_name,$sql);         
  }

}
