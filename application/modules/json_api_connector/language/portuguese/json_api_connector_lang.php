<?php
$lang = array(
    "Json API Connector" => "Conector da API do Json",
    "JSON API Connector is for sending data to only of your external URL. Once any specific field change is triggered, we will send JSON data to your provided webhook URL & it will wait for 10 second to get response in 10 seconds from your webhook URL. If we don't get response in 10 seconds,then we will send another post request to webhook. You can then process your data as you want. With this option, you can integrate with other third party app like zapier or create own custom application." => "O JSON API Connector é para enviar dados apenas para o seu URL externo. Uma vez que qualquer mudança de campo específica é acionada, nós enviaremos dados JSON para o seu URL do webhook fornecido e ele aguardará 10 segundos para receber a resposta em 10 segundos do seu URL do webhook. Se não obtivermos resposta em 10 segundos, enviaremos outra solicitação de postagem para o webhook. Você pode então processar seus dados como quiser. Com esta opção, você pode integrar com outro aplicativo de terceiros como zapier ou criar aplicativo personalizado próprio.",
    "Connection Lists" => "Listas de Conexões",
    "New Json API Connection" => "Nova conexão de API do Json",
    "No Data found." => "Nenhum dado encontrado.",
    "SN" => "SN",
    "Name" => "Nome",
    "Webhook URL" => "URL do Webhook",
    "Actions" => "Ações",
    "Page Name" => "Nome da página",
    "Added Time" => "Adicionado tempo",
    "Last Triggered Time" => "Último Tempo Disparado",
    "view" => "Visão",
    "Delete" => "Excluir",
    "Add New Connection" => "Adicionar nova conexão",
    "Connection Name" => "Nome da conexão",
    "Enter your connection name" => "Digite seu nome de conexão",
    "Enter your webhook URL" => "Insira seu URL do webhook",
    "Please select a page" => "Por favor, selecione uma página",
    "What Field Change Trigger Webhook" => "Que Troca de Campo Troca Webhook",
    "email" => "o email",
    "phone number" => "número de telefone",
    "location" => "localização",
    "Which Data You Want To Send" => "Quais dados você deseja enviar",
    "PSid" => "PSid",
    "First Name" => "Primeiro nome",
    "Last Name" => "Último nome",
    "Subscribed At" => "Subscrito em",
    "Email" => "O email",
    "Labels" => "Rótulos",
    "Page ID" => "ID da página",
    "Phone Number" => "Número de telefone",
    "User Location" => "Localização do usuário",
    "save" => "Salve ",
    "Json API Connector Info" => "Informações do conector da API do Json",
    "Update Connector" => "Atualizar conector",
    "Json API Connector." => "Conector da API do Json.",
    "Your given information has been added successfully." => "Suas informações fornecidas foram adicionadas com sucesso.",
    "something went wrong,please try again." => "Alguma coisa deu errado. Por favor tente outra vez.",
    "Post Variables" => "Postar variáveis",
    "Triggered Options" => "Opções acionadas",
    "Last 10 Activities" => "Últimas 10 atividades",
    "Http Code" => "Código Http",
    "Curl Error" => "Curl Error",
    "Post Data" => "Dados de postagem",
    "Post Time" => "Hora da postagem",
    "update" => "atualizar",
    "Your given information has been updated successfully." => "Suas informações fornecidas foram atualizadas com sucesso.",
);