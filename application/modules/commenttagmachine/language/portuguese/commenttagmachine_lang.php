<?php 
$lang = array (
  'Comment Tag Machine' => 'Comentário Tag Machine',
  'Comment Tag Machine - Comment & Bulk Tag' => 'Comentário Tag Machine - Comment & Bulk Tag',
  'Comment Tag Machine - Bulk Comment Reply' => 'Comentário Tag Machine - Bulk Comment Reply',
  'Enable Post' => 'Ativar postagem',
  'Create Campaign' => 'Criar campanha',
  'Create' => 'Crio',
  'Comment & Bulk Tag Report' => 'Comentário e relatório de etiqueta em massa',
  'Bulk Comment Reply Report' => 'Relatório de resposta de comentário em massa',
  'Enable Post : Page List' => 'Ativar postagem: lista de páginas',
  'Tag Enabled' => 'Tag ativado',
  'Enable Comment Tagging' => 'Ativar marcação de comentários',
  'Enable from Post List' => 'Habilitar a partir da lista de postagens',
  'enable by post id' => 'habilitar por identificação de postagem',
  'edit by post id' => 'editar por identificação da postagem',
  'Enable & Fetch Commenter' => 'Enable & Fetch Commenter',
  'Commenters' => 'Comentaristas',
  'Comments' => 'Comentários',
  'Latest Posts' => 'últimas postagens',
  'content' => 'conteúdo',
  'created' => 'criada',
  'Enabled successfully' => 'Ativado com sucesso',
  'post has been successfully enabled for tagging and commenter information has been fetched.' => 'O post foi ativado com sucesso para a marcação e as informações do comentador foram buscadas.',
  'page not found.' => 'página não encontrada.',
  'this post is already enabled for tagging.' => 'Esta publicação já está habilitada para marcação.',
  'Post List' => 'Lista de postagem',
  'Comment & Bulk Tag Campaign' => 'Campanha de comentários e palavras-chave',
  'Bulk Comment Reply Campaign' => 'Campanha de resposta de comentários em massa',
  'Comment & Bulk Tag Campaign Report' => 'Relatório de Campanha de Comentários e Análises em massa',
  'Bulk Comment Reply Campaign Report' => 'Relatório de Campanha de Respondência de Comentário em Massa',
  'Edit Comment & Bulk Tag Campaign' => 'Editar comentário e campanha de etiqueta de massa',
  'Edit Bulk Comment Reply Campaign' => 'Edit Bulk Comment Reply Campaign',
  'Comment & Bulk Tag' => 'Comentário e etiqueta em massa',
  'Bulk Comment Reply' => 'Responder em massa Responder',
  'Post Created' => 'Pós-criado',
  'Commenter Name' => 'Nome do comentarista',
  'Comment ID' => 'ID do comentário',
  'Last Comment ID' => 'ID do último comentário',
  'Comment Time' => 'Tempo de comentários',
  'Last Comment Time' => 'Tempo do último comentário',
  'Comment' => 'Comente',
  'Commenter List' => 'Lista de comentaristas',
  'Comment List' => 'Lista de comentários',
  'Re-scan Comments' => 'Re-digitalizar comentários',
  'Re-scan' => 'Re-scan',
  'Post Created From' => 'Post criado a partir de',
  'Post Created To' => 'Post criado para',
  'Re-scanned Successfully' => 'Re-digitalizado com sucesso',
  'post comments has been updated successfully.' => 'Os comentários dos post foram atualizados com sucesso.',
  'picture' => 'cenário',
  'Tag Content' => 'Conteúdo de Tag',
  'Content to bulk tag commenters.' => 'Conteúdo para comentaristas de tags em massa.',
  'Select Commenter Range' => 'Selecione o intervalo do comentarista',
  'This range is sorted by comment time in decending order.' => 'Esse intervalo é classificado por hora do comentário em ordem decrescente.',
  'Custom Input' => 'Entrada personalizada',
  'Start typing commenter names you want to excude from tag list' => 'Comece a digitar os nomes dos comentadores que você deseja excluir da lista de tags',
  'Do not tag these commenters' => 'Não marque esses comentaristas',
  'You can choose one or more. The commenters you choose here will be unlisted from this campaign and will not be tagged. Start typing a commenter name, it is auto-complete.' => 'Você pode escolher um ou mais. Os comentaristas que você escolher aqui não serão listados nesta campanha e não serão marcados. Comece a digitar o nome de um comentador, é auto-completo.',
  'Latest' => 'Mais recentes',
  'Tag List' => 'Lista de tags',
  'List of commenters which this campaign will tag' => 'Lista de comentaristas que esta campanha marcará',
  'Up to' => 'Até',
  'image/video for bulk tag comment' => 'imagem / vídeo para comentários de tag em massa',
  'image/video upload' => 'upload de imagem / vídeo',
  'upload image or video to embed with your bulk tag comment.' => 'Carregue imagem ou vídeo para incorporar com o seu comentário de etiqueta em massa.',
  'Campaign name is required.' => 'É necessário o nome da campanha.',
  'Tag content is required.' => 'É necessário o conteúdo da tag.',
  'You have not selected commenters.' => 'Você não selecionou comentaristas.',
  'Campagin has been created successfully.' => 'Campagin foi criado com sucesso.',
  'click here to see report.' => 'Clique aqui para ver o relatório.',
  'No subscribed commenter found.' => 'Nenhum comentador subscrito encontrado.',
  'Reply Content' => 'Responder Conteúdo',
  'Bulk comment reply content.' => 'Conteúdo de resposta de comentário em massa.',
  'upload image or video to embed with your comment reply.' => 'Carregue imagem ou vídeo para incorporar com a resposta do seu comentário.',
  'same user may comment multiple time, do you want to reply all of them or not.' => 'o mesmo usuário pode comentar várias vezes, quer responder a todos ou não.',
  'reply same commenter multiple times?' => 'responda o mesmo comentador várias vezes?',
  'Too frequent replies can be suspicious to Facebook. It is safe to use some seconds of delay. Zero means random delay.' => 'Respostas muito frequentes podem ser suspeitas para o Facebook. É seguro usar alguns segundos de atraso. Zero significa atraso aleatório.',
  'delay between two replies [seconds]' => 'atraso entre duas respostas [segundos]',
  'Reply content is required.' => 'Responder conteúdo é necessário.',
  'Tag Count' => 'Contagem de tags',
  'Instant' => 'Instante',
  'Attachment' => 'Anexo',
  'Click here to preview image/video attachment' => 'Clique aqui para visualizar a imagem / vídeo anexo',
  'Campagin has been updated successfully.' => 'Campagin foi atualizado com sucesso.',
  'Reply Count' => 'Contagem de resposta',
  'Replied at' => 'Respondido em',
  'Sent' => 'Enviei',
  'Bulk tagged comment has been posted successfully.' => 'O comentário marcado em massa foi postado com sucesso.',
  'See Comment' => 'Ver comentário',
  'Failed' => 'Falhou',
  'Reply Status' => 'Status de resposta',
  'This add-on requires HTTPS.' => 'Este complemento requer HTTPS.'
);
?>