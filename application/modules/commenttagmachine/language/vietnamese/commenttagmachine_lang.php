<?php 
$lang = array (
  'Comment Tag Machine' => 'Máy Tag Bình luận',
  'Comment Tag Machine - Comment & Bulk Tag' => 'Máy Tag Bình luận - Nhận xét & Thẻ Bulk',
  'Comment Tag Machine - Bulk Comment Reply' => 'Comment Machine Tag - Bulk Comment Trả lời',
  'Enable Post' => 'Bật Đăng',
  'Create Campaign' => 'Tạo chiến dịch',
  'Create' => 'Tạo nên',
  'Comment & Bulk Tag Report' => 'Báo cáo nhận xét & số lượng lớn',
  'Bulk Comment Reply Report' => 'Báo cáo Trả lời Hàng loạt Báo cáo Trả lời',
  'Enable Post : Page List' => 'Bật Đăng: Trang Danh sách',
  'Tag Enabled' => 'Đã Bật Thẻ',
  'Enable Comment Tagging' => 'Bật gắn thẻ nhận xét',
  'Enable from Post List' => 'Bật từ Danh sách bài đăng',
  'enable by post id' => 'bật theo id bài đăng',
  'edit by post id' => 'chỉnh sửa theo id bài đăng',
  'Enable & Fetch Commenter' => 'Bật và tìm kiếm bình luận',
  'Commenters' => 'Người bình luận',
  'Comments' => 'Bình luận',
  'Latest Posts' => 'Bài mới nhất',
  'content' => 'Nội dung',
  'created' => 'tạo',
  'Enabled successfully' => 'Đã bật thành công',
  'post has been successfully enabled for tagging and commenter information has been fetched.' => 'đăng đã được kích hoạt thành công cho việc gắn thẻ và nhận xét thông tin đã được tìm nạp.',
  'page not found.' => 'không tìm thấy trang.',
  'this post is already enabled for tagging.' => 'bài đăng này đã được kích hoạt để gắn thẻ.',
  'Post List' => 'Danh sách bài đăng',
  'Comment & Bulk Tag Campaign' => 'Chiến dịch Thẻ nhận xét & Số lượng lớn',
  'Bulk Comment Reply Campaign' => 'Chiến dịch trả lời hàng loạt',
  'Comment & Bulk Tag Campaign Report' => 'Báo cáo Chiến dịch Báo cáo & Nhóm Tốc độ',
  'Bulk Comment Reply Campaign Report' => 'Báo cáo chiến dịch trả lời hàng loạt',
  'Edit Comment & Bulk Tag Campaign' => 'Chỉnh sửa Chiến dịch Nhãn Thảo luận & Số lượng lớn',
  'Edit Bulk Comment Reply Campaign' => 'Chỉnh sửa Chiến dịch Trả lời Hàng loạt',
  'Comment & Bulk Tag' => 'Nhận xét & Số lượng lớn Thẻ',
  'Bulk Comment Reply' => 'Trả lời hàng loạt',
  'Post Created' => 'Tạo bài đăng',
  'Commenter Name' => 'Tên người bình luận',
  'Comment ID' => 'ID nhận xét',
  'Last Comment ID' => 'ID Nhận xét Mới nhất',
  'Comment Time' => 'Thời gian bình luận',
  'Last Comment Time' => 'Thời gian phản hồi cuối cùng',
  'Comment' => 'Bình luận',
  'Commenter List' => 'Danh sách bình luận',
  'Comment List' => 'Danh sách nhận xét',
  'Re-scan Comments' => 'Quét lại các nhận xét',
  'Re-scan' => 'Quét lại',
  'Post Created From' => 'Đăng Tạo từ',
  'Post Created To' => 'Đăng lên Tạo',
  'Re-scanned Successfully' => 'Đã quét lại thành công',
  'post comments has been updated successfully.' => 'đăng nhận xét đã được cập nhật thành công.',
  'picture' => 'hình ảnh',
  'Tag Content' => 'Nội dung Thẻ',
  'Content to bulk tag commenters.' => 'Nội dung cho các bình luận viên hàng loạt.',
  'Select Commenter Range' => 'Chọn Phạm vi bình luận',
  'This range is sorted by comment time in decending order.' => 'Phạm vi này được sắp xếp theo thời gian nhận xét theo thứ tự decending.',
  'Custom Input' => 'Nhập tùy chỉnh',
  'Start typing commenter names you want to excude from tag list' => 'Bắt đầu nhập tên người bình luận mà bạn muốn loại trừ khỏi danh sách từ khóa',
  'Do not tag these commenters' => 'Không gắn thẻ những bình luận này',
  'You can choose one or more. The commenters you choose here will be unlisted from this campaign and will not be tagged. Start typing a commenter name, it is auto-complete.' => 'Bạn có thể chọn một hoặc nhiều. Những người bình luận bạn chọn ở đây sẽ không công khai từ chiến dịch này và sẽ không được gắn thẻ. Bắt đầu nhập tên người bình luận, nó tự động hoàn tất.',
  'Latest' => 'Muộn nhất',
  'Tag List' => 'Danh sách Tag',
  'List of commenters which this campaign will tag' => 'Danh sách bình luận mà chiến dịch này sẽ gắn thẻ',
  'Up to' => 'Lên đến',
  'image/video for bulk tag comment' => 'hình ảnh / video cho nhận xét thẻ hàng loạt',
  'image/video upload' => 'tải lên hình ảnh / video',
  'upload image or video to embed with your bulk tag comment.' => 'tải lên hình ảnh hoặc video để nhúng với nhận xét thẻ hàng loạt của bạn.',
  'Campaign name is required.' => 'Tên chiến dịch là bắt buộc.',
  'Tag content is required.' => 'Nội dung thẻ là bắt buộc.',
  'You have not selected commenters.' => 'Bạn chưa chọn người bình luận.',
  'Campagin has been created successfully.' => 'Campagin đã được tạo thành công.',
  'click here to see report.' => 'bấm vào đây để xem báo cáo.',
  'No subscribed commenter found.' => 'Không tìm thấy người bình luận đã đăng ký.',
  'Reply Content' => 'Trả lời nội dung',
  'Bulk comment reply content.' => 'Thảo luận phản hồi hàng loạt nội dung.',
  'upload image or video to embed with your comment reply.' => 'tải lên hình ảnh hoặc video để nhúng với phản hồi bình luận của bạn.',
  'same user may comment multiple time, do you want to reply all of them or not.' => 'cùng một người sử dụng có thể nhận xét nhiều lần, bạn có muốn trả lời tất cả chúng hay không.',
  'reply same commenter multiple times?' => 'trả lời cùng một commenter nhiều lần?',
  'Too frequent replies can be suspicious to Facebook. It is safe to use some seconds of delay. Zero means random delay.' => 'Các câu trả lời quá thường xuyên có thể đáng nghi với Facebook. An toàn để sử dụng một vài giây của sự chậm trễ. Zero có nghĩa là sự chậm trễ ngẫu nhiên.',
  'delay between two replies [seconds]' => 'chậm trễ giữa hai lần trả lời [giây]',
  'Reply content is required.' => 'Nội dung trả lời là bắt buộc.',
  'Tag Count' => 'Số thẻ',
  'Instant' => 'Tức thì',
  'Attachment' => 'Tập tin đính kèm',
  'Click here to preview image/video attachment' => 'Nhấp vào đây để xem trước tập tin hình ảnh / video đính kèm',
  'Campagin has been updated successfully.' => 'Campagin đã được cập nhật thành công.',
  'Reply Count' => 'Số lượng trả lời',
  'Replied at' => 'Trả lời lúc',
  'Sent' => 'Gởi',
  'Bulk tagged comment has been posted successfully.' => 'Nhận xét được gắn thẻ hàng loạt đã được đăng thành công.',
  'See Comment' => 'Xem Nhận xét',
  'Failed' => 'Thất bại',
  'Reply Status' => 'Trạng thái Trả lời',
  'This add-on requires HTTPS.' => 'Tiện ích này yêu cầu HTTPS.'
);
?>