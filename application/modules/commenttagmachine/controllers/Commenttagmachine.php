<?php
/*
Addon Name: Comment Tag Machine
Unique Name: commenttagmachine
Module ID: 201
Project ID: 5
Addon URI: http://getfbinboxer.com
Author: Xerone IT
Author URI: http://xeroneit.net
Version: 2.0.2
Description: Comment Tag Machine
*/

require_once("application/controllers/Home.php"); // loading home controller

class Commenttagmachine extends Home
{
	public $addon_data=array();
    public function __construct()
    {
        parent::__construct();
        $this->load->config("commenttagmachine_config");
        // getting addon information in array and storing to public variable
        // addon_name,unique_name,module_id,addon_uri,author,author_uri,version,description,controller_name,installed
        //------------------------------------------------------------------------------------------
        $addon_path=APPPATH."modules/".strtolower($this->router->fetch_class())."/controllers/".ucfirst($this->router->fetch_class()).".php"; // path of addon controller
        $this->addon_data=$this->get_addon_data($addon_path); 

        $this->member_validity();

        $this->user_id=$this->session->userdata('user_id'); // user_id of logged in user, we may need it

        // all addon must be login protected
        //------------------------------------------------------------------------------------------
        // if ($this->session->userdata('logged_in')!= 1) redirect('home/login', 'location');          

        // if you want the addon to be accessed by admin and member who has permission to this addon
        //-------------------------------------------------------------------------------------------
        // if(isset($addon_data['module_id']) && is_numeric($addon_data['module_id']) && $addon_data['module_id']>0)
        // {
        //     if($this->session->userdata('user_type') != 'Admin' && !in_array($addon_data['module_id'],$this->module_access))
        //     redirect('home/login_page', 'location');
        // }

    }


    public function index()
  	{
          $this->page_list(); 
  	}

    public function page_list()
    {
        if($this->session->userdata('user_type') != 'Admin' && !in_array(202,$this->module_access) && !in_array(201,$this->module_access))
        redirect('home/login_page', 'location'); 

        $data['body'] = 'page_list';
        $data['page_title'] = $this->lang->line('Enable Post : Page List');
        $page_info = array();
        $page_list = $this->basic->get_data("facebook_rx_fb_page_info",array("where"=>array("facebook_rx_fb_user_info_id"=>$this->session->userdata("facebook_rx_fb_user_info"))),"","","","","page_name asc");
        if(!empty($page_list))
        {
            $i = 0;
            foreach($page_list as $value)
            {
                $enabled_post_list = $this->basic->get_data('tag_machine_enabled_post_list',array('where'=>array('page_info_table_id'=>$value['id'])));
                $page_list[$i]['enabled_post'] = count($enabled_post_list);
                $i++;
            }
        }

        $data["page_info"] = $page_list;
        $this->_viewcontroller($data);
    }

    public function import_latest_post()
    {
        if(!$_POST) exit();
        if($this->session->userdata('user_type') != 'Admin' && !in_array(202,$this->module_access) && !in_array(201,$this->module_access))
        exit();

        $table_id = $this->input->post('table_id');// page auto id
        $page_info = $this->basic->get_data("facebook_rx_fb_page_info",array("where"=>array("id"=>$table_id)));
        $respnse = array();
        $respnse['page_name'] = $page_info[0]['page_name'];
        $page_table_id = $page_info[0]['id'];

        $existing_data = array();
        $existing_data_info = $this->basic->get_data('tag_machine_enabled_post_list',array('where'=>array('facebook_rx_fb_user_info_id'=>$this->session->userdata("facebook_rx_fb_user_info"),'page_info_table_id'=>$page_table_id)));

        if(!empty($existing_data_info))
        {
            foreach($existing_data_info as $value)
            {
                $existing_data[$value['post_id']] = array("id"=>$value['id'],"post_id"=>$value['post_id'],"commenter_count"=>$value['commenter_count'],"comment_count"=>$value["comment_count"]);
            }
        }
        $existing_post_id=array_keys($existing_data);

        $page_id = $page_info[0]['page_id'];
        $access_token = $page_info[0]['page_access_token'];

        try
        {
            $this->load->library("fb_rx_login");
            $post_list = $this->fb_rx_login->get_postlist_from_fb_page($page_id,$access_token);
       
            if(isset($post_list['data']) && empty($post_list['data'])){
                $respnse['message'] = "<h3><div class='alert alert-danger text-center'>".$this->lang->line("There is no post on this page.")."</div></h3>";
            }
            else if(!isset($post_list['data']))
            {
                $respnse['message'] = "<h3><div class='alert alert-danger text-center'>".$this->lang->line("something went wrong, please try again.")."</div></h3>";
            }
            else
            {
                $str='<script>
                  $j(document).ready(function() {
                      $(".table-responsive").mCustomScrollbar({
                          autoHideScrollbar:true,
                          theme:"3d-dark",          
                          axis: "x"
                      });   
                  });
               </script>';

                $str .= "<div id='sync_commenter_info_response' style='padding:10px 0;'></div>
                        <div class='table-responsive'>                    
                            <table class='table table-bordered table-striped'>                                
                                <thead>
                                    <tr>
                                        <th>{$this->lang->line("sl")}</th>
                                        <th class='text-center'>{$this->lang->line("post id")}</th>
                                        <th class='text-center'>{$this->lang->line("created")}</th>
                                        <th class='text-center'>{$this->lang->line("action")}</th>
                                        <th>{$this->lang->line("content")}</th>
                                    </tr>
                                </thead>
                                <tbody>";
                $i = 1;
                foreach($post_list['data'] as $value)
                {                   
                    $message = isset($value['message']) ? $value['message'] : '';
                    $encoded_message=htmlspecialchars($message);
                    if(strlen($message) >= 200)
                        $message = substr($message, 0, 200).'...';
                    else $message = $message;

                    if(in_array($value['id'], $existing_post_id))
                    {
                        $button = "<label class='label label-warning'><i class='fa fa-check'></i> ".$this->lang->line("Already Enabled")." [".$existing_data[$value['id']]['commenter_count']." ".$this->lang->line('Commenters')." | ".$existing_data[$value['id']]['comment_count']." ".$this->lang->line("Comments")."]</label>";
                    }
                    else  $button = "<button class='btn btn-sm btn-outline-success sync_commenter_info' post-description='".$encoded_message."' post-created-at='".$value['created_time']['date']."' id='".$table_id.'-'.$value['id']."' page_table_id='".$table_id."' post_id='".$value['id']."'><i class='fa fa-check-square'></i> ".$this->lang->line('Enable & Fetch Commenter')."</button>";
                    $str .= "
                                <tr>
                                    <td>".$i."</td>
                                    <td><a target='_BLANK' href='http://facebook.com/".$value['id']."'>".$value['id']."</a></td>
                                    <td>".substr($value['created_time']['date'], 0, 19)."</td>
                                    <td>".$button."</td>
                                    <td class='text-left'>".mb_convert_encoding($message, 'UTF-8', 'UTF-8')."</td>
                                </tr>
                            ";
                    $i++;                
                }
                $str .= "
                        </tbody>
                    </table>
                </div>";

                $respnse['message'] = $str;
            }

        }
        catch(Exception $e) 
        {
          $error_msg = "<div class='alert alert-danger text-center'><i class='fa fa-remove'></i> ".$e->getMessage()."</div>";
          $respnse['message'] = $error_msg;
        }

        echo json_encode($respnse);
    }

    public function sync_commenter_info($value='')
    {
       if(!$_POST) exit();
       if($this->session->userdata('user_type') != 'Admin' && !in_array(202,$this->module_access) && !in_array(201,$this->module_access))
       exit(); 

       $page_id=$this->input->post('page_id');
       $post_id=$this->input->post('post_id');
       $post_created_at=$this->input->post('post_created_at');
       $post_description=$this->input->post('post_description');
       $post_description=htmlspecialchars_decode($post_description);
       
       $page_info = $this->basic->get_data("facebook_rx_fb_page_info",array("where"=>array("id"=>$page_id,"user_id"=>$this->user_id)));
       $page_access_token=isset($page_info[0]['page_access_token']) ? $page_info[0]['page_access_token'] : "";
       $fb_page_id=isset($page_info[0]['page_id']) ? $page_info[0]['page_id'] : "";
       $page_name=isset($page_info[0]['page_name']) ? $page_info[0]['page_name'] : "";
       $page_profile=isset($page_info[0]['page_profile']) ? $page_info[0]['page_profile'] : "";

       if($page_access_token=='')
       {
            echo json_encode(array('status'=>'0','message'=>"<i class='fa fa-remove'></i> ".$this->lang->line("something went wrong, please try again.")));
            exit();
       }
       $this->load->library("commenttagmachine_class");
       $comment_list = $this->commenttagmachine_class->get_all_comment_of_post_pagination($post_id,$page_access_token);
       
       $commenter_count=isset($comment_list["commenter_info"]) ? count($comment_list["commenter_info"]) : 0 ;
       if(isset($comment_list["commenter_info"]))
       {
          if(isset($comment_list["commenter_info"][$fb_page_id])) 
          {
            $commenter_count--; // if page is also commenter
            unset($comment_list["commenter_info"][$fb_page_id]);
          }
       }

       $comment_count=isset($comment_list["comment_info"]) ? count($comment_list["comment_info"]) : 0 ;
       
       $insert_data=array
       (
        "facebook_rx_fb_user_info_id" => $this->session->userdata("facebook_rx_fb_user_info"),
        "user_id" => $this->user_id,
        "page_info_table_id" => $page_id,
        "page_id" => $fb_page_id,
        "page_name" => $page_name,
        "page_profile" => $page_profile,
        "post_id" => $post_id,
        "post_description" => $post_description,
        "post_created_at" => substr($post_created_at, 0, 19),
        "last_updated_at" => date("Y-m-d H:i:s"),
        "commenter_count" => $commenter_count
       );  

       $this->db->trans_start();    

       $this->basic->insert_data("tag_machine_enabled_post_list",$insert_data);
       $tag_machine_enabled_post_list_id=$this->db->insert_id();

       $insert_batch=array();
       if(isset($comment_list["commenter_info"]))
       {
          foreach ($comment_list["commenter_info"] as $key => $value) 
          {
            $last_comment_time=$value["last_comment_time"];
            $last_comment_time=str_replace('T',' ',$last_comment_time);
            $last_comment_time=substr($last_comment_time, 0, 19);

            $insert_batch[]=array
            (
                "tag_machine_enabled_post_list_id"=>$tag_machine_enabled_post_list_id,
                "facebook_rx_fb_user_info_id" => $this->session->userdata("facebook_rx_fb_user_info"),
                "user_id" => $this->user_id,
                "page_info_table_id" => $page_id,
                "page_id" => $fb_page_id,
                "page_name" => $page_name,
                "post_id"=>$post_id,
                "last_comment_id"=>$value["last_comment_id"],
                "last_comment_time"=>$last_comment_time,
                "commenter_fb_id"=>$key,
                "commenter_name"=>$value["name"]
            );
          }
       }

       $insert_batch2=array();
       if(isset($comment_list["comment_info"]))
       {
          foreach ($comment_list["comment_info"] as $key => $value) 
          {            
            if($value["commenter_id"]==$fb_page_id) // skipping self comments
            {
                $comment_count--;
                continue;
            }

            $comment_time=$value["created_time"];
            $comment_time=substr($comment_time, 0, 19);

            $insert_batch2[]=array
            (
                "tag_machine_enabled_post_list_id"=>$tag_machine_enabled_post_list_id,
                "facebook_rx_fb_user_info_id" => $this->session->userdata("facebook_rx_fb_user_info"),
                "user_id" => $this->user_id,
                "page_info_table_id" => $page_id,
                "page_id" => $fb_page_id,
                "page_name" => $page_name,
                "post_id"=> $post_id,
                "comment_id" => $value["comment_id"],
                "comment_text" => $value["message"],
                "commenter_fb_id" => $value["commenter_id"],
                "commenter_name"=>$value["commenter_name"],
                "comment_time" => $comment_time
            );
          }
       }

       $this->basic->update_data("tag_machine_enabled_post_list",array("id"=>$tag_machine_enabled_post_list_id),array("comment_count"=>$comment_count));

       if($commenter_count>0)
       $this->db->insert_batch("tag_machine_commenter_info",$insert_batch);

       if($comment_count>0)
       $this->db->insert_batch("tag_machine_comment_info",$insert_batch2);

       $this->db->trans_complete();
       if($this->db->trans_status() === false) 
       {
         echo json_encode(array('status'=>'0','message'=>$this->lang->line("something went wrong, please try again.")));
         exit();
       }

       $button_replace = "<label class='label label-success'><i class='fa fa-check'></i> ".$this->lang->line("Enabled successfully")." [".$commenter_count." ".$this->lang->line('Commenters')." | ".$comment_count." ".$this->lang->line("Comments")."]</label>";
       echo json_encode(array('status'=>'1','message'=>"<i class='fa fa-check'></i> ".$this->lang->line("post has been successfully enabled for tagging and commenter information has been fetched."),"button_replace"=>$button_replace));
      
    }

    public function manual_sync_commenter_info()
    {
        if($this->session->userdata('user_type') != 'Admin' && !in_array(202,$this->module_access) && !in_array(201,$this->module_access))
        exit();
        if(!$_POST) exit();

        $page_id=$this->input->post('page_id');
        $post_id=$this->input->post('post_id');

        $page_table_info = $this->basic->get_data('facebook_rx_fb_page_info',array('where'=>array('id'=>$page_id)));
        $fb_page_id = isset($page_table_info[0]['page_id']) ? $page_table_info[0]['page_id'] : "";
        $page_access_token = isset($page_table_info[0]['page_access_token']) ? $page_table_info[0]['page_access_token'] : "";
        $page_name = isset($page_table_info[0]['page_name']) ? $page_table_info[0]['page_name'] : "";
        $page_profile = isset($page_table_info[0]['page_profile']) ? $page_table_info[0]['page_profile'] : "";

        if($fb_page_id=='' || $page_access_token=='')
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('page not found.')));
            exit();
        }
        
        if(strpos($post_id,'_')!==FALSE)
        $post_id_use=$post_id;
        else $post_id_use = $fb_page_id."_".$post_id;

        if($this->basic->is_exist("tag_machine_enabled_post_list",array("facebook_rx_fb_user_info_id"=>$this->session->userdata("facebook_rx_fb_user_info"),"page_info_table_id"=>$page_id,"post_id"=>$post_id_use),'id'))
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line("this post is already enabled for tagging.")));
            exit();
        }

        try
        {
            $this->load->library('fb_rx_login');
            $post_info = $this->fb_rx_login->get_post_info_by_id($post_id_use,$page_access_token);

            if(isset($post_info['error']))
            {
                $response['error_msg'] = $post_info['error']['message'];
                echo json_encode(array('status'=>'0','message'=>$post_info['error']['message']));
                exit();
            }

            if(empty($post_info))
            {
                echo json_encode(array('status'=>'0','message'=>$this->lang->line("please provide correct post id.")));
                exit();
            }

            foreach ($post_info as $key => $value) 
            {
                $post_description=isset($value["message"])?$value["message"]:"";
                $post_created_at=isset($value["created_time"])?$value["created_time"]:"";
                $post_created_at=str_replace('T',' ',$post_created_at);
                $post_created_at=substr($post_created_at, 0, 19);
            }

            $this->load->library("commenttagmachine_class");
            $comment_list = $this->commenttagmachine_class->get_all_comment_of_post_pagination($post_id_use,$page_access_token);
           
            $commenter_count=isset($comment_list["commenter_info"]) ? count($comment_list["commenter_info"]) : 0 ;
            if(isset($comment_list["commenter_info"]))
            {
              if(isset($comment_list["commenter_info"][$fb_page_id])) 
              {
                $commenter_count--; // if page is also commenter
                unset($comment_list["commenter_info"][$fb_page_id]);
              }
            }

            $comment_count=isset($comment_list["comment_info"]) ? count($comment_list["comment_info"]) : 0 ;

            $insert_data=array
            (
                "facebook_rx_fb_user_info_id" => $this->session->userdata("facebook_rx_fb_user_info"),
                "user_id" => $this->user_id,
                "page_info_table_id" => $page_id,
                "page_id" => $fb_page_id,
                "page_name" => $page_name,
                "page_profile" => $page_profile,
                "post_id" => $post_id_use,
                "post_description" => $post_description,
                "post_created_at" => $post_created_at,
                "last_updated_at" => date("Y-m-d H:i:s"),
                "commenter_count" => $commenter_count
            ); 


           $this->db->trans_start();       

           $this->basic->insert_data("tag_machine_enabled_post_list",$insert_data);
           $tag_machine_enabled_post_list_id=$this->db->insert_id();

           $insert_batch=array();
           if(isset($comment_list["commenter_info"]))
           {
              foreach ($comment_list["commenter_info"] as $key => $value) 
              {                
                $last_comment_time=$value["last_comment_time"];
                $last_comment_time=str_replace('T',' ',$last_comment_time);
                $last_comment_time=substr($last_comment_time, 0, 19);

                $insert_batch[]=array
                (
                    "tag_machine_enabled_post_list_id"=>$tag_machine_enabled_post_list_id,
                    "facebook_rx_fb_user_info_id" => $this->session->userdata("facebook_rx_fb_user_info"),
                    "user_id" => $this->user_id,
                    "page_info_table_id" => $page_id,
                    "page_id" => $fb_page_id,
                    "page_name" => $page_name,
                    "post_id"=>$post_id_use,
                    "last_comment_id"=>$value["last_comment_id"],
                    "last_comment_time"=>$last_comment_time,
                    "commenter_fb_id"=>$key,                    
                    "commenter_name"=>$value["name"]
                );
              }
           }

           $insert_batch2=array();
           if(isset($comment_list["comment_info"]))
           {
              foreach ($comment_list["comment_info"] as $key => $value) 
              {                
                if($value["commenter_id"]==$fb_page_id) // skipping self comments
                {
                    $comment_count--;
                    continue;
                }

                $comment_time=$value["created_time"];
                $comment_time=substr($comment_time, 0, 19);

                $insert_batch2[]=array
                (
                    "tag_machine_enabled_post_list_id"=>$tag_machine_enabled_post_list_id,
                    "facebook_rx_fb_user_info_id" => $this->session->userdata("facebook_rx_fb_user_info"),
                    "user_id" => $this->user_id,
                    "page_info_table_id" => $page_id,
                    "page_id" => $fb_page_id,
                    "page_name" => $page_name,
                    "post_id"=> $post_id,
                    "comment_id" => $value["comment_id"],
                    "comment_text" => $value["message"],
                    "commenter_fb_id" => $value["commenter_id"],
                    "commenter_name"=>$value["commenter_name"],
                    "comment_time" => $comment_time
                );
              }
           }

           $this->basic->update_data("tag_machine_enabled_post_list",array("id"=>$tag_machine_enabled_post_list_id),array("comment_count"=>$comment_count));

           if($commenter_count>0)
           $this->db->insert_batch("tag_machine_commenter_info",$insert_batch);

           if($comment_count>0)
           $this->db->insert_batch("tag_machine_comment_info",$insert_batch2);

           $this->db->trans_complete();
           if($this->db->trans_status() === false) 
           {
             echo json_encode(array('status'=>'0','message'=>$this->lang->line("something went wrong, please try again.")));
             exit();
           }

           echo json_encode(array('status'=>'1','message'=>$this->lang->line("post has been successfully enabled for tagging and commenter information has been fetched.")));

        }
        catch(Exception $e)
        {
            echo json_encode(array('status'=>'0','message'=>$e->getMessage()));
        }

    }

    public function rescan_commenter_info()
    {
        if($this->session->userdata('user_type') != 'Admin' && !in_array(202,$this->module_access) && !in_array(201,$this->module_access))
        exit();
        if(!$_POST) exit();

        $page_id=$this->input->post('page_id');
        $post_id=$this->input->post('post_id');
        $tag_machine_enabled_post_list_id=$this->input->post('enable_id');

        $page_table_info = $this->basic->get_data('facebook_rx_fb_page_info',array('where'=>array('id'=>$page_id)));
        $fb_page_id = isset($page_table_info[0]['page_id']) ? $page_table_info[0]['page_id'] : "";
        $page_access_token = isset($page_table_info[0]['page_access_token']) ? $page_table_info[0]['page_access_token'] : "";
        $page_name = isset($page_table_info[0]['page_name']) ? $page_table_info[0]['page_name'] : "";
        $page_profile = isset($page_table_info[0]['page_profile']) ? $page_table_info[0]['page_profile'] : "";

        if($fb_page_id=='' || $page_access_token=='')
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('page not found.')));
            exit();
        }
        
        $post_id_use=$post_id;      

        try
        {
            $this->load->library('fb_rx_login');
            $post_info = $this->fb_rx_login->get_post_info_by_id($post_id_use,$page_access_token);

            if(isset($post_info['error']))
            {
                $response['error_msg'] = $post_info['error']['message'];
                echo json_encode(array('status'=>'0','message'=>$post_info['error']['message']));
                exit();
            }

            if(empty($post_info))
            {
                echo json_encode(array('status'=>'0','message'=>$this->lang->line("something went wrong.")));
                exit();
            }

            foreach ($post_info as $key => $value) 
            {
                $post_description=isset($value["message"])?$value["message"]:"";
                $post_created_at=isset($value["created_time"])?$value["created_time"]:"";
                $post_created_at=str_replace('T',' ',$post_created_at);
                $post_created_at=substr($post_created_at, 0, 19);
            }

            $this->load->library("commenttagmachine_class");
            $comment_list = $this->commenttagmachine_class->get_all_comment_of_post_pagination($post_id_use,$page_access_token);
           
            $commenter_count=isset($comment_list["commenter_info"]) ? count($comment_list["commenter_info"]) : 0 ;
            if(isset($comment_list["commenter_info"]))
            {
              if(isset($comment_list["commenter_info"][$fb_page_id])) 
              {
                $commenter_count--; // if page is also commenter
                unset($comment_list["commenter_info"][$fb_page_id]);
              }
            }

           $comment_count=isset($comment_list["comment_info"]) ? count($comment_list["comment_info"]) : 0 ;
      
           $insert_batch=array();
           if(isset($comment_list["commenter_info"]))
           {
              foreach ($comment_list["commenter_info"] as $key => $value) 
              {                
                $last_comment_time=$value["last_comment_time"];
                $last_comment_time=str_replace('T',' ',$last_comment_time);
                $last_comment_time=substr($last_comment_time, 0, 19);

                $insert_batch[]=array
                (
                    "tag_machine_enabled_post_list_id"=>$tag_machine_enabled_post_list_id,
                    "facebook_rx_fb_user_info_id" => $this->session->userdata("facebook_rx_fb_user_info"),
                    "user_id" => $this->user_id,
                    "page_info_table_id" => $page_id,
                    "page_id" => $fb_page_id,
                    "page_name" => $page_name,
                    "post_id"=>$post_id_use,
                    "last_comment_id"=>$value["last_comment_id"],
                    "last_comment_time"=>$last_comment_time,
                    "commenter_fb_id"=>$key,                    
                    "commenter_name"=>$value["name"]
                );
              }
           }

           $insert_batch2=array();
           if(isset($comment_list["comment_info"]))
           {
              foreach ($comment_list["comment_info"] as $key => $value) 
              {                
                if($value["commenter_id"]==$fb_page_id) // skipping self comments
                {
                    $comment_count--;
                    continue;
                }

                $comment_time=$value["created_time"];
                $comment_time=substr($comment_time, 0, 19);

                $insert_batch2[]=array
                (
                    "tag_machine_enabled_post_list_id"=>$tag_machine_enabled_post_list_id,
                    "facebook_rx_fb_user_info_id" => $this->session->userdata("facebook_rx_fb_user_info"),
                    "user_id" => $this->user_id,
                    "page_info_table_id" => $page_id,
                    "page_id" => $fb_page_id,
                    "page_name" => $page_name,
                    "post_id"=> $post_id,
                    "comment_id" => $value["comment_id"],
                    "comment_text" => $value["message"],
                    "commenter_fb_id" => $value["commenter_id"],
                    "commenter_name"=>$value["commenter_name"],
                    "comment_time" => $comment_time
                );
              }
           }

           $unsubscribe_commenter=array();
           $unsubscribe_data=$this->basic->get_data("tag_machine_commenter_info",array("where"=>array("tag_machine_enabled_post_list_id"=>$tag_machine_enabled_post_list_id,"subscribed"=>"0")));
           foreach ($unsubscribe_data as $key => $value) 
           {
               $unsubscribe_commenter[]=$value['commenter_fb_id'];
           }

           $this->db->trans_start();

           $this->basic->update_data("tag_machine_enabled_post_list",array("id"=>$tag_machine_enabled_post_list_id),array("comment_count"=>$comment_count,"commenter_count"=>$commenter_count,"last_updated_at"=>date("Y-m-d H:i:s")));
           $this->basic->delete_data("tag_machine_commenter_info",array("tag_machine_enabled_post_list_id"=>$tag_machine_enabled_post_list_id));           
           $this->basic->delete_data("tag_machine_comment_info",array("tag_machine_enabled_post_list_id"=>$tag_machine_enabled_post_list_id)); 
          
           if($commenter_count>0)
           $this->db->insert_batch("tag_machine_commenter_info",$insert_batch);

           if($comment_count>0)
           $this->db->insert_batch("tag_machine_comment_info",$insert_batch2);

           if(count($unsubscribe_commenter)>0)
           {
             $this->db->where("tag_machine_enabled_post_list_id",$tag_machine_enabled_post_list_id);
             $this->db->where_in("commenter_fb_id",$unsubscribe_commenter);
             $this->db->update("tag_machine_commenter_info",array("subscribed"=>"0"));

             $this->db->where("tag_machine_enabled_post_list_id",$tag_machine_enabled_post_list_id);
             $this->db->where_in("commenter_fb_id",$unsubscribe_commenter);
             $this->db->update("tag_machine_comment_info",array("subscribed"=>"0"));
           }

           $this->db->trans_complete();
           if($this->db->trans_status() === false) 
           {
             echo json_encode(array('status'=>'0','message'=>$this->lang->line("something went wrong, please try again.")));
             exit();
           }

           echo json_encode(array('status'=>'1','message'=>$this->lang->line("post comments has been updated successfully.")));

        }
        catch(Exception $e)
        {
            echo json_encode(array('status'=>'0','message'=>$e->getMessage()));
        }

    }

    public function post_list($page_info_table_id=0)
    {
        if($this->session->userdata('user_type') != 'Admin' && !in_array(202,$this->module_access) && !in_array(201,$this->module_access))
        redirect('home/login_page', 'location'); 

        $data['body'] = "post_list";
        $data['page_title'] = $this->lang->line("Post List");
        $page_info=$this->basic->get_data("tag_machine_enabled_post_list",array("where"=>array("tag_machine_enabled_post_list.facebook_rx_fb_user_info_id"=>$this->session->userdata("facebook_rx_fb_user_info"))),array("facebook_rx_fb_page_info.*"),array('facebook_rx_fb_page_info'=>"tag_machine_enabled_post_list.page_info_table_id=facebook_rx_fb_page_info.id,left"),'','','facebook_rx_fb_page_info.page_name ASC','tag_machine_enabled_post_list.page_info_table_id');
        $data['page_info'] = $page_info;
        $data['auto_search_page_info_table_id'] = $page_info_table_id;
        $this->session->set_userdata("comment_tag_machine_post_list_auto_search_page_info_table_id",$page_info_table_id);
        $data["time_zone"]= $this->_time_zone_list();
        $this->_viewcontroller($data);
    }

    public function post_list_data()
    {
        if($this->session->userdata('user_type') != 'Admin' && !in_array(202,$this->module_access) && !in_array(201,$this->module_access))
        redirect('home/login_page', 'location'); 

        if ($_SERVER['REQUEST_METHOD'] === 'GET') exit();

        $page = isset($_POST['page']) ? intval($_POST['page']) : 15;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 5;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';

        $search_page_id = trim($this->input->post("search_page_id", true));
        $search_post_id = trim($this->input->post("search_post_id", true));

        $post_created_from = trim($this->input->post('post_created_from', true));
        if($post_created_from) $post_created_from = date('Y-m-d H:i:s', strtotime($post_created_from));

        $post_created_to = trim($this->input->post('post_created_to', true));
        if($post_created_to) $post_created_to = date('Y-m-d H:i:s', strtotime($post_created_to));

        $is_searched = $this->input->post('is_searched', true);


        if($is_searched)
        {
            $this->session->set_userdata('comment_tag_machine_post_list_page_id', $search_page_id);
            $this->session->set_userdata('comment_tag_machine_post_list_post_id', $search_post_id);
            $this->session->set_userdata('comment_tag_machine_post_list_post_created_from', $post_created_from);
            $this->session->set_userdata('comment_tag_machine_post_list_post_created_to', $post_created_to);
        }
        else // auto search
        {
          if($this->session->userdata("comment_tag_machine_post_list_auto_search_page_info_table_id")!=0)
          {
            $this->session->set_userdata('comment_tag_machine_post_list_page_id',$this->session->userdata("comment_tag_machine_post_list_auto_search_page_info_table_id"));
            $this->session->unset_userdata('comment_tag_machine_post_list_post_id');
            $this->session->unset_userdata('comment_tag_machine_post_list_post_created_from');
            $this->session->unset_userdata('comment_tag_machine_post_list_post_created_to');
          }
        }

        $search_page_id  = $this->session->userdata('comment_tag_machine_post_list_page_id');
        $search_post_id  = $this->session->userdata('comment_tag_machine_post_list_post_id');
        $post_created_from  = $this->session->userdata('comment_tag_machine_post_list_post_created_from');
        $post_created_to = $this->session->userdata('comment_tag_machine_post_list_post_created_to');

        $where_simple=array();

        if ($search_page_id) $where_simple['page_info_table_id']    = $search_page_id;
        if ($search_post_id) $where_simple['post_id like ']    = "%".$search_post_id."%";

        if ($post_created_from)
        {
            if ($post_created_from != '1970-01-01')
            $where_simple["Date_Format(post_created_at,'%Y-%m-%d') >="]= $post_created_from;

        }
        if ($post_created_to)
        {
            if ($post_created_to != '1970-01-01')
            $where_simple["Date_Format(post_created_at,'%Y-%m-%d') <="]=$post_created_to;

        }

        $where_simple['facebook_rx_fb_user_info_id'] = $this->session->userdata("facebook_rx_fb_user_info");
        $order_by_str=$sort." ".$order;
        $offset = ($page-1)*$rows;
        $where = array('where' => $where_simple);

        $table = "tag_machine_enabled_post_list";
        $info = $this->basic->get_data($table,$where,$select='',$join='',$limit=$rows, $start=$offset,$order_by=$order_by_str);
        // echo $this->db->last_query();

        for($i=0;$i<count($info);$i++)
        {
            $info[$i]['last_updated_at'] = date("M j, y H:i",strtotime($info[$i]['last_updated_at']));
            $info[$i]['post_created_at'] = date("M j, y H:i",strtotime($info[$i]['post_created_at']));

            $info[$i]['rescan'] = "<a enable-id='".$info[$i]["id"]."' page-id='".$info[$i]["page_info_table_id"]."' post-id='".$info[$i]["post_id"]."' id='rescan_".$info[$i]["page_info_table_id"]."_".$info[$i]["post_id"]."' title='".$this->lang->line("Re-scan Comments")."' class='rescan_comments btn btn-outline-primary btn-sm'><i class='fa fa-refresh'></i> ".$this->lang->line("Re-scan")."</a>&nbsp;";

            $disable_create1=$disable_create2="";
            if($info[$i]['commenter_count']<=1) $disable_create1="disabled";
            if($info[$i]['comment_count']<=1) $disable_create2="disabled";

            $info[$i]['comment_bulk_tag'] = "<a id='bulktag-".$info[$i]["id"]."-".$info[$i]["commenter_count"]."' title='".$this->lang->line("Create Campaign")."' class='".$disable_create1." create_bulk_tag_campaign btn-sm btn btn-outline-dark'><i class='fa fa-plus-circle'></i> ".$this->lang->line("Create")."</a>&nbsp;";
            $info[$i]['comment_bulk_tag'] .=  "<a href='".base_url("commenttagmachine/bulk_tag_campaign_list/".$info[$i]["id"])."' title='".$this->lang->line("Report")."' class='comment_bulk_tag_report btn-sm btn btn-outline-info'><i class='fa fa-list'></i> ".$this->lang->line("Report")."</a>&nbsp;";

            $info[$i]['bulk_comment_reply'] = "<a id='bulkreply-".$info[$i]["id"]."-".$info[$i]["comment_count"]."' title='".$this->lang->line("Create Campaign")."' class='".$disable_create2." bulk_comment_reply_campaign btn-sm btn btn-outline-dark'><i class='fa fa-plus-circle'></i> ".$this->lang->line("Create")."</a>&nbsp;";
            $info[$i]['bulk_comment_reply'] .=  "<a href='".base_url("commenttagmachine/bulk_comment_reply_campaign_list/".$info[$i]["id"])."' title='".$this->lang->line("Report")."' class='bulk_comment_reply_report btn-sm btn btn-outline-info'><i class='fa fa-list'></i> ".$this->lang->line("Report")."</a>&nbsp;";
        }
        $total_rows_array = $this->basic->count_row($table, $where, $count = "id");
        $total_result = $total_rows_array[0]['total_rows'];

        echo convert_to_grid_data($info, $total_result);
    }

    public function post_comment_list()
    {
        if(!$_POST) exit();
        if($this->session->userdata('user_type') != 'Admin' && !in_array(202,$this->module_access) && !in_array(201,$this->module_access)) exit();

        $id = $this->input->post("id");
        $table="tag_machine_comment_info";        

        $comment_data = $this->basic->get_data($table,array("where"=>array("tag_machine_enabled_post_list_id"=>$id,"facebook_rx_fb_user_info_id"=>$this->session->userdata("facebook_rx_fb_user_info"))),'','','',NULL,$order_by='comment_time DESC');


        $response = "";
        if(count($comment_data)==0)
        {
            $response.= "<h4><div class='alert alert-warning text-center'>".$this->lang->line("no data found")."</div></h4>";
            echo $response;
            exit();
        }

        $postid=isset($comment_data[0]["post_id"])?$comment_data[0]["post_id"]:"";
        $pagename=isset($comment_data[0]["page_name"])?$comment_data[0]["page_name"]:"";
        $pageid=isset($comment_data[0]["page_id"])?$comment_data[0]["page_id"]:"";
        $response.= "<h3 class='text-center' style='font-size: 17px;margin-top: -5px;margin-bottom: 18px;'><a class='orange' target='_BLANK' href='https://facebook.com/".$pageid."'><i class='fa fa-newspaper-o'></i> ".$pagename."</a> <a target='_BLANK' href='https://facebook.com/".$postid."'> (".$this->lang->line('Visit Post').")</a></h3>";
       
        $response .= '<script>
                    $j(document).ready(function() {
                         $(".table-responsive").mCustomScrollbar({
                            autoHideScrollbar:true,
                            theme:"3d-dark",          
                            axis: "x"
                        }); 
                        $("#post_comment_table").DataTable();
                    });
                 </script>
                 <div class="text-center" style="margin-top: 0px !important;">
                         <a href="'.site_url().'commenttagmachine/download_comment_list_info/'.$id.'" target="_blank" ><button class="btn btn-outline-info download_comment_list_info"><i class="fa fa-cloud-download"></i> '.$this->lang->line("Download comment list info").'</button></a>
                 </div>
                 ';

        $response .="<div class='table-responsive'>";
        $response .="<table id='post_comment_table' class='table table-hover table-bordered table-striped table-condensed nowrap'>";
        $response .= "<thead><tr>";
        $response .= "<th class='text-center'></th>";
        $response .= "<th>{$this->lang->line("Commenter Name")}</th>";
        $response .= "<th class='text-center'>{$this->lang->line("Comment ID")}</th>";
        $response .= "<th class='text-center'>{$this->lang->line("Comment Time")}</th>";
        $response .= "<th>{$this->lang->line("Comment")}</th>";
        $response .= "</tr></thead>";
        $i=0;
        foreach ($comment_data as $key2 => $value)
        {
            $i++;
            $response .= "<tr>";
            $response .= "<td class='text-center'>".$i."</td>";
            $response .= "<td><a target='_BLANK' href='https://facebook.com/".$value["commenter_fb_id"]."'>".$value["commenter_name"]."</a></td>";
            $response .= "<td class='text-center'><a target='_BLANK' href='https://facebook.com/".$value["comment_id"]."'>".$value["comment_id"]."</a></td>";
            $response .= "<td class='text-center'>".date("M j, y H:i",strtotime($value["comment_time"]))."</td>";
            $response .= "<td>".$value["comment_text"]."</td>";
            $response .= "</tr>";          
        }
        $response .= "</table></div>";

        echo $response;
    }


    /**
     * downloads comment id, text, commenters id, name
     * uses php memory to download on the fly
     *             
     * @return null
     */
    public function download_comment_list_info($id = 1)
    {
      
        
        if($this->session->userdata('user_type') != 'Admin' && !in_array(202,$this->module_access) && !in_array(201,$this->module_access)) exit();

        $table="tag_machine_comment_info";        

        $comment_data = $this->basic->get_data($table,array("where"=>array("tag_machine_enabled_post_list_id"=>$id,"facebook_rx_fb_user_info_id"=>$this->session->userdata("facebook_rx_fb_user_info"))),'','','',NULL,$order_by='comment_time DESC');


        $postid=isset($comment_data[0]["post_id"])?$comment_data[0]["post_id"]:"";
        $pagename=isset($comment_data[0]["page_name"])?$comment_data[0]["page_name"]:"";
        $pageid=isset($comment_data[0]["page_id"])?$comment_data[0]["page_id"]:"";

        $filename="{$this->user_id}_comment_list_info.csv";
        // make output csv file unicode compatible
        $f = fopen('php://memory', 'w'); 
        fputs( $f, "\xEF\xBB\xBF" );

        /**Write header in csv file***/
        $write_data[]="Commenter Name";
        $write_data[]="Commenter Id";
        $write_data[]="Comment Id";
        $write_data[]="Comment Time";
        $write_data[]="Comment Text";

        fputcsv($f,$write_data, ",");

        foreach ($comment_data as $key2 => $value)
        {
            
            $write_data=array();
            $write_data[]=$value["commenter_name"];
            $write_data[]=$value["commenter_fb_id"];
            $write_data[]=$value["comment_id"];
            $write_data[]=$value["comment_time"];
            $write_data[]=$value["comment_text"];

            fputcsv($f,$write_data, ",");   
        }
        

        // reset the file pointer to the start of the file
        fseek($f, 0);
        // tell the browser it's going to be a csv file
        header('Content-Type: application/csv');
        // tell the browser we want to save it instead of displaying it
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        // make php send the generated csv lines to the browser
        fpassthru($f); 

        
    }

    public function post_commenter_list()
    {
        if(!$_POST) exit();
        if($this->session->userdata('user_type') != 'Admin' && !in_array(202,$this->module_access) && !in_array(201,$this->module_access)) exit();

        $id = $this->input->post("id");
        $table="tag_machine_commenter_info";        

        $commenter_data = $this->basic->get_data($table,array("where"=>array("tag_machine_enabled_post_list_id"=>$id,"facebook_rx_fb_user_info_id"=>$this->session->userdata("facebook_rx_fb_user_info"))),'','','',NULL,$order_by='last_comment_time DESC');


        $response = "";
        if(count($commenter_data)==0)
        {
            $response.= "<h4><div class='alert alert-warning text-center'>".$this->lang->line("no data found")."</div></h4>";
            echo $response;
            exit();
        }

        $response .= '<script>
                    $j(document).ready(function() {
                        $(".table-responsive").mCustomScrollbar({
                            autoHideScrollbar:true,
                            theme:"3d-dark",          
                            axis: "x"
                        });
                        $("#post_commenter_table").DataTable();
                    });
                 </script>';

        $postid=isset($commenter_data[0]["post_id"])?$commenter_data[0]["post_id"]:"";
        $pagename=isset($commenter_data[0]["page_name"])?$commenter_data[0]["page_name"]:"";
        $pageid=isset($commenter_data[0]["page_id"])?$commenter_data[0]["page_id"]:"";
        $response.= "<h3 class='text-center' style='font-size: 17px;margin-top: -5px;margin-bottom: 18px;'><a class='orange' target='_BLANK' href='https://facebook.com/".$pageid."'><i class='fa fa-newspaper-o'></i> ".$pagename."</a> <a target='_BLANK' href='https://facebook.com/".$postid."'> (".$this->lang->line('Visit Post').")</a></h3>
          <div class='text-center' style='margin-top: 0px !important;'>
                  <a href='".site_url()."commenttagmachine/download_commenter_list_info/".$id."' target='_blank' ><button class='btn btn-outline-info download_comment_list_info'><i class='fa fa-cloud-download'></i> ".$this->lang->line("Download commenter list info")."</button></a>
          </div>
        ";
 
        $response .="<div class='table-responsive'>";
        $response .="<table id='post_commenter_table' class='table table-hover table-bordered table-striped table-condensed nowrap'>";
        $response .= "<thead><tr>";
        $response .= "<th class='text-center'></th>";
        $response .= "<th>{$this->lang->line("Commenter Name")}</th>";
        $response .= "<th class='text-center'>{$this->lang->line("Last Comment ID")}</th>";
        $response .= "<th class='text-center'>{$this->lang->line("Status")}</th>";
        $response .= "<th class='text-center'>{$this->lang->line("Last Comment Time")}</th>";
        $response .= "</tr></thead>";
        $i=0;
        foreach ($commenter_data as $key2 => $value)
        {
            $i++;
            $response .= "<tr>";
            $response .= "<td class='text-center'>".$i."</td>";
            $response .= "<td><a target='_BLANK' href='https://facebook.com/".$value["commenter_fb_id"]."'>".$value["commenter_name"]."</a></td>";
            $response .= "<td class='text-center'><a target='_BLANK' href='https://facebook.com/".$value["last_comment_id"]."'>".$value["last_comment_id"]."</a></td>";

            if($value['subscribed'] == '1')
            {
                $subscribe_unsubscribe_button = "<button id ='".$value['tag_machine_enabled_post_list_id']."-".$value['commenter_fb_id']."-".$value['subscribed']."' type='button' class='commenter_subscribe_unsubscribe btn-sm btn btn-danger'>".$this->lang->line("unsubscribe")."</button>";
            }
            else
            {
                $subscribe_unsubscribe_button = "<button id ='".$value['tag_machine_enabled_post_list_id']."-".$value['commenter_fb_id']."-".$value['subscribed']."' type='button' class='commenter_subscribe_unsubscribe btn-sm btn btn-success'>".$this->lang->line("subscribe")."</button>";
            }

            $response .= "<td class='text-center'>".$subscribe_unsubscribe_button."</td>";
            $response .= "<td class='text-center'>".date("M j, y H:i",strtotime($value["last_comment_time"]))."</td>";
            $response .= "</tr>";          
        }
        $response .= "</table></div>";

        echo $response;
    }


    /**
     * downloads commenters id, name, Last Comment ID, Status, Last Comment Time
     * uses php memory to download on the fly
     * @return null
     */
    public function download_commenter_list_info($id = 1)
    {
      
        if($this->session->userdata('user_type') != 'Admin' && !in_array(202,$this->module_access) && !in_array(201,$this->module_access)) exit();

        $table="tag_machine_commenter_info";        

        $commenter_data = $this->basic->get_data($table,array("where"=>array("tag_machine_enabled_post_list_id"=>$id,"facebook_rx_fb_user_info_id"=>$this->session->userdata("facebook_rx_fb_user_info"))),'','','',NULL,$order_by='last_comment_time DESC');

        $postid=isset($commenter_data[0]["post_id"])?$commenter_data[0]["post_id"]:"";
        $pagename=isset($commenter_data[0]["page_name"])?$commenter_data[0]["page_name"]:"";
        $pageid=isset($commenter_data[0]["page_id"])?$commenter_data[0]["page_id"]:"";
        
        $filename="{$this->user_id}_commenter_list_info.csv";
        // make output csv file unicode compatible
        $f = fopen('php://memory', 'w'); 
        fputs( $f, "\xEF\xBB\xBF" );

        /**Write header in csv file***/
        $write_data[]="Commenter Name";
        $write_data[]="Commenter Id";
        $write_data[]="Last Comment ID";
        $write_data[]="Status";
        $write_data[]="Last Comment Time";

        fputcsv($f,$write_data, ",");
        
        foreach ($commenter_data as $key2 => $value)
        {

            $write_data=array();
            $write_data[] = $value["commenter_name"];
            $write_data[] = $value["commenter_fb_id"];
            $write_data[] = $value["last_comment_id"];
            $write_data[] = $value['subscribed'];
            $write_data[] = $value["last_comment_time"];

            fputcsv($f,$write_data, ",");  
        }

        // reset the file pointer to the start of the file
        fseek($f, 0);
        // tell the browser it's going to be a csv file
        header('Content-Type: application/csv');
        // tell the browser we want to save it instead of displaying it
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        // make php send the generated csv lines to the browser
        fpassthru($f); 
            
    }

    public function subscribe_unsubscribe_status_change()
    {     
        if (empty($_POST['subscribe_unsubscribe_status'])) exit();
        if($this->session->userdata('user_type') != 'Admin' && !in_array(202,$this->module_access) && !in_array(201,$this->module_access)) exit();

        $client_subscribe_unsubscribe = array();
        $post_val=$this->input->post('subscribe_unsubscribe_status');
        $client_subscribe_unsubscribe = explode("-",$post_val);
        
        $tag_machine_enabled_post_list_id = isset($client_subscribe_unsubscribe[0]) ? $client_subscribe_unsubscribe[0]: 0;
        $commenter_fb_id = isset($client_subscribe_unsubscribe[1]) ? $client_subscribe_unsubscribe[1]: "";
        $current_status =  isset($client_subscribe_unsubscribe[2]) ? $client_subscribe_unsubscribe[2]: "0";
        
        if($current_status=="1") $permission="0";
        else $permission="1";

        $where = array
        (
            'tag_machine_enabled_post_list_id' => $tag_machine_enabled_post_list_id,
            'commenter_fb_id'=>$commenter_fb_id,
            'user_id' => $this->user_id
        );
        $data = array('subscribed' => $permission);

        if($this->basic->update_data('tag_machine_commenter_info', $where, $data))
        {     
            
            $this->basic->update_data('tag_machine_comment_info', $where, $data);

            if($permission=="0")  $response = "<button id ='".$tag_machine_enabled_post_list_id."-".$commenter_fb_id."-".$permission."' type='button' class='commenter_subscribe_unsubscribe btn-sm btn btn-success'>".$this->lang->line("subscribe")."</button>";
            else  $response = "<button id ='".$tag_machine_enabled_post_list_id."-".$commenter_fb_id."-".$permission."' type='button' class='commenter_subscribe_unsubscribe btn-sm btn btn-danger'>".$this->lang->line("unsubscribe")."</button>";
          
            echo $response;
        }
    }

    public function commenter_autocomplete($tag_campaign_tag_machine_enabled_post_list_id="")
    {
       
       $search_query= $this->input->get('search');

       $this->db->select();
       $this->db->from('tag_machine_commenter_info');
       $this->db->like('commenter_name', $search_query);
       $this->db->order_by('commenter_name', 'ASC');
       $this->db->where("subscribed","1");
       $this->db->where("tag_machine_enabled_post_list_id",$tag_campaign_tag_machine_enabled_post_list_id);
       $this->db->limit(20);
       $data=$this->db->get()->result_array();
       $results=array();

       foreach ($data as $key => $value)
       {
          $results[]=array("value"=>$value["commenter_fb_id"],"text"=>$value["commenter_name"]);
       }
       echo json_encode($results);
    }

    public function commenter_range_option()
    {       
        if(!$_POST) exit();
        $resposne='';

        $tag_machine_enabled_post_list_id=$this->input->post("tag_machine_enabled_post_list_id");
        $commenter_list = $this->basic->count_row("tag_machine_commenter_info",array("where"=>array("tag_machine_enabled_post_list_id"=>$tag_machine_enabled_post_list_id,"subscribed"=>"1")));
        $commenter_count=$commenter_list[0]['total_rows'];
       
        $item_per_range=$this->config->item('item_per_range');
        if($item_per_range=='') $item_per_range=50;

        $cal=ceil($commenter_count/$item_per_range);

        for($i=1;$i<=$cal;$i++)
        {
            if($i==1)
            {
                $start=$i;
                $end=$item_per_range;
            }
            else
            {
                $start=$end+1;
                $end=$end+$item_per_range;
            }
            if($end>=$commenter_count) $end=$commenter_count;
            if($start==$end) continue;

            $resposne.= '<option style="padding:5px;" value="'.$start.'-'.$end.'">&nbsp;&nbsp;&nbsp; o &nbsp;'.$this->lang->line("Latest")." ".$start.'-'.$end.'</option>';
        }
        echo $resposne;
    }

    public function create_bulk_tag_campaign_action()
    {
        if(!$_POST) exit();
        if($this->session->userdata('user_type') != 'Admin' && !in_array(201,$this->module_access))  exit();
        $status=$this->_check_usage($module_id=201,$request=1);
        if($status=="3")  
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('sorry, your monthly limit is exceeded for this module.')));
            exit();
        }

        $schedule_type = $this->input->post('schedule_type');
        $schedule_time = $this->input->post('schedule_time');
        $time_zone = $this->input->post('time_zone');
        if($schedule_type=='now') $schedule_time='';

        $tag_machine_enabled_post_list_id = $this->input->post('tag_campaign_tag_machine_enabled_post_list_id');
        $facebook_rx_fb_user_info_id = $this->session->userdata("facebook_rx_fb_user_info");
        $user_id = $this->user_id;
        $campaign_name = $this->input->post('campaign_name');
        $tag_content = $this->input->post('message');
        $uploaded_image_video = $this->input->post('uploaded_image_video');
        $commenter_range = $this->input->post('commenter_range');

        $item_per_range=$this->config->item('item_per_range');
        if($item_per_range=='') $item_per_range=50;

        $explode_range=explode('-', $commenter_range);
        $start=isset($explode_range[0])?$explode_range[0]:1;
        $end=isset($explode_range[1])?$explode_range[1]:$item_per_range;
        $comenter_limit=($end-$start)+1;
        $commenter_start=$start-1;
        
        $tag_exclude = $this->input->post('exclude');
        if(!is_array($tag_exclude)) $tag_exclude = array();

        $page_info=$this->basic->get_data("tag_machine_enabled_post_list",array("where"=>array("id"=>$tag_machine_enabled_post_list_id,"facebook_rx_fb_user_info_id"=>$facebook_rx_fb_user_info_id)));

        if(count($page_info)==0)
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('something went wrong, please try again.')));
            exit();
        }

        $page_info_table_id=isset($page_info[0]["page_info_table_id"])?$page_info[0]["page_info_table_id"]:"";
        $page_id=isset($page_info[0]["page_id"])?$page_info[0]["page_id"]:""; // fb page id
        $page_name=isset($page_info[0]["page_name"])?$page_info[0]["page_name"]:"";
        $page_profile=isset($page_info[0]["page_profile"])?$page_info[0]["page_profile"]:"";
        $post_id=isset($page_info[0]["post_id"])?$page_info[0]["post_id"]:"";
        $post_created_at=isset($page_info[0]["post_created_at"])?$page_info[0]["post_created_at"]:"";
        $post_description=isset($page_info[0]["post_description"])?$page_info[0]["post_description"]:"";

        $commenter_list = $this->basic->get_data("tag_machine_commenter_info",array("where"=>array("tag_machine_enabled_post_list_id"=>$tag_machine_enabled_post_list_id,"subscribed"=>"1")),"","",$comenter_limit,$commenter_start,"last_comment_time DESC");
        $tag_database_array=array();
        $commenter_count=0;
        foreach ($commenter_list as $key => $value) 
        {
            if(in_array($value['commenter_fb_id'], $tag_exclude)) continue;

            $tag_database_array[$value["commenter_fb_id"]]=
            array
            (
                "commenter_name"=>$value["commenter_name"],
                "commenter_fb_id"=>$value["commenter_fb_id"],
                "last_comment_id"=>$value["last_comment_id"],
                "last_comment_time"=>$value["last_comment_time"]
            );
            $commenter_count++;
        }
        $tag_database=json_encode($tag_database_array);
        $current_time=date("Y-m-d H:i:s");
        $data = array
        (
           "campaign_name"=>$campaign_name, 
           "tag_database"=>$tag_database, 
           "tag_exclude"=>json_encode($tag_exclude), 
           "tag_content"=>$tag_content, 
           "uploaded_image_video"=>$uploaded_image_video,
           "campaign_created"=>$current_time, 
           "posting_status"=>"0", 
           "last_updated_at"=>$current_time, 
           "tag_machine_enabled_post_list_id"=>$tag_machine_enabled_post_list_id, 
           "facebook_rx_fb_user_info_id"=>$facebook_rx_fb_user_info_id, 
           "user_id"=>$user_id, 
           "page_info_table_id"=>$page_info_table_id, 
           "page_id"=>$page_id, 
           "page_name"=>$page_name, 
           "page_profile"=>$page_profile, 
           "post_id"=>$post_id, 
           "post_created_at"=>$post_created_at, 
           "post_description"=>$post_description, 
           "commenter_count"=>$commenter_count,
           "schedule_type"=>$schedule_type,
           "schedule_time"=>$schedule_time,
           "time_zone"=>$time_zone
        );
        $this->basic->insert_data("tag_machine_bulk_tag",$data);
        $campaign_id=$this->db->insert_id();
        $this->_insert_usage_log($module_id=201,$request=1);
        $campaign_link=" <a href='".base_url('commenttagmachine/bulk_tag_campaign_list/0/'.$campaign_id)."'>".$this->lang->line('click here to see report.')."</a>";
        $success_message=$this->lang->line("Campagin has been created successfully.").$campaign_link;
        echo json_encode(array('status'=>'1','message'=>$success_message));

    }

    public function create_comment_reply_campaign_action()
    {
        if(!$_POST) exit();
        if($this->session->userdata('user_type') != 'Admin' && !in_array(202,$this->module_access))  exit();

        $status=$this->_check_usage($module_id=202,$request=1);
        if($status=="3")  
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('sorry, your monthly limit is exceeded for this module.')));
            exit();
        }

        $schedule_type = $this->input->post('schedule_type2');
        $schedule_time = $this->input->post('schedule_time2');
        $time_zone = $this->input->post('time_zone2');
        if($schedule_type=='now') $schedule_time='';

        $tag_machine_enabled_post_list_id = $this->input->post('bulk_comment_reply_campaign_enabled_post_list_id');
        $facebook_rx_fb_user_info_id = $this->session->userdata("facebook_rx_fb_user_info");
        $user_id = $this->user_id;
        $campaign_name = $this->input->post('campaign_name2');
        $reply_content = $this->input->post('message2');
        $uploaded_image_video = $this->input->post('uploaded_image_video2');
        $reply_multiple = $this->input->post('reply_multiple');
        $delay_time = $this->input->post('delay_time');
        if($delay_time=="") $delay_time=0;
        $delay_time=abs($delay_time);

        $page_info=$this->basic->get_data("tag_machine_enabled_post_list",array("where"=>array("id"=>$tag_machine_enabled_post_list_id,"facebook_rx_fb_user_info_id"=>$facebook_rx_fb_user_info_id)));
        if(count($page_info)==0)
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line('something went wrong, please try again.')));
            exit();
        }
        $page_info_table_id=isset($page_info[0]["page_info_table_id"])?$page_info[0]["page_info_table_id"]:"";
        $page_id=isset($page_info[0]["page_id"])?$page_info[0]["page_id"]:""; // fb page id
        $page_name=isset($page_info[0]["page_name"])?$page_info[0]["page_name"]:"";
        $page_profile=isset($page_info[0]["page_profile"])?$page_info[0]["page_profile"]:"";
        $post_id=isset($page_info[0]["post_id"])?$page_info[0]["post_id"]:"";
        $post_created_at=isset($page_info[0]["post_created_at"])?$page_info[0]["post_created_at"]:"";
        $post_description=isset($page_info[0]["post_description"])?$page_info[0]["post_description"]:"";

        $comment_list = $this->basic->get_data("tag_machine_comment_info",array("where"=>array("tag_machine_enabled_post_list_id"=>$tag_machine_enabled_post_list_id,"subscribed"=>"1")),"","","","","comment_time DESC");
        $report=array();
        $comment_count=0;
        $only_commenter_id_array=array();
        foreach ($comment_list as $key => $value) 
        {
            if($reply_multiple=='0')
            {
                if(in_array($value["commenter_fb_id"], $only_commenter_id_array))
                continue;
            } 

            $report[$value["comment_id"]]=
            array
            (
                "commenter_name"=>$value["commenter_name"],
                "commenter_fb_id"=>$value["commenter_fb_id"],
                "comment_id"=>$value["comment_id"],
                "comment_time"=>$value["comment_time"],
                "status"=>'Pending',
                "replied_at"=>"x"
            );
            $only_commenter_id_array[]=$value["commenter_fb_id"];
            $comment_count++;
        }
        $report_json=json_encode($report);
        $current_time=date("Y-m-d H:i:s");
        $data = array
        (
           "campaign_name"=>$campaign_name, 
           "reply_content"=>$reply_content, 
           "uploaded_image_video"=>$uploaded_image_video,
           "reply_multiple"=>$reply_multiple,
           "report"=>$report_json,
           "campaign_created"=>$current_time, 
           "posting_status"=>"0", 
           "delay_time"=>$delay_time, 
           "is_try_again"=>'1', 
           "total_reply"=>$comment_count, 
           "last_updated_at"=>$current_time, 
           "tag_machine_enabled_post_list_id"=>$tag_machine_enabled_post_list_id, 
           "facebook_rx_fb_user_info_id"=>$facebook_rx_fb_user_info_id, 
           "user_id"=>$user_id, 
           "page_info_table_id"=>$page_info_table_id, 
           "page_id"=>$page_id, 
           "page_name"=>$page_name, 
           "page_profile"=>$page_profile, 
           "post_id"=>$post_id, 
           "post_created_at"=>$post_created_at, 
           "post_description"=>$post_description,
           "schedule_type"=>$schedule_type,
           "schedule_time"=>$schedule_time,
           "time_zone"=>$time_zone
        );

        $this->db->trans_start();

        $this->basic->insert_data("tag_machine_bulk_reply",$data);
        $campaign_id=$this->db->insert_id();
        $this->_insert_usage_log($module_id=202,$request=1);

        foreach ($report as $key => $value) 
        {
            $insert_now=$value;
            $insert_now['campaign_id']=$campaign_id;
            unset($insert_now['status']);
            unset($insert_now['replied_at']);
            $this->basic->insert_data("tag_machine_bulk_reply_send",$insert_now);
        }
        
        $this->db->trans_complete();
        if($this->db->trans_status() === false) 
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line("something went wrong, please try again.")));
            exit();
        }

        $campaign_link=" <a href='".base_url('commenttagmachine/bulk_comment_reply_campaign_list/0/'.$campaign_id)."'>".$this->lang->line('click here to see report.')."</a>";
        $success_message=$this->lang->line("Campagin has been created successfully.").$campaign_link;
        echo json_encode(array('status'=>'1','message'=>$success_message));

    }

    public function bulk_tag_campaign_list($tag_machine_enabled_post_list=0,$campaign_id=0)
    {
        if($this->session->userdata('user_type') != 'Admin' && !in_array(201,$this->module_access))
        redirect('home/login_page', 'location'); 

        $data['body'] = "bulk_tag_campaign_list";
        $data['page_title'] = $this->lang->line("Comment & Bulk Tag Campaign Report");
        $page_info=$this->basic->get_data("tag_machine_enabled_post_list",array("where"=>array("tag_machine_enabled_post_list.facebook_rx_fb_user_info_id"=>$this->session->userdata("facebook_rx_fb_user_info"))),array("facebook_rx_fb_page_info.*"),array('facebook_rx_fb_page_info'=>"tag_machine_enabled_post_list.page_info_table_id=facebook_rx_fb_page_info.id,left"),'','','facebook_rx_fb_page_info.page_name ASC','tag_machine_enabled_post_list.page_info_table_id');
        $data['page_info'] = $page_info;
        $data['auto_search_enabled_post_list'] = $tag_machine_enabled_post_list;
        $data['auto_search_campaign_id'] = $campaign_id;
        $this->session->set_userdata("bulk_tag_campaign_list_auto_search_enabled_post_list_id",$tag_machine_enabled_post_list);
        $this->session->set_userdata("bulk_tag_campaign_list_auto_search_campaign_id",$campaign_id);
        $data["time_zone"]= $this->_time_zone_list();
        $this->_viewcontroller($data);
    }

    public function bulk_tag_campaign_list_data()
    {
        if($this->session->userdata('user_type') != 'Admin' && !in_array(201,$this->module_access))
        redirect('home/login_page', 'location'); 

        if ($_SERVER['REQUEST_METHOD'] === 'GET') exit();

        $page = isset($_POST['page']) ? intval($_POST['page']) : 15;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 5;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';

        $search_campaign_name = trim($this->input->post("search_campaign_name", true));
        $search_page_id = trim($this->input->post("search_page_id", true));
        $search_post_id = trim($this->input->post("search_post_id", true));
        $search_enable_id = trim($this->input->post("search_enable_id", true)); // auto search
        $search_campaign_id = trim($this->input->post("search_campaign_id", true)); // auto search

        $schedule_time_from = trim($this->input->post('schedule_time_from', true));
        if($schedule_time_from) $schedule_time_from = date('Y-m-d H:i:s', strtotime($schedule_time_from));

        $schedule_time_to = trim($this->input->post('schedule_time_to', true));
        if($schedule_time_to) $schedule_time_to = date('Y-m-d H:i:s', strtotime($schedule_time_to));

        $is_searched = $this->input->post('is_searched', true);


        if($is_searched)
        {
            $this->session->set_userdata('bulk_tag_campaign_list_campaign_name', $search_campaign_name);
            $this->session->set_userdata('bulk_tag_campaign_list_page_id', $search_page_id);
            $this->session->set_userdata('bulk_tag_campaign_list_post_id', $search_post_id);
            $this->session->set_userdata('bulk_tag_campaign_list_schedule_time_from', $schedule_time_from);
            $this->session->set_userdata('bulk_tag_campaign_list_schedule_time_to', $schedule_time_to);
            $this->session->unset_userdata('bulk_tag_campaign_list_campaign_id');//auto search
            $this->session->unset_userdata('bulk_tag_campaign_list_enable_id');//auto search
        }
        else // auto search
        {
          if($this->session->userdata("bulk_tag_campaign_list_auto_search_enabled_post_list_id")!=0 || $this->session->userdata('bulk_tag_campaign_list_auto_search_campaign_id')!=0)
          {            
            $this->session->unset_userdata('bulk_tag_campaign_list_campaign_name');
            $this->session->unset_userdata('bulk_tag_campaign_list_page_id');
            $this->session->unset_userdata('bulk_tag_campaign_list_post_id');
            $this->session->unset_userdata('bulk_tag_campaign_list_schedule_time_from');
            $this->session->unset_userdata('bulk_tag_campaign_list_schedule_time_to');

            $this->session->set_userdata('bulk_tag_campaign_list_campaign_id',$this->session->userdata("bulk_tag_campaign_list_auto_search_campaign_id"));//auto search
            $this->session->set_userdata('bulk_tag_campaign_list_enable_id',$this->session->userdata("bulk_tag_campaign_list_auto_search_enabled_post_list_id"));//auto search
          }
        }

        $search_campaign_name  = $this->session->userdata('bulk_tag_campaign_list_campaign_name');
        $search_page_id  = $this->session->userdata('bulk_tag_campaign_list_page_id');
        $search_post_id  = $this->session->userdata('bulk_tag_campaign_list_post_id');
        $schedule_time_from  = $this->session->userdata('bulk_tag_campaign_list_schedule_time_from');
        $schedule_time_to = $this->session->userdata('bulk_tag_campaign_list_schedule_time_to');

        //==================================auto search===============================
        $search_enabled_post_id = $this->session->userdata('bulk_tag_campaign_list_enable_id');
        $search_campaign_id = $this->session->userdata('bulk_tag_campaign_list_campaign_id');
        //==================================auto search===============================

        $where_simple=array();

        if ($search_campaign_name) $where_simple['campaign_name like ']    = "%".$search_campaign_name."%";
        if ($search_page_id) $where_simple['page_info_table_id']    = $search_page_id;
        if ($search_post_id) $where_simple['post_id like ']    = "%".$search_post_id."%";

        if ($schedule_time_from)
        {
            if ($schedule_time_from != '1970-01-01')
            $where_simple["Date_Format(schedule_time,'%Y-%m-%d') >="]= $schedule_time_from;

        }
        if ($schedule_time_to)
        {
            if ($schedule_time_to != '1970-01-01')
            $where_simple["Date_Format(schedule_time,'%Y-%m-%d') <="]=$schedule_time_to;
        }

        //==================================auto search===============================
         if ($search_campaign_id) $where_simple['id']    = $search_campaign_id;
         if ($search_enabled_post_id) $where_simple['tag_machine_enabled_post_list_id'] = $search_enabled_post_id;
        //==================================auto search===============================

        $where_simple['facebook_rx_fb_user_info_id'] = $this->session->userdata("facebook_rx_fb_user_info");
        $order_by_str=$sort." ".$order;
        $offset = ($page-1)*$rows;
        $where = array('where' => $where_simple);

        $table = "tag_machine_bulk_tag";
        $info = $this->basic->get_data($table,$where,$select='',$join='',$limit=$rows, $start=$offset,$order_by=$order_by_str);
        // echo $this->db->last_query();

        for($i=0;$i<count($info);$i++)
        {
            $info[$i]['campaign_created'] = date("M j, y H:i",strtotime($info[$i]['campaign_created']));
            $info[$i]['last_updated_at'] = date("M j, y H:i",strtotime($info[$i]['last_updated_at']));

            if($info[$i]['schedule_type']=='later') $info[$i]['schedule_time'] = date("M j, y H:i",strtotime($info[$i]['schedule_time']));
            else $info[$i]['schedule_time'] = $this->lang->line("Instant");

            $info[$i]['report'] = "<a class='btn-sm btn btn-outline-info show_report' data-id='".$info[$i]['id']."'><i class='fa fa-list'></i> ".$this->lang->line('Report')."</a>";

            if($info[$i]['uploaded_image_video']!='')
            $info[$i]["attachment"] = "<a target='_BLANK' href='".base_url('upload/commenttagmachine/'.$info[$i]['uploaded_image_video'])."' class='label label-light'><i class='green fa fa-paperclip'></i> ".$this->lang->line('yes')."</a>";
            else $info[$i]["attachment"] = "<span class='label label-light'><i class='red fa fa-remove'></i> ".$this->lang->line('no')."</span>";

            if($info[$i]['posting_status']=='0' && $info[$i]['schedule_type']=='later')
            $info[$i]['edit'] =  "<a href='".base_url("commenttagmachine/edit_bulk_tag_campaign/".$info[$i]["id"])."' title='".$this->lang->line("Edit")."' class='btn-sm btn btn-outline-primary'><i class='fa fa-pencil'></i> ".$this->lang->line("Edit")."</a>";
            else $info[$i]['edit'] = "<i class='fa fa-remove'></i>";

            if($info[$i]['posting_status']!='1') 
            $info[$i]['delete'] = "<a data-id='".$info[$i]["id"]."' title='".$this->lang->line("Delete")."' class='delete_campaign btn-sm btn btn-outline-danger'><i class='fa fa-trash'></i> ".$this->lang->line("Delete")."</a>";
            else $info[$i]['delete'] = "<i class='fa fa-remove'></i>";

            if($info[$i]['posting_status']=='2') $info[$i]['posting_status'] =  "<span class='label label-light'><i class='green fa fa-check-circle'></i> ".$this->lang->line('completed')."</span>";
            else if($info[$i]['posting_status']=='1') $info[$i]['posting_status'] =  "<span class='label label-light'><i class='orange fa fa-spinner'></i> ".$this->lang->line('processing')."</span>";
            else $info[$i]['posting_status'] =  "<span class='label label-light'><i class='red fa fa-remove'></i> ".$this->lang->line('pending')."</span>";
          
        }
        $total_rows_array = $this->basic->count_row($table, $where, $count = "id");
        $total_result = $total_rows_array[0]['total_rows'];

        echo convert_to_grid_data($info, $total_result);
    }

    public function bulk_tag_campaign_report()
    {
        if(!$_POST) exit();
        if($this->session->userdata('user_type') != 'Admin' && !in_array(201,$this->module_access)) exit();

        $id = $this->input->post("id");
        $table="tag_machine_bulk_tag";        

        $report_data = $this->basic->get_data($table,array("where"=>array("id"=>$id,"facebook_rx_fb_user_info_id"=>$this->session->userdata("facebook_rx_fb_user_info"))));
        $report=isset($report_data[0]["tag_database"])?json_decode($report_data[0]["tag_database"],true):array();
        $response = "";
        if(count($report)==0)
        {
            $response.= "<h4><div class='alert alert-warning text-center'>".$this->lang->line("no data found")."</div></h4>";
            echo $response;
            exit();
        }

        $response .= '<script>
                    $j(document).ready(function() {
                      $(".table-responsive").mCustomScrollbar({
                        autoHideScrollbar:true,
                        theme:"3d-dark",          
                        axis: "x"
                      });  
                      $("#post_commenter_table").DataTable();
                    });
                 </script>';

        $campaign_name=isset($report_data[0]["campaign_name"])?$report_data[0]["campaign_name"]:"";
        $postid=isset($report_data[0]["post_id"])?$report_data[0]["post_id"]:"";
        $pagename=isset($report_data[0]["page_name"])?$report_data[0]["page_name"]:"";
        $pageid=isset($report_data[0]["page_id"])?$report_data[0]["page_id"]:"";
        $tag_content=isset($report_data[0]["tag_content"])?$report_data[0]["tag_content"]:"";
        $response.= "<h3 class='text-center'>".$campaign_name."</h4><br>";
        $response.= "<h4 class='text-center'><a style='color:#607D8B;' target='_BLANK' href='https://facebook.com/".$pageid."'><i class='fa fa-newspaper-o'></i> ".$pagename."</a> <a class='orange' target='_BLANK' href='https://facebook.com/".$postid."'> (".$this->lang->line('Visit Post').")</a></h4>";
       
        if($report_data[0]["error_message"]!="") $response.="<br><div class='alert alert-danger text-center' style='padding-top:7px;padding-bottom:7px;'>".$report_data[0]['error_message']."</div>";
        if($report_data[0]["tag_response"]!="") $response.="<br><div class='alert alert-success text-center' style='padding-top:7px;padding-bottom:7px;'><i class='fa fa-check-square'></i> ".$this->lang->line("Bulk tagged comment has been posted successfully.")." <a target='_BLANK' href='https://facebook.com/".$report_data[0]['tag_response']."'>".$this->lang->line("See Comment")."</a></div>";
    
        $response .="<div class='table-responsive'>";
        $response .="<table id='post_commenter_table' class='table table-hover table-bordered table-striped table-condensed nowrap'>";
        $response .= "<thead><tr>";
        $response .= "<th class='text-center'></th>";
        $response .= "<th>{$this->lang->line("Commenter Name")}</th>";
        $response .= "<th class='text-center'>{$this->lang->line("Last Comment ID")}</th>";
        $response .= "<th class='text-center'>{$this->lang->line("Last Comment Time")}</th>";
        $response .= "</tr></thead>";
        $i=0;
        foreach ($report as $key2 => $value)
        {
            $i++;
            $response .= "<tr>";
            $response .= "<td class='text-center'>".$i."</td>";
            $response .= "<td><a target='_BLANK' href='https://facebook.com/".$value["commenter_fb_id"]."'>".$value["commenter_name"]."</a></td>";
            $response .= "<td class='text-center'><a target='_BLANK' href='https://facebook.com/".$value["last_comment_id"]."'>".$value["last_comment_id"]."</a></td>";
            $response .= "<td class='text-center'>".date("M j, y H:i",strtotime($value["last_comment_time"]))."</td>";
            $response .= "</td>";

            $response .= "</tr>";          
        }
        $response .= "</table><div class='well' style='background: #fff !important;border: none !important;min-height: 20px; !importantpadding: 19px !important;margin-bottom: 20px !important;-webkit-box-shadow: none !important;-moz-box-shadow: none !important;border-top: solid 1px #efefef !important;box-shadow: none !important;'><b>".$this->lang->line('tag content')."</b>: ".$tag_content."</div></div>";

        echo $response;
    }

    public function delete_bulk_tag_campaign($id=0)
    {
        if(!$_POST) exit();
        if($this->session->userdata('user_type') != 'Admin' && !in_array(201,$this->module_access)) exit();
        $id=$this->input->post("id");

        $xdata = $this->basic->get_data("tag_machine_bulk_tag",array("where"=>array("id"=>$id,"facebook_rx_fb_user_info_id"=>$this->session->userdata("facebook_rx_fb_user_info"))));

        $posting_status  = isset($xdata[0]["posting_status"]) ? $xdata[0]["posting_status"] : "";

        if($posting_status=="0") // removing usage data if deleted and campaign is pending
        $this->_delete_usage_log($module_id=201,$request=1);
        
        if($this->basic->delete_data("tag_machine_bulk_tag",array("id"=>$id,"facebook_rx_fb_user_info_id"=>$this->session->userdata("facebook_rx_fb_user_info"))))
        echo "1";        
        else echo "0";
    }

    public function edit_bulk_tag_campaign($id='')
    {
        if($id==0) exit();

        $data['body'] = "edit_bulk_tag_campaign";
        $data['page_title'] = $this->lang->line("Edit Comment & Bulk Tag Campaign");
        $data["time_zone"]= $this->_time_zone_list();
        $data["xdata"] = $this->basic->get_data("tag_machine_bulk_tag",array("where"=>array("id"=>$id,"facebook_rx_fb_user_info_id"=>$this->session->userdata("facebook_rx_fb_user_info"))));

        // only pending campaigns are editable
        if(!isset($data["xdata"][0]["posting_status"]) || $data["xdata"][0]["posting_status"]!='0' ) exit();
        // only scheduled campaigns can be editted
        if($data["xdata"][0]["schedule_type"]!='later') exit();

        $tag_machine_enabled_post_list_id=$data["xdata"][0]["tag_machine_enabled_post_list_id"];
        $commenter_list = $this->basic->count_row("tag_machine_commenter_info",array("where"=>array("tag_machine_enabled_post_list_id"=>$tag_machine_enabled_post_list_id,"subscribed"=>"1")));
        $commenter_count=$commenter_list[0]['total_rows'];
       
        $item_per_range=$this->config->item('item_per_range');
        if($item_per_range=='') $item_per_range=50;

        $cal=ceil($commenter_count/$item_per_range);
        $resposne='';
        for($i=1;$i<=$cal;$i++)
        {
            if($i==1)
            {
                $start=$i;
                $end=$item_per_range;
            }
            else
            {
                $start=$end+1;
                $end=$end+$item_per_range;
            }
            if($end>=$commenter_count) $end=$commenter_count;
            if($start==$end) continue;

            $resposne.= '<option style="padding:5px;" value="'.$start.'-'.$end.'">&nbsp;&nbsp;&nbsp; o &nbsp;'.$this->lang->line("Latest")." ".$start.'-'.$end.'</option>';
        }
        $data["commenter_range"]=$resposne;
    
        $previous_exclude = isset($data["xdata"][0]["tag_exclude"]) ? json_decode($data["xdata"][0]["tag_exclude"],true) : array();

        $data["xtag_exclude"]=array();
        if(count($previous_exclude)>0)
        $data["xtag_exclude"] = $this->basic->get_data("tag_machine_commenter_info",array("where_in"=>array("commenter_fb_id"=>$previous_exclude)));

        $this->_viewcontroller($data);
    }

    public function edit_bulk_tag_campaign_action()
    {
        if(!$_POST) exit();
        if($this->session->userdata('user_type') != 'Admin' && !in_array(201,$this->module_access))  exit();
  
        $campaign_id = $this->input->post('campaign_id');
        $schedule_type = "later";
        $schedule_time = $this->input->post('schedule_time');
        $time_zone = $this->input->post('time_zone');

        $tag_machine_enabled_post_list_id = $this->input->post('tag_campaign_tag_machine_enabled_post_list_id');
        $facebook_rx_fb_user_info_id = $this->session->userdata("facebook_rx_fb_user_info");
        $user_id = $this->user_id;
        $campaign_name = $this->input->post('campaign_name');
        $tag_content = $this->input->post('message');
        $uploaded_image_video = $this->input->post('uploaded_image_video');
        $commenter_range = $this->input->post('commenter_range');

        $item_per_range=$this->config->item('item_per_range');
        if($item_per_range=='') $item_per_range=50;

        $explode_range=explode('-', $commenter_range);
        $start=isset($explode_range[0])?$explode_range[0]:1;
        $end=isset($explode_range[1])?$explode_range[1]:$item_per_range;
        $comenter_limit=($end-$start)+1;
        $commenter_start=$start-1;
        
        $tag_exclude = $this->input->post('exclude');
        if(!is_array($tag_exclude)) $tag_exclude = array();

        $commenter_list = $this->basic->get_data("tag_machine_commenter_info",array("where"=>array("tag_machine_enabled_post_list_id"=>$tag_machine_enabled_post_list_id,"subscribed"=>"1")),"","",$comenter_limit,$commenter_start,"last_comment_time DESC");
        $tag_database_array=array();
        $commenter_count=0;
        foreach ($commenter_list as $key => $value) 
        {
            if(in_array($value['commenter_fb_id'], $tag_exclude)) continue;

            $tag_database_array[$value["commenter_fb_id"]]=
            array
            (
                "commenter_name"=>$value["commenter_name"],
                "commenter_fb_id"=>$value["commenter_fb_id"],
                "last_comment_id"=>$value["last_comment_id"],
                "last_comment_time"=>$value["last_comment_time"]
            );
            $commenter_count++;
        }
        $tag_database=json_encode($tag_database_array);
        $current_time=date("Y-m-d H:i:s");
        $data = array
        (
           "campaign_name"=>$campaign_name, 
           "tag_database"=>$tag_database, 
           "tag_exclude"=>json_encode($tag_exclude), 
           "tag_content"=>$tag_content, 
           "uploaded_image_video"=>$uploaded_image_video,
           "last_updated_at"=>$current_time,            
           "commenter_count"=>$commenter_count,
           "schedule_time"=>$schedule_time,
           "time_zone"=>$time_zone
        );
        $this->basic->update_data("tag_machine_bulk_tag",array("id"=>$campaign_id,"facebook_rx_fb_user_info_id"=>$facebook_rx_fb_user_info_id),$data);
        $campaign_link=" <a href='".base_url('commenttagmachine/bulk_tag_campaign_list/0/'.$campaign_id)."'>".$this->lang->line('click here to see report.')."</a>";
        $success_message=$this->lang->line("Campagin has been updated successfully.").$campaign_link;
        echo json_encode(array('status'=>'1','message'=>$success_message));

    }

    public function bulk_comment_reply_campaign_list($tag_machine_enabled_post_list=0,$campaign_id=0)
    {
        if($this->session->userdata('user_type') != 'Admin' && !in_array(202,$this->module_access))
        redirect('home/login_page', 'location'); 

        $data['body'] = "bulk_comment_reply_campaign_list";
        $data['page_title'] = $this->lang->line("Bulk Comment Reply Campaign Report");
        $page_info=$this->basic->get_data("tag_machine_enabled_post_list",array("where"=>array("tag_machine_enabled_post_list.facebook_rx_fb_user_info_id"=>$this->session->userdata("facebook_rx_fb_user_info"))),array("facebook_rx_fb_page_info.*"),array('facebook_rx_fb_page_info'=>"tag_machine_enabled_post_list.page_info_table_id=facebook_rx_fb_page_info.id,left"),'','','facebook_rx_fb_page_info.page_name ASC','tag_machine_enabled_post_list.page_info_table_id');
        $data['page_info'] = $page_info;
        $data['auto_search_enabled_post_list'] = $tag_machine_enabled_post_list;
        $data['auto_search_campaign_id'] = $campaign_id;
        $this->session->set_userdata("bulk_comment_reply_campaign_list_auto_search_enabled_post_list_id",$tag_machine_enabled_post_list);
        $this->session->set_userdata("bulk_comment_reply_campaign_list_auto_search_campaign_id",$campaign_id);
        $data["time_zone"]= $this->_time_zone_list();
        $this->_viewcontroller($data);
    }

    public function bulk_comment_reply_campaign_list_data()
    {
        if($this->session->userdata('user_type') != 'Admin' && !in_array(202,$this->module_access))
        redirect('home/login_page', 'location'); 

        if ($_SERVER['REQUEST_METHOD'] === 'GET') exit();

        $page = isset($_POST['page']) ? intval($_POST['page']) : 15;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 5;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'DESC';

        $search_campaign_name = trim($this->input->post("search_campaign_name", true));
        $search_page_id = trim($this->input->post("search_page_id", true));
        $search_post_id = trim($this->input->post("search_post_id", true));
        $search_enable_id = trim($this->input->post("search_enable_id", true)); // auto search
        $search_campaign_id = trim($this->input->post("search_campaign_id", true)); // auto search

        $schedule_time_from = trim($this->input->post('schedule_time_from', true));
        if($schedule_time_from) $schedule_time_from = date('Y-m-d H:i:s', strtotime($schedule_time_from));

        $schedule_time_to = trim($this->input->post('schedule_time_to', true));
        if($schedule_time_to) $schedule_time_to = date('Y-m-d H:i:s', strtotime($schedule_time_to));

        $is_searched = $this->input->post('is_searched', true);


        if($is_searched)
        {
            $this->session->set_userdata('bulk_comment_reply_campaign_list_campaign_name', $search_campaign_name);
            $this->session->set_userdata('bulk_comment_reply_campaign_list_page_id', $search_page_id);
            $this->session->set_userdata('bulk_comment_reply_campaign_list_post_id', $search_post_id);
            $this->session->set_userdata('bulk_comment_reply_campaign_list_schedule_time_from', $schedule_time_from);
            $this->session->set_userdata('bulk_comment_reply_campaign_list_schedule_time_to', $schedule_time_to);
            $this->session->unset_userdata('bulk_comment_reply_campaign_list_campaign_id');//auto search
            $this->session->unset_userdata('bulk_comment_reply_campaign_list_enable_id');//auto search
        }
        else // auto search
        {
          if($this->session->userdata("bulk_comment_reply_campaign_list_auto_search_enabled_post_list_id")!=0 || $this->session->userdata('bulk_comment_reply_campaign_list_auto_search_campaign_id')!=0)
          {            
            $this->session->unset_userdata('bulk_comment_reply_campaign_list_campaign_name');
            $this->session->unset_userdata('bulk_comment_reply_campaign_list_page_id');
            $this->session->unset_userdata('bulk_comment_reply_campaign_list_post_id');
            $this->session->unset_userdata('bulk_comment_reply_campaign_list_schedule_time_from');
            $this->session->unset_userdata('bulk_comment_reply_campaign_list_schedule_time_to');

            $this->session->set_userdata('bulk_comment_reply_campaign_list_campaign_id',$this->session->userdata("bulk_comment_reply_campaign_list_auto_search_campaign_id"));//auto search
            $this->session->set_userdata('bulk_comment_reply_campaign_list_enable_id',$this->session->userdata("bulk_comment_reply_campaign_list_auto_search_enabled_post_list_id"));//auto search
          }
        }

        $search_campaign_name  = $this->session->userdata('bulk_comment_reply_campaign_list_campaign_name');
        $search_page_id  = $this->session->userdata('bulk_comment_reply_campaign_list_page_id');
        $search_post_id  = $this->session->userdata('bulk_comment_reply_campaign_list_post_id');
        $schedule_time_from  = $this->session->userdata('bulk_comment_reply_campaign_list_schedule_time_from');
        $schedule_time_to = $this->session->userdata('bulk_comment_reply_campaign_list_schedule_time_to');

        //==================================auto search===============================
        $search_enabled_post_id = $this->session->userdata('bulk_comment_reply_campaign_list_enable_id');
        $search_campaign_id = $this->session->userdata('bulk_comment_reply_campaign_list_campaign_id');
        //==================================auto search===============================

        $where_simple=array();

        if ($search_campaign_name) $where_simple['campaign_name like ']    = "%".$search_campaign_name."%";
        if ($search_page_id) $where_simple['page_info_table_id']    = $search_page_id;
        if ($search_post_id) $where_simple['post_id like ']    = "%".$search_post_id."%";

        if ($schedule_time_from)
        {
            if ($schedule_time_from != '1970-01-01')
            $where_simple["Date_Format(schedule_time,'%Y-%m-%d') >="]= $schedule_time_from;

        }
        if ($schedule_time_to)
        {
            if ($schedule_time_to != '1970-01-01')
            $where_simple["Date_Format(schedule_time,'%Y-%m-%d') <="]=$schedule_time_to;
        }

        //==================================auto search===============================
         if ($search_campaign_id) $where_simple['id']    = $search_campaign_id;
         if ($search_enabled_post_id) $where_simple['tag_machine_enabled_post_list_id'] = $search_enabled_post_id;
        //==================================auto search===============================

        $where_simple['facebook_rx_fb_user_info_id'] = $this->session->userdata("facebook_rx_fb_user_info");
        $order_by_str=$sort." ".$order;
        $offset = ($page-1)*$rows;
        $where = array('where' => $where_simple);

        $table = "tag_machine_bulk_reply";
        $info = $this->basic->get_data($table,$where,$select='',$join='',$limit=$rows, $start=$offset,$order_by=$order_by_str);
        // echo $this->db->last_query();

        for($i=0;$i<count($info);$i++)
        {
            $info[$i]['campaign_created'] = date("M j, y H:i",strtotime($info[$i]['campaign_created']));
            $info[$i]['last_updated_at'] = date("M j, y H:i",strtotime($info[$i]['last_updated_at']));

            if($info[$i]['schedule_type']=='later') $info[$i]['schedule_time'] = date("M j, y H:i",strtotime($info[$i]['schedule_time']));
            else $info[$i]['schedule_time'] = $this->lang->line("Instant");

            $info[$i]['report'] = "<a class='btn-sm btn btn-outline-info show_report' data-id='".$info[$i]['id']."'><i class='fa fa-list'></i> ".$this->lang->line('Report')."</a>";

            if($info[$i]['uploaded_image_video']!='')
            $info[$i]["attachment"] = "<a target='_BLANK' href='".base_url('upload/commenttagmachine/'.$info[$i]['uploaded_image_video'])."' class='label label-light'><i class='green fa fa-paperclip'></i> ".$this->lang->line('yes')."</a>";
            else $info[$i]["attachment"] = "<span class='label label-light'><i class='red fa fa-remove'></i> ".$this->lang->line('no')."</span>";

            if($info[$i]['posting_status']=='0' && $info[$i]['schedule_type']=='later')
            $info[$i]['edit'] =  "<a href='".base_url("commenttagmachine/edit_bulk_comment_reply_campaign/".$info[$i]["id"])."' title='".$this->lang->line("Edit")."' class='btn-sm btn btn-outline-primary'><i class='fa fa-edit'></i> ".$this->lang->line("Edit")."</a>";
            else $info[$i]['edit'] = "<i class='fa fa-remove'></i>";

            if($info[$i]['posting_status']!='1') 
            $info[$i]['delete'] = "<a data-id='".$info[$i]["id"]."' title='".$this->lang->line("Delete")."' class='delete_campaign btn-sm btn btn-outline-danger'><i class='fa fa-trash'></i> ".$this->lang->line("Delete")."</a>";
            else $info[$i]['delete'] = "<i class='fa fa-remove'></i>";

            if($info[$i]['posting_status']=='2') $info[$i]['posting_status'] =  "<span class='label label-light'><i class='green fa fa-check-circle'></i> ".$this->lang->line('completed')."</span>";
            else if($info[$i]['posting_status']=='1') $info[$i]['posting_status'] =  "<span class='label label-light'><i class='orange fa fa-spinner'></i> ".$this->lang->line('processing')."</span>";
            else $info[$i]['posting_status'] =  "<span class='label label-light'><i class='red fa fa-remove'></i> ".$this->lang->line('pending')."</span>";          
          
        }
        $total_rows_array = $this->basic->count_row($table, $where, $count = "id");
        $total_result = $total_rows_array[0]['total_rows'];

        echo convert_to_grid_data($info, $total_result);
    }

    public function bulk_comment_reply_campaign_report()
    {
        if(!$_POST) exit();
        if($this->session->userdata('user_type') != 'Admin' && !in_array(202,$this->module_access)) exit();

        $id = $this->input->post("id");
        $table="tag_machine_bulk_reply";        

        $report_data = $this->basic->get_data($table,array("where"=>array("id"=>$id,"facebook_rx_fb_user_info_id"=>$this->session->userdata("facebook_rx_fb_user_info"))));
        $report=isset($report_data[0]["report"])?json_decode($report_data[0]["report"],true):array();
        $response = "";
        if(count($report)==0)
        {
            $response.= "<h4><div class='alert alert-warning text-center'>".$this->lang->line("no data found")."</div></h4>";
            echo $response;
            exit();
        }

        $response .= '<script>
                    $j(document).ready(function() {
                      $(".table-responsive").mCustomScrollbar({
                          autoHideScrollbar:true,
                          theme:"3d-dark",          
                          axis: "x"
                      }); 
                      $("#post_commenter_table").DataTable();
                    });
                 </script>';

        $campaign_name=isset($report_data[0]["campaign_name"])?$report_data[0]["campaign_name"]:"";
        $postid=isset($report_data[0]["post_id"])?$report_data[0]["post_id"]:"";
        $pagename=isset($report_data[0]["page_name"])?$report_data[0]["page_name"]:"";
        $pageid=isset($report_data[0]["page_id"])?$report_data[0]["page_id"]:"";
        $reply_content=isset($report_data[0]["reply_content"])?$report_data[0]["reply_content"]:"";
        $response.= "<h3 class='text-center'>".$campaign_name."</h4><br>";
        $response.= "<h4 class='text-center'><a style='color:607D8B !important' target='_BLANK' href='https://facebook.com/".$pageid."'><i class='fa fa-newspaper-o'></i> ".$pagename."</a> <a class='orange' target='_BLANK' href='https://facebook.com/".$postid."'> (".$this->lang->line('Visit Post').")</a></h4>";
       
        if($report_data[0]["error_message"]!="") $response.="<br><div class='alert alert-danger text-center' style='padding-top:7px;padding-bottom:7px;'>".$report_data[0]['error_message']."</div>";
         
        $response .="<div class='table-responsive'>";
        $response .="<table id='post_commenter_table' class='table table-hover table-bordered table-striped table-condensed nowrap'>";
        $response .= "<thead><tr>";
        $response .= "<th class='text-center'></th>";
        $response .= "<th>{$this->lang->line("Commenter Name")}</th>";
        $response .= "<th class='text-center'>{$this->lang->line("Comment ID")}</th>";
        $response .= "<th>{$this->lang->line("Reply Status")}</th>";
        $response .= "<th class='text-center'>{$this->lang->line("Replied at")}</th>";
        $response .= "<th class='text-center'>{$this->lang->line("Comment Time")}</th>";
        $response .= "</tr></thead>";
        $i=0;
        foreach ($report as $key2 => $value)
        {
            $i++;
            $response .= "<tr>";
            $response .= "<td class='text-center'>".$i."</td>";
            $response .= "<td><a target='_BLANK' href='https://facebook.com/".$value["commenter_fb_id"]."'>".$value["commenter_name"]."</a></td>";
            $response .= "<td class='text-center'><a target='_BLANK' href='https://facebook.com/".$value["comment_id"]."'>".$value["comment_id"]."</a></td>";
            $response .= "<td class='text-center'>";
                if($value["status"]=="Pending") $reply_status=$this->lang->line('pending');
                else $reply_status="<a target='_BLANK' href='https://facebook.com/".$value["status"]."'>".$value["status"]."</a>";
                $response .=  $reply_status;
            $response .= "</td>";
            $response .= "<td class='text-center'>";
              if($value["replied_at"]!='x') $response .= date("M j, y H:i",strtotime($value["replied_at"]));
              else $response .= $value["replied_at"];
            $response .= "</td>";
            $response .= "<td class='text-center'>".date("M j, y H:i",strtotime($value["comment_time"]))."</td>";

            $response .= "</tr>";          
        }
        $response .= "</table><div class='well' style='background: #fff !important;border: none !important;min-height: 20px; !importantpadding: 19px !important;margin-bottom: 20px !important;-webkit-box-shadow: none !important;-moz-box-shadow: none !important;border-top: solid 1px #efefef !important;box-shadow: none !important;'><b>".$this->lang->line('reply content')."</b>: ".$reply_content."</div></div>";

        echo $response;
    }

    public function delete_bulk_comment_reply_campaign($id=0)
    {
        if(!$_POST) exit();
        if($this->session->userdata('user_type') != 'Admin' && !in_array(202,$this->module_access)) exit();
        $id=$this->input->post("id");

        $xdata = $this->basic->get_data("tag_machine_bulk_reply",array("where"=>array("id"=>$id,"facebook_rx_fb_user_info_id"=>$this->session->userdata("facebook_rx_fb_user_info"))));

        $posting_status  = isset($xdata[0]["posting_status"]) ? $xdata[0]["posting_status"] : "";

        if($posting_status=="0") // removing usage data if deleted and campaign is pending
        $this->_delete_usage_log($module_id=202,$request=1);
        
        if($this->basic->delete_data("tag_machine_bulk_reply",array("id"=>$id,"facebook_rx_fb_user_info_id"=>$this->session->userdata("facebook_rx_fb_user_info"))))
        {
          if($this->basic->delete_data("tag_machine_bulk_reply_send",array("campaign_id"=>$id))) echo "1"; 
          else echo "0"; 
        }      
        else echo "0";
    }

    public function edit_bulk_comment_reply_campaign($id='')
    {
        if($id==0) exit();

        $data['body'] = "edit_bulk_comment_reply_campaign";
        $data['page_title'] = $this->lang->line("Edit Bulk Comment Reply Campaign");
        $data["time_zone"]= $this->_time_zone_list();
        $data["xdata"] = $this->basic->get_data("tag_machine_bulk_reply",array("where"=>array("id"=>$id,"facebook_rx_fb_user_info_id"=>$this->session->userdata("facebook_rx_fb_user_info"))));

        // only pending campaigns are editable
        if(!isset($data["xdata"][0]["posting_status"]) || $data["xdata"][0]["posting_status"]!='0' ) exit();
        // only scheduled campaigns can be editted
        if($data["xdata"][0]["schedule_type"]!='later') exit();
    
        $this->_viewcontroller($data);
    }

    public function edit_bulk_comment_reply_campaign_action()
    {
        if(!$_POST) exit();
        if($this->session->userdata('user_type') != 'Admin' && !in_array(202,$this->module_access))  exit();
  
        $campaign_id = $this->input->post('campaign_id');
        $schedule_type = "later";
        $schedule_time = $this->input->post('schedule_time');
        $time_zone = $this->input->post('time_zone');

        $tag_machine_enabled_post_list_id = $this->input->post('tag_campaign_tag_machine_enabled_post_list_id');
        $facebook_rx_fb_user_info_id = $this->session->userdata("facebook_rx_fb_user_info");
        $user_id = $this->user_id;
        $campaign_name = $this->input->post('campaign_name');
        $tag_content = $this->input->post('message');
        $uploaded_image_video = $this->input->post('uploaded_image_video');
        $commenter_range = $this->input->post('commenter_range');

        $item_per_range=$this->config->item('item_per_range');
        if($item_per_range=='') $item_per_range=50;

        $explode_range=explode('-', $commenter_range);
        $start=isset($explode_range[0])?$explode_range[0]:1;
        $end=isset($explode_range[1])?$explode_range[1]:$item_per_range;
        $comenter_limit=($end-$start)+1;
        $commenter_start=$start-1;
        
        $tag_exclude = $this->input->post('exclude');
        if(!is_array($tag_exclude)) $tag_exclude = array();

        $commenter_list = $this->basic->get_data("tag_machine_commenter_info",array("where"=>array("tag_machine_enabled_post_list_id"=>$tag_machine_enabled_post_list_id,"subscribed"=>"1")),"","",$comenter_limit,$commenter_start,"last_comment_time DESC");
        $tag_database_array=array();
        $commenter_count=0;
        foreach ($commenter_list as $key => $value) 
        {
            if(in_array($value['commenter_fb_id'], $tag_exclude)) continue;

            $tag_database_array[$value["commenter_fb_id"]]=
            array
            (
                "commenter_name"=>$value["commenter_name"],
                "commenter_fb_id"=>$value["commenter_fb_id"],
                "last_comment_id"=>$value["last_comment_id"],
                "last_comment_time"=>$value["last_comment_time"]
            );
            $commenter_count++;
        }
        $tag_database=json_encode($tag_database_array);
        $current_time=date("Y-m-d H:i:s");
        $data = array
        (
           "campaign_name"=>$campaign_name, 
           "tag_database"=>$tag_database, 
           "tag_exclude"=>json_encode($tag_exclude), 
           "tag_content"=>$tag_content, 
           "uploaded_image_video"=>$uploaded_image_video,
           "last_updated_at"=>$current_time,            
           "commenter_count"=>$commenter_count,
           "schedule_time"=>$schedule_time,
           "time_zone"=>$time_zone
        );
        $this->basic->update_data("tag_machine_bulk_tag",array("id"=>$campaign_id,"facebook_rx_fb_user_info_id"=>$facebook_rx_fb_user_info_id),$data);
        $campaign_link=" <a href='".base_url('commenttagmachine/bulk_tag_campaign_list/0/'.$campaign_id)."'>".$this->lang->line('click here to see report.')."</a>";
        $success_message=$this->lang->line("Campagin has been updated successfully.").$campaign_link;
        echo json_encode(array('status'=>'1','message'=>$success_message));

    }

    public function edit_comment_reply_campaign_action()
    {
        if(!$_POST) exit();
        if($this->session->userdata('user_type') != 'Admin' && !in_array(202,$this->module_access))  exit();

        $campaign_id = $this->input->post('campaign_id');
        $schedule_type = 'later';
        $schedule_time = $this->input->post('schedule_time2');
        $time_zone = $this->input->post('time_zone2');

        $tag_machine_enabled_post_list_id = $this->input->post('bulk_comment_reply_campaign_enabled_post_list_id');
        $facebook_rx_fb_user_info_id = $this->session->userdata("facebook_rx_fb_user_info");
        $user_id = $this->user_id;
        $campaign_name = $this->input->post('campaign_name2');
        $reply_content = $this->input->post('message2');
        $uploaded_image_video = $this->input->post('uploaded_image_video2');
        $reply_multiple = $this->input->post('reply_multiple');
        $delay_time = $this->input->post('delay_time');
        if($delay_time=="") $delay_time=0;
        $delay_time=abs($delay_time);

        $comment_list = $this->basic->get_data("tag_machine_comment_info",array("where"=>array("tag_machine_enabled_post_list_id"=>$tag_machine_enabled_post_list_id,"subscribed"=>"1")),"","","","","comment_time DESC");
        $report=array();
        $comment_count=0;
        $only_commenter_id_array=array();
        foreach ($comment_list as $key => $value) 
        {
            if($reply_multiple=='0')
            {
                if(in_array($value["commenter_fb_id"], $only_commenter_id_array))
                continue;
            } 

            $report[]=
            array
            (
                "commenter_name"=>$value["commenter_name"],
                "commenter_fb_id"=>$value["commenter_fb_id"],
                "comment_id"=>$value["comment_id"],
                "comment_time"=>$value["comment_time"],
                "status"=>'Pending',
                "replied_at"=>"x"
            );
            $only_commenter_id_array[]=$value["commenter_fb_id"];
            $comment_count++;
        }
        $report_json=json_encode($report);
        $current_time=date("Y-m-d H:i:s");
        $data = array
        (
           "campaign_name"=>$campaign_name, 
           "reply_content"=>$reply_content, 
           "uploaded_image_video"=>$uploaded_image_video,
           "reply_multiple"=>$reply_multiple,
           "report"=>$report_json,           
           "delay_time"=>$delay_time,            
           "total_reply"=>$comment_count, 
           "last_updated_at"=>$current_time,   
           "schedule_time"=>$schedule_time,
           "time_zone"=>$time_zone
        );

        $this->db->trans_start();

        $this->basic->update_data("tag_machine_bulk_reply",array("id"=>$campaign_id,"facebook_rx_fb_user_info_id"=>$facebook_rx_fb_user_info_id),$data);

        $this->basic->delete_data("tag_machine_bulk_reply_send",array("campaign_id"=>$campaign_id));
        foreach ($report as $key => $value) 
        {
            $insert_now=$value;
            $insert_now['campaign_id']=$campaign_id;
            unset($insert_now['status']);
            unset($insert_now['replied_at']);
            $this->basic->insert_data("tag_machine_bulk_reply_send",$insert_now);
        }
        
        $this->db->trans_complete();
        if($this->db->trans_status() === false) 
        {
            echo json_encode(array('status'=>'0','message'=>$this->lang->line("something went wrong, please try again.")));
            exit();
        }

        $campaign_link=" <a href='".base_url('commenttagmachine/bulk_comment_reply_campaign_list/0/'.$campaign_id)."'>".$this->lang->line('click here to see report.')."</a>";
        $success_message=$this->lang->line("Campagin has been updated successfully.").$campaign_link;
        echo json_encode(array('status'=>'1','message'=>$success_message));

    }


  

    public function upload_image_video()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') exit();
        $ret=array();
        $output_dir = FCPATH."upload/commenttagmachine";

        $folder_path = FCPATH."upload/commenttagmachine";
        if (!file_exists($folder_path)) {
            mkdir($folder_path, 0777, true);
        }

        if (isset($_FILES["myfile"])) {
            $error =$_FILES["myfile"]["error"];
            $post_fileName =$_FILES["myfile"]["name"];
            $post_fileName_array=explode(".", $post_fileName);
            $ext=array_pop($post_fileName_array);
            $ext=strtolower($ext);
            $filename=implode('.', $post_fileName_array);
            $filename="image_video_".$this->user_id."_".time().substr(uniqid(mt_rand(), true), 0, 6).".".$ext;

            $allow=".png,.jpg,.jpeg,.JPEG,.JPG,.PNG,.gif,.GIF,.flv,.mp4,.wmv,.WMV,.MP4,.FLV";
            $allow=str_replace('.', '', $allow);
            $allow=explode(',', $allow);
            if(!in_array(strtolower($ext), $allow)) 
            {
                echo json_encode("Are you kidding???");
                exit();
            }


            move_uploaded_file($_FILES["myfile"]["tmp_name"], $output_dir.'/'.$filename);
            $ret[]= $filename;
            echo json_encode($filename);
        }
    }

    public function delete_uploaded_file() // deletes the uploaded video to upload another one
    {
        if(!$_POST) exit();
        $output_dir = FCPATH."upload/commenttagmachine/";
        if(isset($_POST["op"]) && $_POST["op"] == "delete" && isset($_POST['name']))
        {
             $fileName =$_POST['name'];
             $fileName=str_replace("..",".",$fileName); //required. if somebody is trying parent folder files 
             $filePath = $output_dir. $fileName;
             if (file_exists($filePath)) 
             {
                @unlink($filePath);
             }
        }
    }



    public function _api_key_check($api_key="")
    {
        $user_id="";
        if($api_key!="")
        {
            $explde_api_key=explode('-',$api_key);
            $user_id="";
            if(array_key_exists(0, $explde_api_key))
            $user_id=$explde_api_key[0];
        }

        if($api_key=="")
        {        
            echo "API Key is required.";    
            exit();
        }

        if(!$this->basic->is_exist("native_api",array("api_key"=>$api_key,"user_id"=>$user_id)))
        {
           echo "API Key does not match with any user.";
           exit();
        }

        if(!$this->basic->is_exist("users",array("id"=>$user_id,"status"=>"1","deleted"=>"0","user_type"=>"Admin")))
        {
            echo "API Key does not match with any authentic user.";
            exit();
        }       

    }

    // every 5 mins or higher
    public function comment_bulk_tag_cronjob($api_key="")
    {
        $this->_api_key_check($api_key);

        $number_of_campaign_to_be_processed = 1; // max number of campaign that can be processed by this cron job

        /****** Get all campaign from database where status=0 means pending ******/
        $where['where'] = array('posting_status'=>"0");
        $join = array('users'=>'tag_machine_bulk_tag.user_id=users.id,left');
        $campaign_info= $this->basic->get_data("tag_machine_bulk_tag",$where,$select=array("tag_machine_bulk_tag.*","users.deleted as user_deleted"),$join,$limit=50, $start=0, $order_by='schedule_time ASC');  
        
        $access_token_database_database = array(); //  [campaign_id][page_auto_id] =>access token
        $facebook_rx_fb_user_info_id_database = array(); // campaign_id => facebook_rx_fb_user_info_id
        $facebook_rx_config_id_database = array(); // facebook_rx_fb_user_info_id => facebook_rx_config_id
        $campaign_id_array=array();  // all selected campaign id array
        $campaign_info_fildered = array(); // valid for process, campign info array

        $valid_campaign_count = 1;
        foreach($campaign_info as $info)
        {
             if($info['user_deleted'] == '1' || $info['user_deleted']=="") continue;

            $campaign_id = $info['id'];
            $time_zone = $info['time_zone'];
            $schedule_time = $info['schedule_time'];     
            $page_id = $info["page_info_table_id"]; 

            if($time_zone) date_default_timezone_set($time_zone);            
            $now_time = date("Y-m-d H:i:s");

            if((strtotime($now_time) < strtotime($schedule_time)) && $time_zone!="") continue; 
            if($valid_campaign_count > $number_of_campaign_to_be_processed) break; 

            $token_info =  $this->basic->get_data('facebook_rx_fb_page_info',array("where"=>array('id'=>$page_id)),array("page_access_token","facebook_rx_fb_user_info_id","id","page_name"));
            foreach ($token_info as $key => $value) 
            {
                $access_token_database_database[$campaign_id][$value["id"]] = $value['page_access_token'];
                $facebook_rx_fb_user_info_id = $value["facebook_rx_fb_user_info_id"];
                $facebook_rx_fb_user_info_id_database[$campaign_id] = $facebook_rx_fb_user_info_id;
            }           
           
            // valid campaign info and campig ids
            $campaign_info_fildered[] = $info;
            $campaign_id_array[] = $info['id']; 
            $valid_campaign_count++;   
        }

        if(count($campaign_id_array)==0) exit();        

        $this->db->where_in("id",$campaign_id_array);
        $this->db->update("tag_machine_bulk_tag",array("posting_status"=>"1"));

        // get config id
        $getdata= $this->basic->get_data("facebook_rx_fb_user_info",array("where_in"=>array("id"=>$facebook_rx_fb_user_info_id_database)),array("id","facebook_rx_config_id"));
        foreach ($getdata as $key => $value) 
        {
            $facebook_rx_config_id_database[$value["id"]] = $value["facebook_rx_config_id"];
        } 

        $this->load->library("fb_rx_login");

        foreach($campaign_info_fildered as $info)
        {
            $campaign_id = $info['id'];                   
            $post_id = $info['post_id'];                   
            $page_id = $info['page_info_table_id']; 
            $post_access_token = isset($access_token_database_database[$campaign_id][$page_id]) ? $access_token_database_database[$campaign_id][$page_id] : "";   
            $uploaded_image_video = $info["uploaded_image_video"]; 
     
            $now_time = date("Y-m-d H:i:s");

            if(!isset($post_access_token) || $post_access_token=="") 
            {
              $this->basic->update_data("tag_machine_bulk_tag",array("id"=>$campaign_id),array("posting_status"=>"2","error_message"=>"Access token not found.","last_updated_at"=>$now_time));
              continue;
            }

            $image=$video=$gif="";
            if($uploaded_image_video!="")
            {
              $ext_exp=explode('.', $uploaded_image_video);
              $ext=array_pop($ext_exp);
              $video_array=array("flv","mp4","wmv");
              if(in_array($ext,$video_array)) 
              $video=FCPATH.'upload/commenttagmachine/'.$uploaded_image_video;  
              else if($ext=='gif') $gif=base_url("upload/commenttagmachine/".$uploaded_image_video);
              else $image=base_url("upload/commenttagmachine/".$uploaded_image_video); 
            }    

            $fb_rx_fb_user_info_id = isset($facebook_rx_fb_user_info_id_database[$campaign_id])?$facebook_rx_fb_user_info_id_database[$campaign_id]:""; // find gb user id for this campaign
            
            if(!isset($fb_rx_fb_user_info_id) || $facebook_rx_fb_user_info_id_database=="") 
            {
              $this->basic->update_data("tag_machine_bulk_tag",array("id"=>$campaign_id),array("posting_status"=>"2","error_message"=>"Facebook accouny not found.","last_updated_at"=>$now_time));
              continue;
            }
            
            $this->fb_rx_login->app_initialize($facebook_rx_config_id_database[$fb_rx_fb_user_info_id]);

            $tag_database=json_decode($info["tag_database"],true);
            $tags="";
            foreach ($tag_database as $key => $value) 
            {
               $tags.="@[".$key."], ";
            }
            $tags=trim($tags);
            $tags=trim($tags,',');
            $tag_content=$info["tag_content"]."

            ".$tags;

            try 
            {
              $response=$this->fb_rx_login->auto_comment($tag_content,$post_id,$post_access_token,$image,$video,$gif);
              $commentid=isset($response['id'])?$response['id']:"";         
              $this->basic->update_data("tag_machine_bulk_tag",array("id"=>$campaign_id),array("posting_status"=>"2","tag_response"=>$commentid,"last_updated_at"=>$now_time));
            } 
            catch (Exception $e) 
            {
              $error_msg = $e->getMessage();
              $this->basic->update_data("tag_machine_bulk_tag",array("id"=>$campaign_id),array("posting_status"=>"2","error_message"=>$error_msg,"last_updated_at"=>$now_time));
            }

            
        }          
   
    }
    // every 2 minute or higher
    public function bulk_comment_reply_cronjob($api_key="")
    {
        $this->_api_key_check($api_key);

        $number_of_message_to_be_sent_in_try=$this->config->item("number_of_message_to_be_sent_in_try"); 
        if($number_of_message_to_be_sent_in_try==0) $number_of_message_to_be_sent_in_try="";
        $update_report_after_time=$this->config->item("update_report_after_time"); 
        if($update_report_after_time=="" || $update_report_after_time==0) $update_report_after_time=10;
        $number_of_campaign_to_be_processed = 1; // max number of campaign that can be processed by this cron job

        /****** Get all campaign from database where status=0 means pending ******/
        $where['or_where'] = array('posting_status'=>"0","is_try_again"=>"1");
        $join = array('users'=>'tag_machine_bulk_reply.user_id=users.id,left');
        $campaign_info= $this->basic->get_data("tag_machine_bulk_reply",$where,$select=array("tag_machine_bulk_reply.*","users.deleted as user_deleted"),$join,$limit=50, $start=0, $order_by='schedule_time ASC');  

        $access_token_database_database = array(); //  [campaign_id][page_auto_id] =>access token
        $facebook_rx_fb_user_info_id_database = array(); // campaign_id => facebook_rx_fb_user_info_id
        $facebook_rx_config_id_database = array(); // facebook_rx_fb_user_info_id => facebook_rx_config_id
        $campaign_id_array=array();  // all selected campaign id array
        $campaign_info_fildered = array(); // valid for process, campign info array

        $valid_campaign_count = 1;
        foreach($campaign_info as $info)
        {
            if($info['user_deleted'] == '1' || $info['user_deleted']=="") continue;

            $campaign_id= $info['id'];
            $time_zone= $info['time_zone'];
            $schedule_time= $info['schedule_time'];    
            $page_info_table_id = $info["page_info_table_id"]; 

            if($time_zone) date_default_timezone_set($time_zone);            
            $now_time = date("Y-m-d H:i:s");

            if((strtotime($now_time) < strtotime($schedule_time)) && $time_zone!="") continue; 
            if($valid_campaign_count > $number_of_campaign_to_be_processed) break; 

            $token_info =  $this->basic->get_data('facebook_rx_fb_page_info',array("where"=>array('id'=>$page_info_table_id)),array("page_access_token","facebook_rx_fb_user_info_id","id","page_name"));
            foreach ($token_info as $key => $value) 
            {
                $access_token_database_database[$campaign_id][$value["id"]] = $value['page_access_token'];
                $facebook_rx_fb_user_info_id = $value["facebook_rx_fb_user_info_id"];
                $facebook_rx_fb_user_info_id_database[$campaign_id] = $facebook_rx_fb_user_info_id;
            }  
            // valid campaign info and campig ids
            $campaign_info_fildered[] = $info;
            $campaign_id_array[] = $info['id']; 
            $valid_campaign_count++;      
        }


        if(count($campaign_id_array)==0) exit();        

        $this->db->where_in("id",$campaign_id_array);
        $this->db->update("tag_machine_bulk_reply",array("posting_status"=>"1","is_try_again"=>"0"));

        // get config id
        $getdata= $this->basic->get_data("facebook_rx_fb_user_info",array("where_in"=>array("id"=>$facebook_rx_fb_user_info_id_database)),array("id","facebook_rx_config_id"));
        foreach ($getdata as $key => $value) 
        {
            $facebook_rx_config_id_database[$value["id"]] = $value["facebook_rx_config_id"];
        } 


        $this->load->library("fb_rx_login");
        foreach($campaign_info_fildered as $info)
        {
            $campaign_id= $info['id']; 
            $user_id = $info["user_id"]; 
            $delay_time = $info["delay_time"];
            $catch_error_count=$info["last_try_error_count"];
            $successfully_sent=$info["successfully_sent"];

            $reply_content = $info["reply_content"];                
            $post_id = $info['post_id'];                   
            $page_id = $info['page_info_table_id']; 

            $post_access_token = isset($access_token_database_database[$campaign_id][$page_id]) ? $access_token_database_database[$campaign_id][$page_id] : "";
            if(!isset($post_access_token) || $post_access_token=="") 
            {
              $this->basic->update_data("tag_machine_bulk_reply",array("id"=>$campaign_id),array("posting_status"=>"2","is_try_again"=>"0","error_message"=>"Access token not found.","last_updated_at"=>date("Y-m-d H:i:s")));
              continue;
            }

            $uploaded_image_video = $info["uploaded_image_video"]; 
            $image=$video=$gif="";
            if($uploaded_image_video!="")
            {
              $ext_exp=explode('.', $uploaded_image_video);
              $ext=array_pop($ext_exp);
              $video_array=array("flv","mp4","wmv");
              if(in_array($ext,$video_array)) 
              $video=FCPATH.'upload/commenttagmachine/'.$uploaded_image_video;  
              else if($ext=='gif') $gif=base_url("upload/commenttagmachine/".$uploaded_image_video);
              else $image=base_url("upload/commenttagmachine/".$uploaded_image_video); 
            }   

            $fb_rx_fb_user_info_id = $facebook_rx_fb_user_info_id_database[$campaign_id]; // find gb user id for this campaign
            $this->fb_rx_login->app_initialize($facebook_rx_config_id_database[$fb_rx_fb_user_info_id]);

            $report = json_decode($info["report"],true); // get json lead list from database and decode it
            $i=0;
            $send_report = $report;
        
            $campaign_lead=$this->basic->get_data("tag_machine_bulk_reply_send",array("where"=>array("campaign_id"=>$campaign_id,"processed"=>"0")),'','',$number_of_message_to_be_sent_in_try);
            foreach($campaign_lead as $key => $value) 
            {             
                $send_table_id = $value['id'];
                $comment_id = $value['comment_id'];
                $commenter_fb_id = $value['commenter_fb_id'];
                $commenter_name = $value['commenter_name'];
                $comment_time = $value['comment_time'];
                $commenter_name_array = explode(' ', $commenter_name);
                $commenter_last_name = array_pop($commenter_name_array);
                $commenter_first_name = implode(' ', $commenter_name_array);
                $commenter_tag_name = "@[".$commenter_fb_id."]";
                $error_msg="";
                $reply_id = "";

                //  generating message
                $reply_content_send = $reply_content;
                $reply_content_send = str_replace('#LEAD_USER_FIRST_NAME#',$commenter_first_name,$reply_content_send);
                $reply_content_send = str_replace('#LEAD_USER_LAST_NAME#',$commenter_last_name,$reply_content_send);
                $reply_content_send = str_replace('#TAG_USER#',$commenter_tag_name,$reply_content_send);
                $reply_content_send = spintax_process($reply_content_send);
                               
                try
                {
                    $response = $this->fb_rx_login->auto_comment($reply_content_send,$comment_id,$post_access_token,$image='',$video="",$gif='');
                    if(isset($response['id']))
                    {
                       $reply_id = $response['id']; 
                       $successfully_sent++; 
                    }
                    else 
                    {
                       $catch_error_count++;
                    } 
                    if($delay_time==0)
                    sleep(rand(2,10));
                    else sleep($delay_time); 
                }

                catch(Exception $e) 
                {
                  $error_msg = $e->getMessage();
                  $catch_error_count++;
                }

                // generating new report
                $now_sent_time=date("Y-m-d H:i:s");
                $reply_status="";
                if($reply_id!="") $reply_status=$reply_id;
                else $reply_id=$error_msg;

                $send_report[$comment_id] = array
                ( 
                    "commenter_name"=>$commenter_name,
                    "commenter_fb_id"=>$commenter_fb_id,
                    "comment_id"=> $comment_id,
                    "comment_time"=> $now_sent_time,
                    "status" => $reply_status,
                    "replied_at" => $now_sent_time
                );

                $i++;  
                // after 10 send update report in database
                if($i%$update_report_after_time==0)
                {
                    $send_report_json= json_encode($send_report);
                    $this->basic->update_data("tag_machine_bulk_reply",array("id"=>$campaign_id),array("report"=>$send_report_json,'successfully_sent'=>$successfully_sent,"error_message"=>$error_msg,"last_try_error_count"=>$catch_error_count,"last_updated_at"=>$now_sent_time));
                }

                // updating a lead, marked as processed
                $this->basic->update_data("tag_machine_bulk_reply_send",array("id"=>$send_table_id),array('processed'=>'1',"sent_time"=>$now_sent_time,"response"=>$reply_status));
            
            } 

            // one campaign completed, now update database finally
            $send_report_json= json_encode($send_report);
            if((count($campaign_lead)<$number_of_message_to_be_sent_in_try) || $number_of_message_to_be_sent_in_try=="")
            {
                $complete_update=array("report"=>$send_report_json,"posting_status"=>'2','successfully_sent'=>$successfully_sent,'last_updated_at'=>date("Y-m-d H:i:s"),"is_try_again"=>"0","last_try_error_count"=>$catch_error_count);
                if(isset($error_msg))
                $complete_update["error_message"]=$error_msg;
                $this->basic->update_data("tag_machine_bulk_reply",array("id"=>$campaign_id),$complete_update);
            }
            else // suppose update_report_after_time=20 but there are 19 message to sent, need to update report in that case
            {
                $this->basic->update_data("tag_machine_bulk_reply",array("id"=>$campaign_id),array("report"=>$send_report_json,'successfully_sent'=>$successfully_sent,"is_try_again"=>"1",'last_updated_at'=>date("Y-m-d H:i:s")));
            }
        }          
   
    }
    

    public function cron_job()
    {
        if($this->session->userdata('user_type') != 'Admin')
        redirect('home/login_page', 'location');
        
        $data['body'] = "cron_job";
        $data['page_title'] = 'cron job';
        $api_data=$this->basic->get_data("native_api",array("where"=>array("user_id"=>$this->session->userdata("user_id"))));
        $data["api_key"]="";
        if(count($api_data)>0) $data["api_key"]=$api_data[0]["api_key"];
        if($this->is_demo=='1') $data["api_key"]='xxxxxxxxxxxxxxxxxxxxxxxxxx';
        $this->_viewcontroller($data);
    }







    public function activate()
    {
        if(!$_POST) exit();
   
        $is_free_addon=true; 
        $addon_controller_name=ucfirst($this->router->fetch_class()); // here addon_controller_name name is Comment [origianl file is Comment.php, put except .php]
        $purchase_code=$this->input->post('purchase_code');
        if(!$is_free_addon)
        {
            $this->addon_credential_check($purchase_code,strtolower($addon_controller_name)); // retuns json status,message if error
        }  
        //this addon system support 2-level sidebar entry, to make sidebar entry you must provide 2D array like below
        $sidebar=array
        (           
            0 =>array // first parent menu , you can add other parent menu in 1,2,3... index
            (
                'name' => $this->lang->line('Comment Tag Machine'), // lang->line is used for multilingual support, you need to add the name's value [example:comment] in your add-on language file. 
                'icon' => 'fa fa-tag', // font awesome
                'url' => '#', // this will take to http://thisappdomain.com/comment/index, you can also use external link (http://xyz.com) but then you need to set is_external=1
                'is_external' => '0', // 0 means internal link and 1 means external
                'child_info' => array // no need to set this index if no child.child_info['have_child']=1 means it has child menus, 0 menas have no child menus. If it has child then child_info['child'] must to declare as array with child info
                    (
                        'have_child'=>'1', // parent has child menus, 0 means no child
                        'child'=>array // if status = 1 then you must add child array, other wise not need to set this index
                            (
                                0 => array // first child menu, you can add other child in 1,2,3...index
                                (
                                    'name'=>'Enable Post', // similar as arent name
                                    'icon'=>'fa fa-check-square', // similar as parent icon
                                    'url' => 'commenttagmachine/page_list', // similar as parent url
                                    'is_external' => '0' // similar as parent is_external
                                ),
                                1 => array // first child menu, you can add other child in 1,2,3...index
                                (
                                    'name'=>'Create Campaign', // similar as arent name
                                    'icon'=>'fa fa-plus', // similar as parent icon
                                    'url' => 'commenttagmachine/post_list', // similar as parent url
                                    'is_external' => '0' // similar as parent is_external
                                ),
                                2 => array // first child menu, you can add other child in 1,2,3...index
                                (
                                    'name'=>'Comment & Bulk Tag Report', // similar as arent name
                                    'icon'=>'fa fa-tags', // similar as parent icon
                                    'url' => 'commenttagmachine/bulk_tag_campaign_list', // similar as parent url
                                    'is_external' => '0' // similar as parent is_external
                                ),
                                3 => array // first child menu, you can add other child in 1,2,3...index
                                (
                                    'name'=>'Bulk Comment Reply Report', // similar as arent name
                                    'icon'=>'fa fa-mail-reply-all', // similar as parent icon
                                    'url' => 'commenttagmachine/bulk_comment_reply_campaign_list', // similar as parent url
                                    'is_external' => '0' // similar as parent is_external
                                ),
                                4 => array // first child menu, you can add other child in 1,2,3...index
                                (
                                    'name'=>'Cron Job', // similar as arent name
                                    'icon'=>'fa fa-clock-o', // similar as parent icon
                                    'url' => 'commenttagmachine/cron_job', // similar as parent url
                                    'is_external' => '0' // similar as parent is_external
                                )
                            )
                    ),  
                'only_admin' => '0' , // 1 means only admin can access [only admin & only member can not be set 1 at the same time]  
                'only_member' => '0' // 1 means only member usercan access [only admin & only member can not be set 1 at the same time] 
            )            
        );  

        // mysql raw query needed to run, it's an array, put each query in a seperate index, create table query must should IF NOT EXISTS
        $sql=array
        (
          0 =>"CREATE TABLE IF NOT EXISTS `tag_machine_bulk_reply` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `campaign_name` varchar(255) NOT NULL,
              `reply_content` text NOT NULL,
              `uploaded_image_video` varchar(255) NOT NULL,
              `reply_multiple` enum('0','1') DEFAULT '0',
              `report` longtext NOT NULL,
              `campaign_created` datetime NOT NULL,
              `posting_status` enum('0','1','2') NOT NULL DEFAULT '0',
              `delay_time` int(11) NOT NULL,
              `is_try_again` enum('0','1') NOT NULL DEFAULT '1',
              `total_reply` int(11) NOT NULL,
              `schedule_type` enum('now','later') NOT NULL DEFAULT 'now',
              `schedule_time` datetime NOT NULL,
              `time_zone` varchar(255) NOT NULL,
              `successfully_sent` int(11) NOT NULL,
              `last_try_error_count` int(11) NOT NULL,
              `last_updated_at` datetime NOT NULL,
              `tag_machine_enabled_post_list_id` int(11) NOT NULL,
              `facebook_rx_fb_user_info_id` int(11) NOT NULL,
              `user_id` int(11) NOT NULL,
              `page_info_table_id` int(11) NOT NULL,
              `page_id` varchar(255) NOT NULL COMMENT 'facebook page id',
              `page_name` varchar(255) DEFAULT NULL,
              `page_profile` text NOT NULL,
              `post_id` varchar(200) NOT NULL,
              `post_created_at` varchar(255) DEFAULT NULL,
              `post_description` text,
              `error_message` tinytext NOT NULL,
              PRIMARY KEY (`id`),
              KEY `posting_status` (`posting_status`,`is_try_again`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;",

            1=>"CREATE TABLE IF NOT EXISTS `tag_machine_bulk_reply_send` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `campaign_id` int(11) NOT NULL COMMENT 'tag_machine_bulk_reply.id',
                `comment_id` varchar(255) NOT NULL,
                `commenter_fb_id` varchar(255) NOT NULL,
                `commenter_name` varchar(255) NOT NULL,
                `comment_time` datetime NOT NULL,
                `sent_time` datetime NOT NULL,
                `response` varchar(255) NOT NULL,
                `processed` enum('0','1') NOT NULL DEFAULT '0',
                PRIMARY KEY (`id`),
                KEY `campaign_id` (`campaign_id`,`processed`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;",

            2=>"CREATE TABLE IF NOT EXISTS `tag_machine_bulk_tag` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `campaign_name` varchar(255) NOT NULL,
              `tag_database` longtext NOT NULL,
              `tag_exclude` longtext NOT NULL,
              `tag_content` text NOT NULL,
              `uploaded_image_video` varchar(255) NOT NULL,
              `error_message` text NOT NULL,
              `tag_response` text NOT NULL,
              `schedule_type` enum('now','later') NOT NULL DEFAULT 'now',
              `schedule_time` datetime NOT NULL,
              `time_zone` varchar(255) NOT NULL,
              `campaign_created` datetime NOT NULL,
              `posting_status` enum('0','1','2') NOT NULL DEFAULT '0',
              `last_updated_at` datetime NOT NULL,
              `tag_machine_enabled_post_list_id` int(11) NOT NULL,
              `facebook_rx_fb_user_info_id` int(11) NOT NULL,
              `user_id` int(11) NOT NULL,
              `page_info_table_id` int(11) NOT NULL,
              `page_id` varchar(255) NOT NULL COMMENT 'facebook page id',
              `page_name` varchar(255) DEFAULT NULL,
              `page_profile` text NOT NULL,
              `post_id` varchar(200) NOT NULL,
              `post_created_at` varchar(255) DEFAULT NULL,
              `post_description` text,
              `commenter_count` int(11) NOT NULL,
              PRIMARY KEY (`id`),
              KEY `posting_status` (`posting_status`),
              KEY `facebook_rx_fb_user_info_id` (`facebook_rx_fb_user_info_id`,`page_info_table_id`,`post_id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;",

            3=>"CREATE TABLE IF NOT EXISTS `tag_machine_commenter_info` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `tag_machine_enabled_post_list_id` int(11) NOT NULL,
              `facebook_rx_fb_user_info_id` int(11) NOT NULL,
              `user_id` int(11) NOT NULL,
              `page_info_table_id` int(11) NOT NULL,
              `page_id` varchar(255) NOT NULL COMMENT 'facebook page id',
              `page_name` varchar(255) DEFAULT NULL,
              `post_id` varchar(200) NOT NULL,
              `last_comment_id` varchar(255) NOT NULL,
              `last_comment_time` varchar(255) NOT NULL,
              `commenter_fb_id` varchar(255) NOT NULL,
              `commenter_name` varchar(255) NOT NULL,
              `subscribed` enum('0','1') NOT NULL DEFAULT '1',
              PRIMARY KEY (`id`),
              UNIQUE KEY `tag_machine_enabled_post_list_id` (`tag_machine_enabled_post_list_id`,`commenter_fb_id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;",

            4=>"CREATE TABLE IF NOT EXISTS `tag_machine_comment_info` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `tag_machine_enabled_post_list_id` int(11) NOT NULL,
              `facebook_rx_fb_user_info_id` int(11) NOT NULL,
              `user_id` int(11) NOT NULL,
              `page_info_table_id` int(11) NOT NULL,
              `page_id` varchar(255) NOT NULL COMMENT 'facebook page id',
              `page_name` varchar(255) DEFAULT NULL,
              `post_id` varchar(200) NOT NULL,
              `comment_id` varchar(255) NOT NULL,
              `comment_text` text NOT NULL,
              `commenter_fb_id` varchar(255) NOT NULL,
              `commenter_name` varchar(255) NOT NULL,
              `comment_time` varchar(255) NOT NULL,
              `subscribed` enum('0','1') NOT NULL DEFAULT '1',
              PRIMARY KEY (`id`),
              UNIQUE KEY `tag_machine_enabled_post_list_id` (`tag_machine_enabled_post_list_id`,`comment_id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;",

            5=>"CREATE TABLE IF NOT EXISTS `tag_machine_enabled_post_list` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `facebook_rx_fb_user_info_id` int(11) NOT NULL,
              `user_id` int(11) NOT NULL,
              `page_info_table_id` int(11) NOT NULL,
              `page_id` varchar(255) NOT NULL COMMENT 'facebook page id',
              `page_name` varchar(255) DEFAULT NULL,
              `page_profile` text NOT NULL,
              `post_id` varchar(200) NOT NULL,
              `post_created_at` varchar(255) DEFAULT NULL,
              `post_description` text,
              `last_updated_at` datetime NOT NULL,
              `commenter_count` int(11) NOT NULL,
              `comment_count` int(11) NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `facebook_rx_fb_user_info_id` (`facebook_rx_fb_user_info_id`,`page_info_table_id`,`post_id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;",
            // extra module, this module aslo deleted manaually
            6=>"INSERT INTO `modules` (`id`, `module_name`, `add_ons_id`, `deleted`) VALUES ('202', 'Comment Tag Machine - Bulk Comment Reply Campaign', '0', '0');",
            // this mwnu is for admin only
            7=>"UPDATE `menu_child_1` SET `only_admin` = '1' WHERE `menu_child_1`.`url` = 'commenttagmachine/cron_job';"
         
        ); 

        //send blank array if you does not need sidebar entry,send a blank array if your addon does not need any sql to run
        $this->register_addon($addon_controller_name,$sidebar,$sql,$purchase_code,"Comment Tag Machine - Comment & Bulk Tag Campaign"); 
    }


    public function deactivate()
    {        
        $addon_controller_name=ucfirst($this->router->fetch_class()); // here addon_controller_name name is Comment [origianl file is Comment.php, put except .php]
        $this->db->query("DELETE FROM `modules` WHERE `modules`.`id` = 202");
        // only deletes add_ons,modules and menu, menu_child1 table entires and put install.txt back, it does not delete any files or custom sql
        $this->unregister_addon($addon_controller_name);         
    }

    public function delete()
    {        
        $addon_controller_name=ucfirst($this->router->fetch_class()); // here addon_controller_name name is Comment [origianl file is Comment.php, put except .php]

         // mysql raw query needed to run, it's an array, put each query in a seperate index, drop table/column query should have IF EXISTS
        $sql=array
        (
          0=>"DROP TABLE IF EXISTS `tag_machine_bulk_reply`;",
          1=>"DROP TABLE IF EXISTS `tag_machine_bulk_reply_send`;",
          2=>"DROP TABLE IF EXISTS `tag_machine_bulk_tag`;",
          3=>"DROP TABLE IF EXISTS `tag_machine_commenter_info`;",
          4=>"DROP TABLE IF EXISTS `tag_machine_comment_info`;",
          5=>"DROP TABLE IF EXISTS `tag_machine_enabled_post_list`;",
          6=>"DELETE FROM `modules` WHERE `modules`.`id` = 202"
        ); 
        
        // deletes add_ons,modules and menu, menu_child1 table ,custom sql as well as module folder, no need to send sql or send blank array if you does not need any sql to run on delete
        $this->delete_addon($addon_controller_name,$sql);         
    }





}