<?php  
include(APPPATH."libraries/Facebook/autoload.php");

class Commenttagmachine_class
{				
	public $database_id=""; 
	public $app_id="";
	public $app_secret="";		
	public $user_access_token="";
	public $fb;


	function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->database();
		$this->CI->load->helper('my_helper');
		$this->CI->load->library('session');

		$this->CI->load->model('basic');
		$this->database_id=$this->CI->session->userdata("fb_rx_login_database_id"); 

		if($this->CI->session->userdata("user_type")=="Admin" && ($this->database_id=="" || $this->database_id==0)) 
		{
			echo "<h3 align='center' style='font-family:arial;line-height:35px;margin:20px;padding:20px;border:1px solid #ccc;'>Hello Admin : No facebbok app configuration found. You have to  <a href='".base_url("facebook_rx_config/index")."'> add facebook app & login with facebook</a>. If you just added your first app and redirected here again then <a href='".base_url("home/logout")."'> logout</a>, login again and <a href='".base_url("facebook_rx_config/index")."'> go to this link</a> to login with facebook for your just added app.   </h3>";
			exit();
		}

		if($this->CI->session->userdata("user_type")=="Member" && ($this->database_id=="" || $this->database_id==0) && $this->CI->config->item("backup_mode")==1) 
		{
			echo "<h3 align='center' style='font-family:arial;line-height:35px;margin:20px;padding:20px;border:1px solid #ccc;'>Hello User : No facebbok app configuration found. You have to  <a href='".base_url("facebook_rx_config/index")."'> add facebook app & login with facebook</a>. If you just added your first app and redirected here again then <a href='".base_url("home/logout")."'> logout</a>, login again and <a href='".base_url("facebook_rx_config/index")."'> go to this link</a> to login with facebook for your just added app.   </h3>";
			exit();
		}

		if($this->database_id != '')
		{
			$facebook_config=$this->CI->basic->get_data("facebook_rx_config",array("where"=>array("id"=>$this->database_id)));
			if(isset($facebook_config[0]))
			{			

				if(isset($facebook_config[0]['developer_access']) && $facebook_config[0]['developer_access'] == '1')
				{
					$encrypt_method = "AES-256-CBC";
					$secret_key = 't8Mk8fsJMnFw69FGG5';
					$secret_iv = '9fljzKxZmMmoT358yZ';
					$key = hash('sha256', $secret_key);
					$iv = substr(hash('sha256', $secret_iv), 0, 16);
					$this->app_id = openssl_decrypt(base64_decode($facebook_config[0]["api_id"]), $encrypt_method, $key, 0, $iv);
					$this->app_secret = openssl_decrypt(base64_decode($facebook_config[0]["api_secret"]), $encrypt_method, $key, 0, $iv);
					$this->user_access_token=$facebook_config[0]["user_access_token"];
				}	
				else
				{					
					$this->app_id=$facebook_config[0]["api_id"];
					$this->app_secret=$facebook_config[0]["api_secret"];
					$this->user_access_token=$facebook_config[0]["user_access_token"];
				}

				if (session_status() == PHP_SESSION_NONE) 
				{
				    session_start();
				}
		
				$this->fb = new Facebook\Facebook([
					'app_id' => $this->app_id, 
					'app_secret' => $this->app_secret,
					'default_graph_version' => 'v2.10',
					'fileUpload'	=>TRUE
					]);
			}
		}


	}
	
	public function get_all_comment_of_post_pagination($post_ids,$post_access_token)
	{ 
		$url="{$post_ids}/comments?order=reverse_chronological&summary=1&limit=400&filter=toplevel";

		$comment_info=array();
		$commenter_info=array();

		$i=0;

		do
		{
			$response = $this->fb->get($url,$post_access_token);
			$data =  $response->getGraphEdge()->asArray();
			$paging_data= $response->getGraphEdge()->getMetaData(); 

			foreach($data as $info){

				$time=  isset($info['created_time'])?(array)$info['created_time']:"";

				$comment_info[$i]['created_time']=isset($time['date'])?$time['date']:"";
				$comment_info[$i]['commenter_name']=isset($info['from']['name'])? $info['from']['name']:"";
				$comment_info[$i]['commenter_id']=isset($info['from']['id'])?$info['from']['id']:"";
				$comment_info[$i]['message']=isset($info['message'])?$info['message']:"";
				$comment_info[$i]['comment_id']=isset($info['id'])?$info['id']:"";

				/* Store Commenter info as unique */

				if(!isset($commenter_info[$comment_info[$i]['commenter_id']])){
					$commenter_info[$comment_info[$i]['commenter_id']]['name']=$comment_info[$i]['commenter_name'];
					$commenter_info[$comment_info[$i]['commenter_id']]['last_comment']=$comment_info[$i]['message'];
					$commenter_info[$comment_info[$i]['commenter_id']]['last_comment_id']=$comment_info[$i]['comment_id'];
					$commenter_info[$comment_info[$i]['commenter_id']]['last_comment_time']=$comment_info[$i]['created_time'];
				}

				$i++;
			}

			$next= isset($paging_data['paging']['cursors']['after'])?$paging_data['paging']['cursors']['after']:"";

			if($next!="")
				$url="{$post_ids}/comments?order=reverse_chronological&after={$next}&limit=400&filter=toplevel";
			else
				$url="";

		}
		while($url!='');

		$all_info=array();

		$all_info['comment_info']= $comment_info;
		$all_info['commenter_info']= $commenter_info;

		return $all_info;
	}

	
	
	public function app_initialize($fb_rx_login_database_id){
	    
	    $this->database_id=$fb_rx_login_database_id;
	    $facebook_config=$this->CI->basic->get_data("facebook_rx_config",array("where"=>array("id"=>$this->database_id)));
		if(isset($facebook_config[0]))
		{			
			$this->app_id=$facebook_config[0]["api_id"];
			$this->app_secret=$facebook_config[0]["api_secret"];
			$this->user_access_token=$facebook_config[0]["user_access_token"];
			if (session_status() == PHP_SESSION_NONE) 
			{
			    session_start();
			}
	
			$this->fb = new Facebook\Facebook([
				'app_id' => $this->app_id, 
				'app_secret' => $this->app_secret,
				'default_graph_version' => 'v2.10',
				'fileUpload'	=>TRUE
				]);
		}
		
	    
	}


	public function app_id_secret_check()
	{
		if($this->app_id == '' || $this->app_secret == '') return 'not_configured';
	}


	public function facebook_api_call($url)
	{
		$headers = array("Content-type: application/json");
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);  
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  
		curl_setopt($ch, CURLOPT_COOKIEJAR,'cookie.txt');  
		curl_setopt($ch, CURLOPT_COOKIEFILE,'cookie.txt');  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3"); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
		$st=curl_exec($ch); 
		return  $results=json_decode($st,TRUE);	 
	}


	public function run_curl_for_fb($url)
	{
		$headers = array("Content-type: application/json"); 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);  
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  
		curl_setopt($ch, CURLOPT_COOKIEJAR,'cookie.txt');  
		curl_setopt($ch, CURLOPT_COOKIEFILE,'cookie.txt');  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3"); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
		$results=curl_exec($ch); 	   
		return  $results;   
	}


	


}


