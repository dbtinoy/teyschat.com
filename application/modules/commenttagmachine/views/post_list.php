<?php 
$this->load->view('admin/theme/message'); 
$this->load->view("include/upload_js"); 
?>

<!-- Main content -->
<div class="well well_border_left">
    <h4 class="text-center blue"> <i class="fa fa-bars"></i> <?php echo $this->lang->line("Post List");?></h4>
</div>
<!-- <section class="content-header">
	<h1 class = 'text-info'><i class="fa fa-list"></i> <?php echo $this->lang->line("Post List");?></h1>
</section> -->
<section class="content">
	<div class="row" >
		<div class="col-xs-12">
			<div class="grid_container" style="width:100%; min-height:840px;">
				<table
				id="tt"
				class="easyui-datagrid"
				url="<?php echo base_url()."commenttagmachine/post_list_data"; ?>"
				pagination="true"
				rownumbers="true"
				toolbar="#tb"
				pageSize="10"
				pageList="[5,10,15,20,50,100]"
				fit= "true"
				fitColumns= "true"
				nowrap= "true"
				view= "detailview"
				idField="id"
				>
					<thead>
						<tr>
							<th field="page_profile" formatter="page_profile_image" align="center"><?php echo $this->lang->line("picture"); ?></th>
							<th field="page_name" sortable="true" formatter="page_name_link"><?php echo $this->lang->line("page name"); ?></th>
							<?php if($this->session->userdata('user_type') == 'Admin'|| in_array(201,$this->module_access)) { ?>
							<th field="comment_bulk_tag" align="center"><?php echo $this->lang->line("Comment & Bulk Tag"); ?></th>
							<?php } ?>
							<?php if($this->session->userdata('user_type') == 'Admin'|| in_array(202,$this->module_access)) { ?>
							<th field="bulk_comment_reply" align="center"><?php echo $this->lang->line("Bulk Comment Reply"); ?></th>
							<?php } ?>							
							<th field="rescan" align="center"><?php echo $this->lang->line("Re-scan"); ?></th>
							<th field="post_id" sortable="true" formatter="post_id_link"><?php echo $this->lang->line("post ID"); ?></th>
							<th field="comment_count" align="center" formatter="comment_list" sortable="true"> <?php echo $this->lang->line("Comments"); ?></th>
							<th field="commenter_count" align="center" formatter="commenter_list"  sortable="true" ><?php echo $this->lang->line("Commenters"); ?></th>
							<th field="last_updated_at" align="center"  sortable="true" ><?php echo $this->lang->line("Last Scanned"); ?></th>
							<th field="post_created_at" align="center"  sortable="true" ><?php echo $this->lang->line("Post Created"); ?></th>
						</tr>
					</thead>
				</table>
			</div>

			<div id="tb" style="padding:3px">

				<?php
					$search_page_id  = $this->session->userdata('comment_tag_machine_post_list_page_id');
			        $search_post_id  = $this->session->userdata('comment_tag_machine_post_list_post_id');
			        $post_created_from = $this->session->userdata('comment_tag_machine_post_list_post_created_from');
			        $post_created_to = $this->session->userdata('comment_tag_machine_post_list_post_created_to');
				?>
		
				<form class="form-inline" style="margin-top:20px">

					<div class="form-group">
						<input id="search_post_id" name="search_post_id" value="<?php echo $search_post_id;?>" class="form-control" size="20" placeholder="<?php echo $this->lang->line("Post ID") ?>">
					</div>

					<div class="form-group">
						<select name="search_page_id" id="search_page_id"  class="form-control">
							<option value=""><?php echo $this->lang->line("all page") ?></option>
							<?php
								foreach ($page_info as $key => $value)
								{
									if($value['id'] == $search_page_id)
									echo "<option selected value='".$value['id']."'>".$value['page_name']."</option>";
									else echo "<option value='".$value['id']."'>".$value['page_name']."</option>";
								}
							?>
						</select>
					</div>

			
					<div class="form-group">
						<input id="post_created_from" value="<?php echo $post_created_from;?>" name="post_created_from" class="form-control datepicker2" size="20" placeholder="<?php echo $this->lang->line("Post Created From") ?>">
					</div>

					<div class="form-group">
						<input id="post_created_to" value="<?php echo $post_created_to;?>" name="post_created_to" class="form-control  datepicker2" size="20" placeholder="<?php echo $this->lang->line("Post Created To") ?>">
					</div>

					<button class='btn btn-info'  onclick="doSearch(event)"><?php echo $this->lang->line("search");?></button>
			</div>

				</form>
		</div>
	</div>
</section>



<?php
    $item_per_range=$this->config->item('item_per_range');
    if($item_per_range=='') $item_per_range=50;

	$somethingwentwrong = $this->lang->line("something went wrong.");
	$pleasewait = $this->lang->line("please wait").'...';
	$startcommenternames = $this->lang->line("Start typing commenter names you want to excude from tag list");
	$list_of_commenters = $this->lang->line("List of commenters which this campaign will tag");
	$campaign_name_is_required=$this->lang->line("Campaign name is required.");
	$tag_content_is_required=$this->lang->line("Tag content is required.");
	$you_have_not_selected_commenters=$this->lang->line("You have not selected commenters.");
	$no_subscribed_commenter_found=$this->lang->line("No subscribed commenter found.");
	$reply_content_is_required=$this->lang->line("Reply content is required.");
	$pleaseselectscheduletimetimezone = $this->lang->line("Please select schedule time/time zone.");
 ?>
<script>

	var base_url="<?php echo site_url(); ?>";
	var somethingwentwrong="<?php echo $somethingwentwrong;?>";
	var pleasewait="<?php echo $pleasewait;?>";
	var startcommenternames="<?php echo $startcommenternames;?>";
	var item_per_range="<?php echo $item_per_range;?>";
	var list_of_commenters="<?php echo $list_of_commenters;?>";
	var campaign_name_is_required="<?php echo $campaign_name_is_required;?>";
	var tag_content_is_required="<?php echo $tag_content_is_required;?>";
	var you_have_not_selected_commenters="<?php echo $you_have_not_selected_commenters;?>";
	var no_subscribed_commenter_found="<?php echo $no_subscribed_commenter_found;?>";
	var reply_content_is_required="<?php echo $reply_content_is_required;?>";

	function page_name_link(value,row,index)
	{
		var page_url = "<a href='https://facebook.com/"+row.page_id+"' target='_blank'>"+row.page_name+"</a>";
		return page_url;
	} 

	function post_id_link(value,row,index)
	{
		var post_url = "<a href='https://facebook.com/"+row.post_id+"' target='_blank'>"+row.post_id+"</a>";
		return post_url;
	} 

	function page_profile_image(value,row,index)
	{
		if(typeof(row.page_profile)==='undefined') return false;
		var img_link = "<img style='width:50px;height:50px' src='"+row.page_profile+"'>";
		return img_link;
	} 

	function comment_list(value,row,index)
	{
		return "<a class='label label-info show_comment_list_modal' data-id='"+row.id+"'><i class='fa fa-comment'></i> "+row.comment_count+"</a>";
	} 

	function commenter_list(value,row,index)
	{
		return "<a class='label label-primary show_commenter_list_modal' data-id='"+row.id+"'><i class='fa fa-user'></i> "+row.commenter_count+"</a>";
	} 

    function doSearch(event)
	{
		event.preventDefault();
		$j('#tt').datagrid('load',{
			search_page_id   	:     $j('#search_page_id').val(),
			search_post_id   	:     $j('#search_post_id').val(),
			post_created_from  	:     $j('#post_created_from').val(),
			post_created_to  	:     $j('#post_created_to').val(),
			is_searched			:     1
		});

	}
</script>

<script>

$j("document").ready(function(){

	$('[data-toggle="popover"]').popover(); 
	$('[data-toggle="popover"]').on('click', function(e) {e.preventDefault(); return true;});

	$(".schedule_block_item").hide();
	$(".schedule_block_item2").hide();

    $j('.datepicker').datetimepicker({
   	theme:'light',
   	format:'Y-m-d H:i:s',
   	formatDate:'Y-m-d H:i:s'
  	});

  	$j('.datepicker2').datetimepicker({
    theme:'light',
    format:'Y-m-d',
    formatDate:'Y-m-d',
    timepicker:false
    });

  	$(document.body).on('change','input[name=schedule_type]',function(){    
    	if($("input[name=schedule_type]:checked").val()=="later")
    	$(".schedule_block_item").show();
    	else 
    	{
    		$("#schedule_time").val("");
    		$("#time_zone").val("");
    		$(".schedule_block_item").hide();
    	}
    }); 

    $(document.body).on('change','input[name=schedule_type2]',function(){    
    	if($("input[name=schedule_type2]:checked").val()=="later")
    	$(".schedule_block_item2").show();
    	else 
    	{
    		$("#schedule_time2").val("");
    		$("#time_zone2").val("");
    		$(".schedule_block_item2").hide();
    	}
    });
 	

  	$(document.body).on('click','.show_comment_list_modal',function(){
  		var loading = '<br/><img src="'+base_url+'assets/pre-loader/Fading squares2.gif" class="center-block"><br/>';
		$("#comment_list_modal").modal();
	    $("#comment_list_body").html(loading);

		var id = $(this).attr("data-id");

		$.ajax({
            type:'POST' ,
            url:"<?php echo site_url();?>commenttagmachine/post_comment_list",
            data:{id:id},
            success:function(response){
            	$("#comment_list_body").html(response);
            }
        });
	});

	$(document.body).on('click','.show_commenter_list_modal',function(){
  		var loading = '<br/><img src="'+base_url+'assets/pre-loader/Fading squares2.gif" class="center-block"><br/>';
		$("#commenter_list_modal").modal();
	    $("#commenter_list_body").html(loading);

		var id = $(this).attr("data-id");

		$.ajax({
            type:'POST' ,
            url:"<?php echo site_url();?>commenttagmachine/post_commenter_list",
            data:{id:id},
            success:function(response){
            	$("#commenter_list_body").html(response);
            }
        });
	});

	$(document.body).on('click','.rescan_comments',function(){
  		
		var page_id = $(this).attr("page-id");
		var post_id = $(this).attr("post-id");
		var enable_id = $(this).attr("enable-id");
		var btn_id="rescan_"+page_id+"_"+post_id;
		$("#"+btn_id).html(pleasewait).addClass('disabled');

		$.ajax({
            type:'POST' ,
            url:"<?php echo site_url();?>commenttagmachine/rescan_commenter_info",
            data:{page_id:page_id,post_id:post_id,enable_id:enable_id},
            dataType:'JSON',
            success:function(response)
            {
            	alert(response.message);
            	$j('#tt').datagrid('reload');
            }
        });
	});

	$(document.body).on('click','.commenter_subscribe_unsubscribe',function(){
	    $(this).html(pleasewait).addClass('disabled');
	    var subscribe_unsubscribe_status = $(this).attr('id');
	    $.ajax({
	      type:'POST',
	      url:"<?php echo site_url();?>commenttagmachine/subscribe_unsubscribe_status_change",
	      data:{subscribe_unsubscribe_status:subscribe_unsubscribe_status},
	      success:function(response)
	      {
	         $("#"+subscribe_unsubscribe_status).parent().html(response); 
	      }
    	});
    });

    $(document.body).on('click','.create_bulk_tag_campaign',function(){  		
		$("#comment_bulk_tag_campaign").modal();
		var post_val = $(this).attr("id");
		var exploded=[];
		exploded=post_val.split('-');
		var tag_machine_enabled_post_list_id=exploded[1];
		var tag_campaign_tag_machine_commenter_count=exploded[2];
		$("#tag_campaign_tag_machine_enabled_post_list_id").val(tag_machine_enabled_post_list_id);
		$("#tag_campaign_tag_machine_commenter_count").val(tag_campaign_tag_machine_commenter_count);

	  	// $('.include_autocomplete').tokenize({
	   //      datas: base_url+"commenttagmachine/commenter_autocomplete/"+tag_machine_enabled_post_list_id,
	   //      placeholder: list_of_commenters,
	   //      dropdownMaxItems: 20,
	   //      tokensMaxItems: item_per_range
	   //  });

	    $('.exclude_autocomplete').tokenize({
	        datas: base_url+"commenttagmachine/commenter_autocomplete/"+tag_machine_enabled_post_list_id,
	        placeholder: startcommenternames,
	        dropdownMaxItems: 20,
	        tokensMaxItems: item_per_range
	    });

	    $.ajax({
            type:'POST' ,
            url:"<?php echo site_url();?>commenttagmachine/commenter_range_option",
            data:{tag_machine_enabled_post_list_id:tag_machine_enabled_post_list_id},
            success:function(response){
            	$("#commenter_range").html(response);
            }
        });
        $("#loading_div").hide();

	});


	$("#image_video_upload").uploadFile({
        url:base_url+"commenttagmachine/upload_image_video",
        fileName:"myfile",
        maxFileSize:100*1024*1024,
        showPreview:false,
        returnType: "json",
        dragDrop: true,
        showDelete: true,
        multiple:false,
        maxFileCount:1, 
        acceptFiles:".png,.jpg,.jpeg,.JPEG,.JPG,.PNG,.gif,.GIF,.flv,.mp4,.wmv,.WMV,.MP4,.FLV",
        deleteCallback: function (data, pd) {
            var delete_url="<?php echo site_url('commenttagmachine/delete_uploaded_file');?>";
            $.post(delete_url, {op: "delete",name: data},
                function (resp,textStatus, jqXHR) {
                	$("#uploaded_image_video").val('');                      
                });
           
         },
         onSuccess:function(files,data,xhr,pd)
           {
               // var data_modified = base_url+"upload/commenttagmachine/"+data;
               $("#uploaded_image_video").val(data);		
           }
    });

    $("#image_video_upload2").uploadFile({
        url:base_url+"commenttagmachine/upload_image_video",
        fileName:"myfile",
        maxFileSize:100*1024*1024,
        showPreview:false,
        returnType: "json",
        dragDrop: true,
        showDelete: true,
        multiple:false,
        maxFileCount:1, 
        acceptFiles:".png,.jpg,.jpeg,.JPEG,.JPG,.PNG,.gif,.GIF,.flv,.mp4,.wmv,.WMV,.MP4,.FLV",
        deleteCallback: function (data, pd) {
            var delete_url="<?php echo site_url('commenttagmachine/delete_uploaded_file');?>";
            $.post(delete_url, {op: "delete",name: data},
                function (resp,textStatus, jqXHR) {
                	$("#uploaded_image_video2").val('');                      
                });
           
         },
         onSuccess:function(files,data,xhr,pd)
           {
               // var data_modified = base_url+"upload/commenttagmachine/"+data;
               $("#uploaded_image_video2").val(data);		
           }
    });


    $(document.body).on('click','#submit_post',function(){    
          		    	
    	var campaign_name = $("#campaign_name").val();
    	var message = $("#message").val();
    	var commenter_range = $("#commenter_range").val();
    	
    	if(campaign_name=="")
    	{
    		alert(campaign_name_is_required);
    		return;
    	}

    	if(message=="")
    	{
    		alert(tag_content_is_required);
    		return;
    	}

    	if(commenter_range=="" || commenter_range==null)
    	{    		
    		alert(you_have_not_selected_commenters);
    		return;
    	}

    	var schedule_type = $("input[name=schedule_type]:checked").val();
    	var schedule_time = $("#schedule_time").val();
    	var time_zone = $("#time_zone").val();
    	var pleaseselectscheduletimetimezone = "<?php echo $pleaseselectscheduletimetimezone; ?>";
    	if(schedule_type=='later' && (schedule_time=="" || time_zone==""))
    	{
    		alert(pleaseselectscheduletimetimezone);
    		return;
    	}

    	$(this).addClass("disabled");
    	$("#response_modal_content").attr("class","");
    	var loading = '<img src="'+base_url+'assets/pre-loader/Fading squares2.gif" class="center-block"><br>';
    	$("#response_modal_content").html(loading);
	    // $("#response_modal").modal();
  	        	
	      var queryString = new FormData($("#bulk_tag_campaign_form")[0]);
	      $.ajax({
		       type:'POST' ,
		       url: base_url+"commenttagmachine/create_bulk_tag_campaign_action",
		       data: queryString,
		       cache: false,
		       contentType: false,
		       processData: false,
		       dataType:'JSON',
		       success:function(response)
		       {  
	      			if(response.status=='1') $("#response_modal_content").attr("class","alert alert-success text-center");
	      			else $("#response_modal_content").attr("class","alert alert-danger text-center");
	      			
	      			$("#response_modal_content").html(response.message);
	      			$("#submit_post").removeClass("disabled");
		       }
	      	});

    });


    $(document.body).on('click','.bulk_comment_reply_campaign',function(){  		
		$("#bulk_comment_reply_campaign").modal();
		var post_val = $(this).attr("id");
		var exploded=[];
		exploded=post_val.split('-');
		var tag_machine_enabled_post_list_id=exploded[1];
		var tag_campaign_tag_machine_comment_count=exploded[2];
		$("#bulk_comment_reply_campaign_enabled_post_list_id").val(tag_machine_enabled_post_list_id);
		$("#bulk_comment_reply_campaign_commenter_count").val(tag_campaign_tag_machine_comment_count);	  
	});

	$(document.body).on('click','#submit_post2',function(){    
          		    	
    	var campaign_name = $("#campaign_name2").val();
    	var message = $("#message2").val();
    	
    	if(campaign_name=="")
    	{
    		alert(campaign_name_is_required);
    		return;
    	}

    	if(message=="")
    	{
    		alert(reply_content_is_required);
    		return;
    	}

    	var schedule_type = $("input[name=schedule_type2]:checked").val();
    	var schedule_time = $("#schedule_time2").val();
    	var time_zone = $("#time_zone2").val();
    	var pleaseselectscheduletimetimezone = "<?php echo $pleaseselectscheduletimetimezone; ?>";
    	if(schedule_type=='later' && (schedule_time=="" || time_zone==""))
    	{
    		alert(pleaseselectscheduletimetimezone);
    		return;
    	}

    	$(this).addClass("disabled");
    	$("#response_modal_content2").attr("class","");
    	var loading = '<img src="'+base_url+'assets/pre-loader/Fading squares2.gif" class="center-block"><br>';
    	$("#response_modal_content2").html(loading);
	    // $("#response_modal").modal();
  	        	
	      var queryString = new FormData($("#bulk_comment_reply_campaign_form")[0]);
	      $.ajax({
		       type:'POST' ,
		       url: base_url+"commenttagmachine/create_comment_reply_campaign_action",
		       data: queryString,
		       cache: false,
		       contentType: false,
		       processData: false,
		       dataType:'JSON',
		       success:function(response)
		       {  
	      			if(response.status=='1') $("#response_modal_content2").attr("class","alert alert-success text-center");
	      			else $("#response_modal_content2").attr("class","alert alert-danger text-center");
	      			
	      			$("#response_modal_content2").html(response.message);
	      			$("#submit_post2").removeClass("disabled");
		       }
	      	});

    });

	$(document.body).on('click','.lead_first_name',function(){
    	var caretPos = $("#message2")[0].selectionStart;
	    var textAreaTxt = $("#message2").val();
	    var txtToAdd = " #LEAD_USER_FIRST_NAME# ";
	    $("#message2").val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos));
	});

	$(document.body).on('click','.lead_last_name',function(){

    	var caretPos =  $("#message2")[0].selectionStart;
	    var textAreaTxt =  $("#message2").val();
	    var txtToAdd = " #LEAD_USER_LAST_NAME# ";
	    $("#message2").val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos));
	});

	$(document.body).on('click','.lead_tag_name',function(){

    	var caretPos =  $("#message2")[0].selectionStart;
	    var textAreaTxt =  $("#message2").val();
	    var txtToAdd = " #TAG_USER# ";
	    $("#message2").val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos));
	});

	$('#comment_bulk_tag_campaign').on('hidden.bs.modal', function () { 
		window.location.assign(base_url+"commenttagmachine/post_list"); 
	});
	$('#bulk_comment_reply_campaign').on('hidden.bs.modal', function () { 
		window.location.assign(base_url+"commenttagmachine/post_list"); 
	});



});
</script>


<?php
if($auto_search_page_info_table_id!=0) { ?>
	<script type="text/javascript">
	$j("document").ready(function(){
		window.location.assign(base_url+"commenttagmachine/post_list"); 
	});
	</script>
<?php } ?>



<div class="modal fade" id="comment_bulk_tag_campaign" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title text-center"><i class="fa fa-plus-circle"></i> <?php echo $this->lang->line("Comment & Bulk Tag Campaign"); ?></h4>
			</div>
			<div class="modal-body">				
				<img src="<?php echo base_url('assets/pre-loader/Fading squares2.gif');?>" class="center-block" id="loading_div">
				<div class="row padding-20">
					<div class="col-xs-12 padding-10">					
						<form action="#" enctype="multipart/form-data" id="bulk_tag_campaign_form" method="post">
							<input type="hidden" name="tag_campaign_tag_machine_enabled_post_list_id" id="tag_campaign_tag_machine_enabled_post_list_id">
							<input type="hidden" name="tag_campaign_tag_machine_commenter_count" id="tag_campaign_tag_machine_commenter_count">
							
							<div class="form-group">
								<label><i class="fas fa-monument"></i> 
									<?php echo $this->lang->line("campaign name") ?> *
									<a href="#" data-placement="top" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("campaign name"); ?>" data-content="<?php echo $this->lang->line("put a name so that you can identify it later"); ?>"><i class='fa fa-info-circle'></i> </a>
								</label>
								<input type="text" class="form-control"  name="campaign_name" id="campaign_name">
							</div>
							<div class="form-group">
								<label><i class="fa fa-cutlery" aria-hidden="true"></i> 
									<?php echo $this->lang->line("Tag Content") ?> *
									<a href="#" data-placement="bottom"  data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("Tag Content") ?>" data-content="<?php echo $this->lang->line("Content to bulk tag commenters."); ?>"><i class='fa fa-info-circle'></i> </a>
								</label>
								<textarea class="form-control" name="message" id="message" placeholder="<?php echo $this->lang->line("Content to bulk tag commenters.");?>" style="height:170px;"></textarea>
							</div>


							<div class="form-group">
								<label class="control-label" ><i class="fas fa-camera-retro" aria-hidden="true"></i> <?php echo $this->lang->line("image/video upload") ?>
									<a href="#" data-placement="bottom" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("image/video upload") ?>" data-content="<?php echo $this->lang->line("upload image or video to embed with your bulk tag comment.") ?>"><i class='fa fa-info-circle'></i></a>
								</label>
								<div class="form-group">      
			                        <div id="image_video_upload"><?php echo $this->lang->line("upload") ?></div>	     
								</div>
								<input type="hidden" name="uploaded_image_video" id="uploaded_image_video">
							</div>

							<div class="clearfix"></div>
										
							<div class="form-group col-xs-12 col-md-6" style="padding-left:0;">
		                        <label><i class="fa fa-sun-o" aria-hidden="true"></i> 
		                       		<?php echo $this->lang->line("Select Commenter Range") ?> *
		                        	<a href="#" data-placement="top" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("Select Commenter Range");?>" data-content="<?php echo $this->lang->line("This range is sorted by comment time in decending order.") ?>"><i class='fa fa-info-circle'></i> </a>
		                        </label>

		                        <select name="commenter_range" id="commenter_range"  class="form-control" size="5"> 
			                                                      
		                        </select>
		                    </div> 

							<div class="form-group col-xs-12 col-md-6"  style="padding-right:0;">
								 <label><i class="fa fa-flag" aria-hidden="true"></i> 
		                       		<?php echo $this->lang->line("Do not tag these commenters") ?>
		                        	<a href="#" data-placement="top" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("Do not tag these commenters") ?>" data-content="<?php echo $this->lang->line("You can choose one or more. The commenters you choose here will be unlisted from this campaign and will not be tagged. Start typing a commenter name, it is auto-complete.") ?>"><i class='fa fa-info-circle'></i> </a>
		                        </label>
		                        <select style="width:100px;"  name="exclude[]" id="exclude" multiple="multiple" class="tokenize-sample form-control exclude_autocomplete">                                     
		                        </select>
		                    </div> 

		                    <div class="form-group">
								<label><i class="fa fa-clock" aria-hidden="true"></i> <?php echo $this->lang->line("schedule") ?></label>
								<br/>
								<input name="schedule_type" value="now" id="schedule_now" checked type="radio"> <?php echo $this->lang->line("now") ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input name="schedule_type" value="later" id="schedule_later" type="radio"> <?php echo $this->lang->line("later") ?> 
							</div>

							<div class="form-group schedule_block_item col-xs-12 col-md-6">
								<label><i class="fa fa-clock" aria-hidden="true"></i> <?php echo $this->lang->line("schedule time") ?>  <a href="#" data-placement="top"  data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("schedule time") ?>" data-content="<?php echo $this->lang->line("Select date and time when you want to process this campaign.") ?>"><i class='fa fa-info-circle'></i> </a></label>
								<input placeholder="<?php echo $this->lang->line("time");?>"  name="schedule_time" id="schedule_time" class="form-control datepicker" type="text"/>
							</div>

							<div class="form-group schedule_block_item col-xs-12 col-md-6">
								<label><i class="fa fa-calendar-o" aria-hidden="true"></i> 
									<?php echo $this->lang->line("time zone") ?>
									 <a href="#" data-placement="top" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("time zone") ?>" data-content="<?php echo $this->lang->line("server will consider your time zone when it process the campaign.") ?>"><i class='fa fa-info-circle'></i> </a>
								</label>
								<?php
								$time_zone[''] = $this->lang->line("please select");
								echo form_dropdown('time_zone',$time_zone,$this->config->item('time_zone'),' class="form-control" id="time_zone" required'); 
								?>
							</div>	

		                    <div class="clearfix"></div>

		                    <!-- <div class="form-group" id="custom_input_div">				                       
		                        <label>
		                       		<?php echo $this->lang->line("Tag List")." [".$this->lang->line("Up to").": ".$item_per_range."]";?> * 
		                        	<a href="#" data-placement="top" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("Tag These Commenters") ?>" data-content="<?php echo $this->lang->line("Select the commenters you want to tag.") ?>"><i class='fa fa-info-circle'></i> </a>
		                        </label>
		                        <select style="width:100px;"  name="include[]" id="include" multiple="multiple" class="tokenize-sample form-control include_autocomplete">                                     
		                        </select>
		                    </div>	 -->	

							<div class="clearfix"></div>
							<div class="alert text-center" id="response_modal_content"></div>
							<!-- <div class="modal-footer text-center" style="text-align: left !important;">
									<button style='margin-bottom:10px;' class="btn btn-primary btn-lg" id="submit_post" name="submit_post" type="button"><i class="fa fa-send"></i> <?php echo $this->lang->line("submit campaign") ?> </button>
							</div> -->
						</form>
					</div>

				</div>
			</div>

			<div class="clearfix"></div>
			<div class="modal-footer text-center" style="text-align: left !important;padding: 20px 0 17px 30px">
			    <button style='margin-bottom:10px;' class="btn btn-primary btn-lg" id="submit_post" name="submit_post" type="button"><i class="fa fa-send"></i> <?php echo $this->lang->line("submit") ?> </button>
            </div>
		</div>
	</div>
</div>

<div class="modal fade" id="bulk_comment_reply_campaign" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title text-center"><i class="fa fa-plus-circle"></i> <?php echo $this->lang->line("Bulk Comment Reply Campaign"); ?></h4>
			</div>
			<div class="modal-body">				
				<div class="row" style="padding: 10px 20px 10px 20px;">
					<div class="col-xs-12">							
						<form action="#" enctype="multipart/form-data" id="bulk_comment_reply_campaign_form" method="post">
							<input type="hidden" name="bulk_comment_reply_campaign_enabled_post_list_id" id="bulk_comment_reply_campaign_enabled_post_list_id">
							<input type="hidden" name="bulk_comment_reply_campaign_commenter_count" id="bulk_comment_reply_campaign_commenter_count">
							
							<div class="form-group">
								<label><i class="fas fa-monument"></i> 
									<?php echo $this->lang->line("campaign name") ?> *
									<a href="#" data-placement="top" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("campaign name"); ?>" data-content="<?php echo $this->lang->line("put a name so that you can identify it later"); ?>"><i class='fa fa-info-circle'></i> </a>
								</label>
								<input type="text" class="form-control"  name="campaign_name2" id="campaign_name2">
							</div>

							<div class="form-group">
								<label><i class="fas fa-reply"></i> 
									<?php echo $this->lang->line("Reply Content") ?> *
									<a href="#" data-placement="bottom"  data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("Reply Content") ?>" data-content="<?php echo $this->lang->line("Bulk comment reply content."); ?> Spintax example : {Hello|Hi|Hola} to you, {Mr.|Mrs.|Ms.} {{John|Tara|Sara}|Tom|Dave}"><i class='fa fa-info-circle'></i> </a>
								</label>
								<span class='pull-right'> 
									<a href="#" data-placement="top"  data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("tag user") ?>" data-content="<?php echo $this->lang->line("You can tag user in your comment reply. Facebook will notify them about mention whenever you tag.") ?>"><i class='fa fa-info-circle'></i> </a> 
									<a title="<?php echo $this->lang->line("tag user") ?>" class='btn btn-default btn-sm lead_tag_name'><i class='fa fa-tags'></i>  <?php echo $this->lang->line("tag user") ?></a>
								</span>
								<span class='pull-right'> 
									<a href="#" data-placement="top"  data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("include lead user last name") ?>" data-content="<?php echo $this->lang->line("You can include #LEAD_USER_LAST_NAME# variable inside your message. The variable will be replaced by real names when we will send it.") ?>"><i class='fa fa-info-circle'></i> </a> 
									<a title="<?php echo $this->lang->line("include lead user name") ?>" class='btn btn-default btn-sm lead_last_name'><i class='fa fa-user'></i>  <?php echo $this->lang->line("last name") ?></a>
								</span>
								<span class='pull-right'> 
									<a href="#" data-placement="top"  data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("include lead user first name") ?>" data-content="<?php echo $this->lang->line("You can include #LEAD_USER_FIRST_NAME# variable inside your message. The variable will be replaced by real names when we will send it.") ?>"><i class='fa fa-info-circle'></i> </a> 
									<a title="<?php echo $this->lang->line("include lead user name") ?>" class='btn btn-default btn-sm lead_first_name'><i class='fa fa-user'></i>  <?php echo $this->lang->line("first name") ?></a>
								</span>
								<textarea class="form-control" name="message2" id="message2" placeholder="<?php echo $this->lang->line("Bulk comment reply content.");?> Spintax example : {Hello|Hi|Hola} to you, {Mr.|Mrs.|Ms.} {{John|Tara|Sara}|Tom|Dave}" style="height:170px;"></textarea>
							</div>

							<div class="form-group">
								<label class="control-label" ><i class="fas fa-camera-retro"></i> <?php echo $this->lang->line("image/video upload") ?>
									<a href="#" data-placement="bottom" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("image/video upload") ?>" data-content="<?php echo $this->lang->line("upload image or video to embed with your comment reply.") ?> "><i class='fa fa-info-circle'></i></a>
								</label>
								<div class="form-group">      
			                        <div id="image_video_upload2"><?php echo $this->lang->line("upload") ?></div>	     
								</div>
								<input type="hidden" name="uploaded_image_video2" id="uploaded_image_video2">
							</div>

							<div class="form-group col-xs-12 col-md-6" style="padding-left:0">
								<label class="control-label" ><i class="fas fa-reply-all"></i> <?php echo $this->lang->line("reply same commenter multiple times?") ?>
									<a href="#" data-placement="bottom" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("reply same commenter multiple times?") ?>" data-content="<?php echo $this->lang->line("same user may comment multiple time, do you want to reply all of them or not.") ?>"><i class='fa fa-info-circle'></i></a>
								</label><br>
								<input type="radio" name="reply_multiple" value="1" checked> <?php echo $this->lang->line("yes"); ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="radio" name="reply_multiple" value="0"> <?php echo $this->lang->line("no"); ?><br>
							</div>

							<div class="form-group col-xs-12 col-md-6" style="padding-right:0;">
								<label><i class="fa fa-american-sign-language-interpreting"></i> 
									<?php echo $this->lang->line("delay between two replies [seconds]") ?> *
									<a href="#" data-placement="bottom"  data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("delay between two replies [seconds]") ?>" data-content="<?php echo $this->lang->line("Too frequent replies can be suspicious to Facebook. It is safe to use some seconds of delay. Zero means random delay."); ?>"><i class='fa fa-info-circle'></i> </a>
								</label>
								<input class="form-control" name="delay_time" id="delay_time" type="number" min="0" value="0">
							</div>

							<div class="form-group">
								<label><i class="fas fa-clock"></i> <?php echo $this->lang->line("schedule") ?></label>
								<br/>
								<input name="schedule_type2" value="now" id="schedule_now2" checked type="radio"> <?php echo $this->lang->line("now") ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input name="schedule_type2" value="later" id="schedule_later2" type="radio"> <?php echo $this->lang->line("later") ?> 
							</div>

							<div class="form-group schedule_block_item2 col-xs-12 col-md-6">
								<label><i class="fas fa-clock"></i> <?php echo $this->lang->line("schedule time") ?> <a href="#" data-placement="top"  data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("schedule time") ?>" data-content="<?php echo $this->lang->line("Select date and time when you want to process this campaign.") ?>"><i class='fa fa-info-circle'></i> </a></label>
								<input placeholder="<?php echo $this->lang->line("time");?>"  name="schedule_time2" id="schedule_time2" class="form-control datepicker" type="text"/>
							</div>

							<div class="form-group schedule_block_item2 col-xs-12 col-md-6" style="padding-right:0;">
								<label><i class="fa fa-calendar-o"></i> 
									<?php echo $this->lang->line("time zone") ?>
									 <a href="#" data-placement="top" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("time zone") ?>" data-content="<?php echo $this->lang->line("server will consider your time zone when it process the campaign.") ?>"><i class='fa fa-info-circle'></i> </a>
								</label>
								<?php
								$time_zone[''] = $this->lang->line("please select");
								echo form_dropdown('time_zone2',$time_zone,$this->config->item('time_zone'),' class="form-control" id="time_zone2" required'); 
								?>
							</div>	

							<div class="clearfix"></div>
							<div class="alert text-center" id="response_modal_content2"></div>
							<!-- <div class="modal-footer text-center" style="text-align: left !important;">
									<button style='margin-bottom:10px;' class="btn btn-primary btn-lg" id="submit_post2" name="submit_post2" type="button"><i class="fa fa-send"></i> <?php echo $this->lang->line("submit campaign") ?> </button>
							</div> -->
						</form>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="modal-footer text-center" style="text-align: left !important;padding: 20px 0 17px 30px">
			    <button class="btn btn-lg btn-primary" id="submit_post2" name="submit_post2" type="button"><i class="fa fa-send"></i> <?php echo $this->lang->line("submit") ?> </button>
            </div>
		</div>
	</div>
</div>


<div class="modal fade" id="comment_list_modal" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-comment"></i> <?php echo $this->lang->line("Comment List"); ?></h4>
			</div>
			<div class="modal-body" id="comment_list_body">

			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="commenter_list_modal" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-user"></i> <?php echo $this->lang->line("Commenter List"); ?></h4>
			</div>
			<div class="modal-body" id="commenter_list_body">

			</div>
		</div>
	</div>
</div>


<style type="text/css" media="screen">
	.popover
	{
	    min-width: 300px !important;
	}
	.tokenize-sample,.Tokenize{border:none !important;padding:0 !important;}
	.box-header{border-bottom:1px solid #ccc !important;margin-bottom:15px;}
	.box-primary{border:1px solid #ccc !important;}
	.box-body{padding:10px 10px !important;}
	.preview{padding:10px 0 !important;}
	.box-footer{border-top:1px solid #ccc !important;padding:10px 0;}
	.padding-5{padding:5px;}
	.padding-20{padding:20px;}
	.box-header{color:#3C8DBC;}
	.box-body
	{
		font-family: helvetica,​arial,​sans-serif;
		padding: 20px;
		background: #fcfcfc;
	}
	#test_msg_box_body
	{
		background: #fff !important;
	}
	.box-footer 
	{		
		background: #fcfcfc;
	}

	.ms-choice span
	{
		padding-top: 2px !important;
	}
	.hidden
	{
		display: none;
	}
	.box-primary
	{
		-webkit-box-shadow: 0px 2px 14px -5px rgba(0,0,0,0.75);
		-moz-box-shadow: 0px 2px 14px -5px rgba(0,0,0,0.75);
		box-shadow: 0px 2px 14px -5px rgba(0,0,0,0.75);
	}

	.TokensContainer{height: 140px !important;}	
	.content-wrapper{background: #fff;}
	.ajax-upload-dragdrop{width:100% !important;}
</style>



