<?php $this->load->view('admin/theme/message'); ?>
<div class="well well_border_left">
	<h4 class="text-center"> <i class="fa fa-clock-o"></i> <?php echo $this->lang->line("cron job"); ?></h4>
</div>
<section class="content-header">
   <section class="content">
	    <?php
		if($api_key!="") { ?>
			<div id=''>
				<h4 style="margin:0">
					<div class="alert alert-info" style="margin-bottom:0;background:#fff !important; color:<?php echo $THEMECOLORCODE;?> !important;border-color:#fff;"">
						<i class="fa fa-clock-o"></i> <?php echo $this->lang->line("Comment & Bulk Tag Campaign");?> [every 5 minutes]
					</div>
				</h4>
				<div class="well" style="background:#fff;margin-top:0;border-radius:0;">
					<?php echo "curl ".site_url("commenttagmachine/comment_bulk_tag_cronjob")."/".$api_key." >/dev/null 2>&1"; ?> 
				</div>
			</div>	
			<div id=''>
				<h4 style="margin:0">
					<div class="alert alert-info" style="margin-bottom:0;background:#fff !important; color:<?php echo $THEMECOLORCODE;?> !important;border-color:#fff;"">
						<i class="fa fa-clock-o"></i> <?php echo $this->lang->line("Bulk Comment Reply Campaign");?> [every minute]
					</div>
				</h4>
				<div class="well" style="background:#fff;margin-top:0;border-radius:0;">
					<?php echo "curl ".site_url("commenttagmachine/bulk_comment_reply_cronjob")."/".$api_key." >/dev/null 2>&1"; ?>
				</div>
			</div>		
		<?php } else echo "<a class='btn btn-lg btn-warning' href='".base_url('native_api/index')."'><i class='fa fa-key'></i> ".$this->lang->line("generate API key")."</a>";?>


   </section>
</section>
