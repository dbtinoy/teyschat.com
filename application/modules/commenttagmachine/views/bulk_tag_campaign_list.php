<?php 
$this->load->view('admin/theme/message'); 
$this->load->view("include/upload_js"); 
?>

<!-- Main content -->
<section class="content-header">
	<h1 class = 'text-info'><i class="fa fa-list"></i> <?php echo $this->lang->line("Comment & Bulk Tag Campaign Report");?></h1>
</section>
<section class="content">
	<div class="row" >
		<div class="col-xs-12">
			<div class="grid_container" style="width:100%; min-height:840px;">
				<table
				id="tt"
				class="easyui-datagrid"
				url="<?php echo base_url()."commenttagmachine/bulk_tag_campaign_list_data"; ?>"

				pagination="true"
				rownumbers="true"
				toolbar="#tb"
				pageSize="10"
				pageList="[5,10,15,20,50,100]"
				fit= "true"
				fitColumns= "true"
				nowrap= "true"
				view= "detailview"
				idField="id"
				>

					<thead>
						<tr>
							<th field="page_profile" formatter="page_profile_image" align="center"><?php echo $this->lang->line("picture"); ?></th>
							<th field="campaign_name"  sortable="true" align="center"><?php echo $this->lang->line("campign name"); ?></th>
							<th field="post_id" sortable="true" formatter="post_id_link"><?php echo $this->lang->line("Post ID"); ?></th>
							<th field="commenter_count"  sortable="true" align="center"><?php echo $this->lang->line("Tag Count"); ?></th>
							<th field="posting_status" align="center"><?php echo $this->lang->line("Status"); ?></th>
							<th field="report" align="center"><?php echo $this->lang->line("Report"); ?></th>	
							<th field="edit" align="center"><?php echo $this->lang->line("edit"); ?></th>	
							<th field="delete" align="center"><?php echo $this->lang->line("delete"); ?></th>	
							<th field="page_name" sortable="true" formatter="page_name_link"><?php echo $this->lang->line("page name"); ?></th>	
							<th field="attachment" align="center"><?php echo $this->lang->line("Attachment"); ?></th>
							<th field="campaign_created" sortable="true"><?php echo $this->lang->line("Created at"); ?></th>
							<th field="last_updated_at" sortable="true"><?php echo $this->lang->line("Last Updated"); ?></th>
						</tr>
					</thead>
				</table>
			</div>

			<div id="tb" style="padding:3px">

				<?php
					$search_campaign_id = $this->session->userdata('bulk_tag_campaign_list_campaign_id');
					$search_enable_id = $this->session->userdata('bulk_tag_campaign_list_enable_id');
					$search_campaign_name  = $this->session->userdata('bulk_tag_campaign_list_campaign_name');
					$search_page_id  = $this->session->userdata('bulk_tag_campaign_list_page_id');
			        $search_post_id  = $this->session->userdata('bulk_tag_campaign_list_post_id');
			        $schedule_time_from = $this->session->userdata('bulk_tag_campaign_list_schedule_time_from');
			        $schedule_time_to = $this->session->userdata('bulk_tag_campaign_list_schedule_time_to');
				?>
		
				<form class="form-inline" style="margin-top:20px">

					<div class="form-group">
						<input id="search_campaign_id" name="search_campaign_id" value="<?php echo $search_campaign_id;?>" type="hidden">
						<input id="search_enable_id" name="search_enable_id" value="<?php echo $search_enable_id;?>" type="hidden">
						<input id="search_campaign_name" name="search_campaign_name" value="<?php echo $search_campaign_name;?>" class="form-control" size="20" placeholder="<?php echo $this->lang->line("Campaign Name") ?>">
					</div>

					<div class="form-group">
						<input id="search_post_id" name="search_post_id" value="<?php echo $search_post_id;?>" class="form-control" size="20" placeholder="<?php echo $this->lang->line("Post ID") ?>">
					</div>

					<div class="form-group">
						<select name="search_page_id" id="search_page_id"  class="form-control">
							<option value=""><?php echo $this->lang->line("all page") ?></option>
							<?php
								foreach ($page_info as $key => $value)
								{
									if($value['id'] == $search_page_id)
									echo "<option selected value='".$value['id']."'>".$value['page_name']."</option>";
									else echo "<option value='".$value['id']."'>".$value['page_name']."</option>";
								}
							?>
						</select>
					</div>

			
					<div class="form-group">
						<input id="schedule_time_from" value="<?php echo $schedule_time_from;?>" name="schedule_time_from" class="form-control datepicker" size="20" placeholder="<?php echo $this->lang->line("Scheduled From") ?>">
					</div>

					<div class="form-group">
						<input id="schedule_time_to" value="<?php echo $schedule_time_to;?>" name="schedule_time_to" class="form-control  datepicker" size="20" placeholder="<?php echo $this->lang->line("Scheduled To") ?>">
					</div>

					<button class='btn btn-info'  onclick="doSearch(event)"><?php echo $this->lang->line("search");?></button>
			</div>

				</form>
		</div>
	</div>
</section>



<?php
	$somethingwentwrong = $this->lang->line("something went wrong.");
	$pleasewait = $this->lang->line("please wait").'...';
	$areyousure = $this->lang->line("are you sure");
 ?>
<script>

	var base_url="<?php echo site_url(); ?>";
	var somethingwentwrong="<?php echo $somethingwentwrong;?>";
	var pleasewait="<?php echo $pleasewait;?>";
	var areyousure="<?php echo $areyousure;?>";

	function page_name_link(value,row,index)
	{
		var page_url = "<a href='https://facebook.com/"+row.page_id+"' target='_blank'>"+row.page_name+"</a>";
		return page_url;
	} 

	function post_id_link(value,row,index)
	{
		var post_url = "<a href='https://facebook.com/"+row.post_id+"' target='_blank'>"+row.post_id+"</a>";
		return post_url;
	} 

	function page_profile_image(value,row,index)
	{
		if(typeof(row.page_profile)==='undefined') return false;
		var img_link = "<img style='width:50px;height:50px' src='"+row.page_profile+"'>";
		return img_link;
	} 

    function doSearch(event)
	{
		event.preventDefault();
		$j('#tt').datagrid('load',{
			search_campaign_name: 	  $j('#search_campaign_name').val(),
			search_page_id   	:     $j('#search_page_id').val(),
			search_post_id   	:     $j('#search_post_id').val(),
			schedule_time_from 	:     $j('#schedule_time_from').val(),
			schedule_time_to  	:     $j('#schedule_time_to').val(),
			search_campaign_id  :     $j('#search_campaign_id').val(),
			search_enable_id  	:     $j('#search_enable_id').val(),
			is_searched			:     1
		});

	}
</script>

<script>

$j("document").ready(function(){

	$('[data-toggle="popover"]').popover(); 
	$('[data-toggle="popover"]').on('click', function(e) {e.preventDefault(); return true;});

    $j('.datepicker').datetimepicker({
    theme:'light',
    format:'Y-m-d',
    formatDate:'Y-m-d',
    timepicker:false
    });

 	
	$(document.body).on('click','.show_report',function(){
  		var loading = '<br/><img src="'+base_url+'assets/pre-loader/Fading squares2.gif" class="center-block"><br/>';
		$("#commenter_list_modal").modal();
	    $("#commenter_list_body").html(loading);

		var id = $(this).attr("data-id");

		$.ajax({
            type:'POST' ,
            url:"<?php echo site_url();?>commenttagmachine/bulk_tag_campaign_report",
            data:{id:id},
            success:function(response){
            	$("#commenter_list_body").html(response);
            }
        });
	});

	$(document.body).on('click','.delete_campaign',function(){
		var id = $(this).attr("data-id");
		// var ans=confirm(areyousure);
		// if(!ans) return false;
		alertify.confirm('<?php echo $this->lang->line("Alert")?>',areyousure,function()
		{
			$.ajax({
	            type:'POST' ,
	            url:"<?php echo site_url();?>commenttagmachine/delete_bulk_tag_campaign",
	            data:{id:id},
	            success:function(response)
	            {
	            	$j('#tt').datagrid('reload');
	            }
	        });
	    },function(){});
	});
});
</script>

<!-- AUTO SEARCH -->
<?php
if($auto_search_enabled_post_list!=0 || $auto_search_campaign_id!=0) { 
?>	
	<script type="text/javascript">
	$j("document").ready(function(){
		window.location.assign(base_url+"commenttagmachine/bulk_tag_campaign_list"); 
	});
	</script>
<?php } ?>


<div class="modal fade" id="commenter_list_modal" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-list"></i> <?php echo $this->lang->line("Tag List"); ?></h4>
			</div>
			<div class="modal-body" id="commenter_list_body">

			</div>
		</div>
	</div>
</div>





