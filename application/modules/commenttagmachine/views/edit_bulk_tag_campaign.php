<?php $this->load->view("include/upload_js"); ?>
<div class="clearfix"></div>
<div id="comment_bulk_tag_campaign" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg" style='width: 1000px !important;'>
		<div class="modal-content">
			<div class="modal-header">
				<!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
				<h4 class="modal-title text-center"><i class="fa fa-edit"></i> <?php echo $this->lang->line("Edit Comment & Bulk Tag Campaign"); ?></h4>
			</div>
			<div class="modal-body">				
				<!-- <img src="<?php echo base_url('assets/pre-loader/Fading squares2.gif');?>" class="center-block" id="loading_div"> -->
				<div class="row padding-20">
					<div class="col-xs-12 padding-10">						
						<form action="#" enctype="multipart/form-data" id="bulk_tag_campaign_form" method="post">
							<input type="hidden" name="campaign_id" value="<?php echo $xdata[0]["id"];?>">
							<input type="hidden" name="tag_campaign_tag_machine_enabled_post_list_id" value="<?php echo $xdata[0]["tag_machine_enabled_post_list_id"];?>">
							<div class="form-group">
								<label><i class="fas fa-monument"></i> 
									<?php echo $this->lang->line("campaign name") ?> *
									<a href="#" data-placement="top" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("campaign name"); ?>" data-content="<?php echo $this->lang->line("put a name so that you can identify it later"); ?>"><i class='fa fa-info-circle'></i> </a>
								</label>
								<input value="<?php echo $xdata[0]["campaign_name"];?>" type="text" class="form-control"  name="campaign_name" id="campaign_name">
							</div>
							<div class="form-group">
								<label><i class="fas fa-cutlery"></i> 
									<?php echo $this->lang->line("Tag Content") ?> *
									<a href="#" data-placement="bottom"  data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("Tag Content") ?>" data-content="<?php echo $this->lang->line("Content to bulk tag commenters."); ?>"><i class='fa fa-info-circle'></i> </a>
								</label>
								<textarea class="form-control" name="message" id="message" placeholder="<?php echo $this->lang->line("Content to bulk tag commenters.");?>" style="height:170px;"><?php echo $xdata[0]["tag_content"];?></textarea>
							</div>


							<div class="form-group">
								<label class="control-label" ><i class="fas fa-camera-retro"></i> <?php echo $this->lang->line("image/video upload") ?>
									<a href="#" data-placement="bottom" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("image/video upload") ?>" data-content="<?php echo $this->lang->line("upload image or video to embed with your bulk tag comment.") ?>"><i class='fa fa-info-circle'></i></a>
								</label>

								<div class="form-group">      
			                        <div id="image_video_upload"><?php echo $this->lang->line("upload") ?></div>	     
								</div>
								<input type="hidden" value="<?php echo $xdata[0]["uploaded_image_video"];?>" name="uploaded_image_video" id="uploaded_image_video">

								<?php if($xdata[0]["uploaded_image_video"]!="") 
								{
									echo '<div id="upload_preview" class="text-center">';
									$ext_exp=explode('.', $xdata[0]["uploaded_image_video"]);
									$ext=array_pop($ext_exp);
									$video_array=array("flv","mp4","wmv");
									if(!in_array($ext,$video_array))
									{
										echo "<img class='img-thumbnail' style='width:320px' src='".base_url("upload/commenttagmachine/".$xdata[0]["uploaded_image_video"])."'>";
									}
									else
									{
										echo '<video width="320px" height="200" controls style="border:1px solid #ccc">
											<source src="'.base_url("upload/commenttagmachine/".$xdata[0]["uploaded_image_video"]).'">												
										</video>;';
									}
									echo '</div>';
								} ?>
							</div>

							<div class="clearfix"></div>
										
							<div class="form-group col-xs-12 col-md-6" style="padding-left:0;">
		                        <label><i class="fa fa-sun-o" aria-hidden="true"></i> 
		                       		<?php echo $this->lang->line("Select Commenter Range") ?> *
		                        	<a href="#" data-placement="top" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("Select Commenter Range");?>" data-content="<?php echo $this->lang->line("This range is sorted by comment time in decending order.") ?>"><i class='fa fa-info-circle'></i> </a>
		                        </label>

		                        <select name="commenter_range" id="commenter_range"  class="form-control" size="5"> 
			                        <?php echo $commenter_range;?>                              
		                        </select>
		                    </div> 

							<div class="form-group col-xs-12 col-md-6"  style="padding-right:0;">
								 <label><i class="fa fa-flag" aria-hidden="true"></i> 
		                       		<?php echo $this->lang->line("Do not tag these commenters") ?>
		                        	<a href="#" data-placement="top" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("Do not tag these commenters") ?>" data-content="<?php echo $this->lang->line("You can choose one or more. The commenters you choose here will be unlisted from this campaign and will not be tagged. Start typing a commenter name, it is auto-complete.") ?>"><i class='fa fa-info-circle'></i> </a>
		                        </label>
		                        <select style="width:100px;"  name="exclude[]" id="exclude" multiple="multiple" class="tokenize-sample form-control exclude_autocomplete">   
		                        <?php 
		                        foreach ($xtag_exclude as $key => $value) 
	                       		{
	                       			echo  "<option selected value='".$value["commenter_fb_id"]."'>".$value["commenter_name"]."</option>";
	                       		}
		                        ?>                                  
		                        </select>
		                    </div> 

		                    <div class="form-group hidden">
								<label><i class="fa fa-clock" aria-hidden="true"></i> <?php echo $this->lang->line("schedule") ?></label>
								<br/>
								<input name="schedule_type" value="now" id="schedule_now" type="radio"> <?php echo $this->lang->line("now") ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input name="schedule_type" value="later" id="schedule_later" checked type="radio"> <?php echo $this->lang->line("later") ?> 
							</div>

							<div class="form-group schedule_block_item col-xs-12 col-md-6" style="padding-left:0;">
								<label><i class="fa fa-clock" aria-hidden="true"></i> <?php echo $this->lang->line("schedule time") ?>  <a href="#" data-placement="top"  data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("schedule time") ?>" data-content="<?php echo $this->lang->line("Select date and time when you want to process this campaign.") ?>"><i class='fa fa-info-circle'></i> </a></label>
								<input value="<?php echo $xdata[0]["schedule_time"];?>" placeholder="<?php echo $this->lang->line("time");?>"  name="schedule_time" id="schedule_time" class="form-control datepicker" type="text"/>
							</div>

							<div class="form-group schedule_block_item col-xs-12 col-md-6" style="padding-right:0;">
								<label><i class="fa fa-calendar-o" aria-hidden="true"></i> 
									<?php echo $this->lang->line("time zone") ?>
									 <a href="#" data-placement="top" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("time zone") ?>" data-content="<?php echo $this->lang->line("server will consider your time zone when it process the campaign.") ?>"><i class='fa fa-info-circle'></i> </a>
								</label>
								<?php
								$time_zone[''] = $this->lang->line("please select");
								echo form_dropdown('time_zone',$time_zone,$xdata[0]["time_zone"],' class="form-control" id="time_zone" required'); 
								?>
							</div>	

		                    <div class="clearfix"></div>

		                    <!-- <div class="form-group" id="custom_input_div">				                       
		                        <label>
		                       		<?php echo $this->lang->line("Tag List")." [".$this->lang->line("Up to").": ".$item_per_range."]";?> * 
		                        	<a href="#" data-placement="top" data-toggle="popover" data-trigger="focus" title="<?php echo $this->lang->line("Tag These Commenters") ?>" data-content="<?php echo $this->lang->line("Select the commenters you want to tag.") ?>"><i class='fa fa-info-circle'></i> </a>
		                        </label>
		                        <select style="width:100px;"  name="include[]" id="include" multiple="multiple" class="tokenize-sample form-control include_autocomplete">                                     
		                        </select>
		                    </div>	 -->	

							<div class="clearfix"></div>
							<div class="alert text-center" id="response_modal_content"></div>
							<!-- <div class="box-footer clearfix">
								<div class="col-xs-12">
									<button style='width:100%;margin-bottom:10px;' class="btn btn-warning center-block btn-lg" id="submit_post" name="submit_post" type="button"><i class="fa fa-pencil"></i> <?php echo $this->lang->line("edit campaign") ?> </button>
								</div>
							</div> -->
						</form>
					</div>
				</div>
			</div>

			<div class="clearfix"></div>
			<div class="modal-footer text-center" style="text-align: left !important;padding: 20px 0 17px 30px">
			    <button style='margin-bottom:10px;' class="btn btn-primary btn-lg" id="submit_post" name="submit_post" type="button"><i class="fa fa-send"></i> <?php echo $this->lang->line("submit") ?> </button>
            </div>
		</div>
	</div>
</div>
<div class="clearfix"></div>




<?php
	$somethingwentwrong = $this->lang->line("something went wrong.");
	$pleasewait = $this->lang->line("please wait").'...';
	$areyousure = $this->lang->line("are you sure");
	$startcommenternames = $this->lang->line("Start typing commenter names you want to excude from tag list");
	$list_of_commenters = $this->lang->line("List of commenters which this campaign will tag");
	$campaign_name_is_required=$this->lang->line("Campaign name is required.");
	$tag_content_is_required=$this->lang->line("Tag content is required.");
	$you_have_not_selected_commenters=$this->lang->line("You have not selected commenters.");
	$no_subscribed_commenter_found=$this->lang->line("No subscribed commenter found.");
	$reply_content_is_required=$this->lang->line("Reply content is required.");
	$pleaseselectscheduletimetimezone = $this->lang->line("Please select schedule time/time zone.");
	$item_per_range=$this->config->item('item_per_range');
    if($item_per_range=='') $item_per_range=50;
    $tag_machine_enabled_post_list_id=$xdata[0]["tag_machine_enabled_post_list_id"];
 ?>
<script>
	var base_url="<?php echo site_url(); ?>";
	var somethingwentwrong="<?php echo $somethingwentwrong;?>";
	var pleasewait="<?php echo $pleasewait;?>";
	var areyousure="<?php echo $areyousure;?>";
	var startcommenternames="<?php echo $startcommenternames;?>";
	var item_per_range="<?php echo $item_per_range;?>";
	var list_of_commenters="<?php echo $list_of_commenters;?>";
	var campaign_name_is_required="<?php echo $campaign_name_is_required;?>";
	var tag_content_is_required="<?php echo $tag_content_is_required;?>";
	var you_have_not_selected_commenters="<?php echo $you_have_not_selected_commenters;?>";
	var no_subscribed_commenter_found="<?php echo $no_subscribed_commenter_found;?>";
	var reply_content_is_required="<?php echo $reply_content_is_required;?>"
	var tag_machine_enabled_post_list_id="<?php echo $tag_machine_enabled_post_list_id;?>"
</script>

<script>

$j("document").ready(function(){

	$('[data-toggle="popover"]').popover(); 
	$('[data-toggle="popover"]').on('click', function(e) {e.preventDefault(); return true;});

    $j('.datepicker').datetimepicker({
   	theme:'light',
   	format:'Y-m-d H:i:s',
   	formatDate:'Y-m-d H:i:s'
  	});

	$('.exclude_autocomplete').tokenize({
        datas: base_url+"commenttagmachine/commenter_autocomplete/"+tag_machine_enabled_post_list_id,
        placeholder: startcommenternames,
        dropdownMaxItems: 20,
        tokensMaxItems: item_per_range
    });



    $(document.body).on('click','#submit_post',function(){    
          		    	
    	var campaign_name = $("#campaign_name").val();
    	var message = $("#message").val();
    	var commenter_range = $("#commenter_range").val();
    	
    	if(campaign_name=="")
    	{
    		alert(campaign_name_is_required);
    		return;
    	}

    	if(message=="")
    	{
    		alert(tag_content_is_required);
    		return;
    	}

    	if(commenter_range=="" || commenter_range==null)
    	{    		
    		alert(you_have_not_selected_commenters);
    		return;
    	}

    	var schedule_type = 'later';
    	var schedule_time = $("#schedule_time").val();
    	var time_zone = $("#time_zone").val();
    	var pleaseselectscheduletimetimezone = "<?php echo $pleaseselectscheduletimetimezone; ?>";
    	if(schedule_type=='later' && (schedule_time=="" || time_zone==""))
    	{
    		alert(pleaseselectscheduletimetimezone);
    		return;
    	}

    	$(this).addClass("disabled");
    	$("#response_modal_content").attr("class","");
    	var loading = '<img src="'+base_url+'assets/pre-loader/Fading squares2.gif" class="center-block"><br>';
    	$("#response_modal_content").html(loading);
	    // $("#response_modal").modal();
  	        	
	      var queryString = new FormData($("#bulk_tag_campaign_form")[0]);
	      $.ajax({
		       type:'POST' ,
		       url: base_url+"commenttagmachine/edit_bulk_tag_campaign_action",
		       data: queryString,
		       cache: false,
		       contentType: false,
		       processData: false,
		       dataType:'JSON',
		       success:function(response)
		       {  
	      			if(response.status=='1') $("#response_modal_content").attr("class","alert alert-success text-center");
	      			else $("#response_modal_content").attr("class","alert alert-danger text-center");
	      			
	      			$("#response_modal_content").html(response.message);
	      			$("#submit_post").removeClass("disabled");
		       }
	      	});

    });

  	$("#image_video_upload").uploadFile({
        url:base_url+"commenttagmachine/upload_image_video",
        fileName:"myfile",
        maxFileSize:100*1024*1024,
        showPreview:false,
        returnType: "json",
        dragDrop: true,
        showDelete: true,
        multiple:false,
        maxFileCount:1, 
        acceptFiles:".png,.jpg,.jpeg,.JPEG,.JPG,.PNG,.gif,.GIF,.flv,.mp4,.wmv,.WMV,.MP4,.FLV",
        deleteCallback: function (data, pd) {
            var delete_url="<?php echo site_url('commenttagmachine/delete_uploaded_file');?>";
            $.post(delete_url, {op: "delete",name: data},
                function (resp,textStatus, jqXHR) {
                	$("#uploaded_image_video").val('');                
                });
           
         },
         onSuccess:function(files,data,xhr,pd)
           {
               // var data_modified = base_url+"upload/commenttagmachine/"+data;
               $("#uploaded_image_video").val(data);                 
               $("#upload_preview").html('');    		
           }
    });

});
</script>






<style type="text/css" media="screen">
	.popover
	{
	    min-width: 300px !important;
	}
	.tokenize-sample,.Tokenize{border:none !important;padding:0 !important;}
	.box-header{border-bottom:1px solid #ccc !important;margin-bottom:15px;}
	.box-primary{border:1px solid #ccc !important;}
	.box-body{padding:10px 20px !important;}
	.preview{padding:10px 0 !important;}
	.box-footer{border-top:1px solid #ccc !important;padding:10px 0;}
	.padding-5{padding:5px;}
	.padding-20{padding:20px;}
	.box-header{color:#3C8DBC;}
	.box-body
	{
		font-family: helvetica,​arial,​sans-serif;
		padding: 20px;
		background: #fcfcfc;
	}
	#test_msg_box_body
	{
		background: #fff !important;
	}
	.box-footer 
	{		
		background: #fcfcfc;
	}

	.ms-choice span
	{
		padding-top: 2px !important;
	}
	.hidden
	{
		display: none;
	}
	.box-primary
	{
		-webkit-box-shadow: 0px 2px 14px -5px rgba(0,0,0,0.75);
		-moz-box-shadow: 0px 2px 14px -5px rgba(0,0,0,0.75);
		box-shadow: 0px 2px 14px -5px rgba(0,0,0,0.75);
	}
	.TokensContainer{height: 140px !important;}	
	.content-wrapper{background: #fff;}
	.ajax-upload-dragdrop{width:100% !important;}
	.content-wrapper{background: #eee !important;}
</style>