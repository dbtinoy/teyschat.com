#RewriteEngine on
#RewriteCond %{HTTP_HOST} ^teyschat.com$ [NC]
#RewriteCond $1 !^(index\.php|images|robots\.txt)
#RewriteRule ^(.*)$ /index.php/$1 [L]


RewriteEngine On

# Canonical redirect www to non-www
RewriteCond %{HTTP_HOST} ^www\.teyschat\.com [NC]
RewriteRule ^ https://teyschat.com%{REQUEST_URI} [R=301,L]

# Rewrite requests for the root application
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ /index.php/$1 [L]  

<Files ~ "^.*\.([Pp][Hh][Pp])"> 
    Order Deny,Allow
    Deny from all
</Files>

<Files index.php>
    Order Allow,Deny
    Allow from all
</Files>